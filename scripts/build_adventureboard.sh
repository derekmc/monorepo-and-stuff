releaseVersion(){
  sed -i 's/RELEASE/release/g' out/volt/voltbuilder.json
}
debugVersion(){
  sed -i 's/RELEASE/debug/g' out/volt/voltbuilder.json
}
configValues(){ 
  sed -i 's/APPNAME/AdventureBoardEditor/g' out/volt/config.xml
  sed -i 's/PACKAGENAME/com.AdventureBoardEditorsupportinfo.AdventureBoardEditor/g' out/volt/config.xml
  sed -i 's/VERSIONCOUNTER/8/g' out/volt/config.xml
  sed -i 's/MINORVERSION/1/g' out/volt/config.xml
  sed -i 's/APPDESC/An Alternative Chorded Keyboard System/g' out/volt/config.xml
  sed -i 's/APPNAME/AdventureBoardEditor/g' out/volt/www/index.html
  sed -i 's/APPDESC/An Alternative Chorded Keyboard System/g' out/volt/www/index.html
  sed -i 's/APPFILENAME/adventureboard/g' out/volt/www/index.html
}

mkdir -p out
rm -rf out/volt
cp -r ../resources/volt out/

# copy extra files
cp ../projects/adventureboard/adventureboard.html out/volt/www/
cp ../projects/adventureboard/chordvisualizer.html out/volt/www/
mkdir -p out/volt/www/image/
cp ../www/image/keymap.png out/volt/www/image/
cp ../www/image/keyboard_overview.png out/volt/www/image/
cp ../www/image/keyboard_letters.png out/volt/www/image/
cp ../www/image/keyboard_special.png out/volt/www/image/
cp ../www/image/keyboard_punctuation.png out/volt/www/image/
cp ../www/image/keyboard_standard.png out/volt/www/image/
cp ../www/image/keyboard_digits.png out/volt/www/image/
cp ../www/image/keylayout.png out/volt/www/image/
cp ../projects/adventureboard/icons/iconTemplate.png out/volt/resources/
cp ../projects/adventureboard/icons/splashTemplate.png out/volt/resources/


# release
configValues
releaseVersion
pushd out/
VoltFile=voltbuilder_adventureboard_release
rm $VoltFile.zip
zip -r $VoltFile volt/*
popd

# debug
cp ../resources/volt/voltbuilder.json out/volt/voltbuilder.json
configValues
debugVersion
pushd out/
VoltFile2=voltbuilder_adventureboard_debug
rm $VoltFile2.zip
zip -r $VoltFile2 volt/*
popd
