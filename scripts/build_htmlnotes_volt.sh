
releaseVersion(){
  sed -i 's/RELEASE/release/g' out/volt/voltbuilder.json
}
debugVersion(){
  sed -i 's/RELEASE/debug/g' out/volt/voltbuilder.json
}
configValues(){
  sed -i 's/APPNAME/HTMLNotes/g' out/volt/config.xml
  sed -i 's/PACKAGENAME/com.htmlnotessupport.HtmlNotes/g' out/volt/config.xml
  sed -i s/VERSIONCOUNTER/$(date '+%Y%m%d')/g out/volt/config.xml
  sed -i 's/MINORVERSION/1/g' out/volt/config.xml
  sed -i 's/APPDESC/Offline HTML Editor with demos/g' out/volt/config.xml
  sed -i 's/APPNAME/HTMLNotes/g' out/volt/www/index.html
  sed -i 's/APPDESC/Offline HTML Editor with demos/g' out/volt/www/index.html
  sed -i 's/APPFILENAME/htmlnotes/g' out/volt/www/index.html
}

mkdir -p out
rm -rf out/volt
cp -r ../resources/volt out/

# copy extra files
cp ../projects/htmlnotes/htmlnotes.html out/volt/www/
mkdir -p out/volt/www/image/
cp ../projects/htmlnotes/img/iconTemplate.png out/volt/resources/
cp ../projects/htmlnotes/img/splashTemplate.png out/volt/resources/

#release
configValues
releaseVersion
pushd out/
VoltFile=voltbuilder_htmlnotes_release
rm $VoltFile.zip
zip -r $VoltFile volt/*
popd

#debug
cp ../resources/volt/voltbuilder.json out/volt/voltbuilder.json
configValues
debugVersion
pushd out/
VoltFile2=voltbuilder_htmlnotes_debug
rm $VoltFile2.zip
zip -r $VoltFile2 volt/*
popd
