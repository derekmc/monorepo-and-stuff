

// objects are hard to write, because you are
// creating a library or API,
// it is easy to have excessive features or too few.

function Mouse(Listener, _x0, _y0){

  // state 
  let Pos = [0, 0];
  let DownPos = null;
  let ClickPending = false; // single click or double click
  let Dragging = false;
  let LastDown = 0;
  let LastUp = 0;
  let Origin = [_x0, _y0];
  let Paused = false;

  const DragDist = 10;
  const ClickDelay = 200; // for single click, doubleclick
  const LongClickDelay = 500;

  // virtual event handlers
  let EventHandlers = {
    "mousedown" : [],
    "mousemove" : [],
    "mouseup" : []
    "singleclick" : [],
    "doubleclick" : [],
    "longclick" : [],
    "drag" : [], // (x, y, x0, y0)
    "dragstart" : [],
    "dragend" : [],
  }

  register(Listener);

  let X = {};
  X.getPos = ()=> pos.slice(0)
  X.updateOrigin = updateOrigin;


  return X;

  function getPos(){
    return Pos.slice(0);
  }

  function updateOrigin(_x0, _y0){
    Origin[0] = _x0;
    Origin[1] = _y0;
  }

  function register(Listener){
    Paused = false;
    Listener.addEventListener("mousedown", mousedown);
    Listener.addEventListener("mousemove", mousemove);
    Listener.addEventListener("mouseup", mouseup);
  }
  function addEventListener(name, handler){
    let handler_list = EventHandlers[name];
    if(!handler_list) return;
    handler_list.push(handler);
  }

  // remove event Listeners
  function pause(){
    Paused = true;
    Listener.removeEventListener("mousedown", mousedown);
    Listener.removeEventListener("mousemove", mousemove);
    Listener.removeEventListener("mouseup", mouseup);
  }

  function resume(){
    register(Listener);
  }

  function mousedown(e){
    let x = e.clientX - Origin[0], y = e.clientY - Origin[1];
    let now = Date.now();

    DownPos = [x, y];
    LastDown = now;
    Dragging = false;
    let params = { x, y, now };
      
    if(ClickPending){
      ClickPending = false;
      triggerEvent("doubleclick", params);
    } else {
      ClickPending = true;
      setTimeout(singleClickTimeout, ClickDelay, params); 
    }

    console.log("mousedown", x, y);
  }

  function mouseup(e){
    let x = e.clientX - Origin[0], y = e.clientY - Origin[1];
    let now = Date.now();
    let x = e.clientX, y = e.clientY;
    console.log("mouseup", x, y);
    Dragging = false;
    DownPos = null;
    LastDown = now;
  }

  function mousemove(e){
    let x = e.clientX, y = e.clientY;
    console.log("mousemove", x, y);
  }

  function triggerEvent(eventName, params){
    if(Paused) return;
    let handler_list = EventHandlers[eventName];
    if(!handler_list) return;
    for(let i=0; i<handler_list.length; ++i){
      let handler = handler_list[i];
      setTimeout(handler(params), 0);
    }
  }


  function singleClickTimeout(params){
    // mouse must be released for a singleclick
    if(DownPos || Dragging) return false;
    if(!ClickPending) return false;
    triggerEvent("singleclick", params)
  }
}
