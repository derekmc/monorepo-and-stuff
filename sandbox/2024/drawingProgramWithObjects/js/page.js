
function Page(container){
  container = container ?? document.body;

  let X = {};
  const Margin = 5;
  const Border = 2;

  register();
  X.render = render;
  X.clear = clear;
  X.getCanvas = getCanvas;

  render();

  return X;

  function register(){
    window.addEventListener("load", render);
    window.addEventListener("resize", resize);
  }


  function clear(){
    container.innerHTML = "";
  }

  function getCanvas(){
    return document.querySelector("#canvas0");
  }

  function render(){
    let margin = Margin - Border;
    container.innerHTML = `
      <canvas id='canvas0'></canvas>
      <style>
        * { margin: 0; overflow: hidden; }
        body{ padding: ${margin}; }
        #canvas0{ border: ${Border}px solid black; }
      </style>
    `
    resize();
  }

  function resize(){
    let w = window.innerWidth;
    let h = window.innerHeight;
    console.log("resize", w, h);
    let canvas = getCanvas();
    canvas.width = window.innerWidth - 2 * Margin;
    canvas.height = window.innerHeight - 2 * Margin;
  }
}
