




function Drawing(canvas){
  //objects vs datastructures
  // objects: data ownership, code reuse, execution division.
  // datastructures: data organization, saving data.

  let windows = [{x0: 0, y0: 0, zoomPower: 0}]; // {x0, y0, zoomPower}
  let strokes = []; // {parent, points: [], styleIndex, windowIndex}
  let styles = [{lineWidth: 2, color: "black"}]; // {lineWidth, color}

  let X = {};

  X.draw = draw;
  X.clear = clear;
  X.addStroke = addStroke;
  X.addWindow = addWindow

  return X;

  function draw(){
    let ctx = canvas.getContext("2d");
    ctx.fillRect(30, 50, 20, 40);
  }
  function clear(){
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  function addStroke(points){
    strokes.push({
      points: points,
      windowIndex: windows.length - 1,
      styleIndex: styles.length - 1,
    })
  }

  function addStyle(style){
    styles.push(style);
  }

  function addWindow(win){
    windows.push(win);
  }
  // funciton load(json){
  // }
  // function dumpjson(){
  // }
}
