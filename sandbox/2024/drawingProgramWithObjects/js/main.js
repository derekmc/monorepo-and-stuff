
window.addEventListener("load", main);

// so much easier with objects.
function main(){
  let page = Page(document.body);
  let mouse = Mouse(window);
  let drawing = Drawing(page.getCanvas());

  drawing.draw();
  setTimeout(()=> drawing.clear(), 5000);
}
