
let ls = (data, template, separator) =>
  data.map(template?? (x=>x)).join(separator ?? " ");

let squares = [1, 2, 3, 4, 5, 6, 7];
let square = x => `<button onclick='alert(${x*x});'> x = ${x} </button>`;
let nl = "<br>\n"

let src = `
<h1> What is the square of x? </h1>
${ls(squares, square, nl)}
<hr>
`

console.log('Content-type:text/html');
console.log('');
console.log(src);

