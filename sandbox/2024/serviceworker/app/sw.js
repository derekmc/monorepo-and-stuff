
const Cache = "demo-cache-001";

const Urls = [
  "./",
  "./css/basic.css",
  "./js/main.js",
  "./main.html",
]
self.addEventListener("install", (event) => {
  event.waitUntil(
    caches.open(Cache).then((cache)=>{
      console.log("opened cache");
      return cache.addAll(Urls);
    })
  );
})

/*
*/
self.addEventListener("fetch", (event) => {
  event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(Cache).then((cache)=>{
        return cache.put(event.request, response.clone()).then(()=>{
          return response;
        })
      })
    }).catch(() =>  {
      return caches.match(event.request);
    })
  )
})
