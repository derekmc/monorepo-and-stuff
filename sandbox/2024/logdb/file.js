
const fs = require("fs");

// file helper functions.
module.exports = {

  mkdir : (dirname, silent) => {
    return new Promise((ok, fail) => {
      fs.mkdir(dirname, (err)=>{
        return !err || silent? ok() : fail(err); })})},

  rmfile : (filename, silent) => {
    return new Promise((ok, fail) => {
      fs.unlink(filename, (err)=>{
        return !err || silent? ok() : fail(err); })})},

  mvfile : (oldname, newname, silent) => {
    return new Promise((ok, fail) => {
      fs.rename(oldname, newname, (err)=>{
        return !err || silent? ok() : fail(err); })})},

  readfile : (filename, silent) => {
    return new Promise((ok, fail) => {
      fs.readFile(filename, "utf8", (err, data) => {
        data = data ?? "";
        return !err || silent? ok("" + data) : fail(err); })})},

  appendfile : (filename, silent) => {
    return new Promise((ok, fail) => {
      // TODO fs.
    })
  },

  writefile : (filename, contents, silent) => {
    return new Promise((ok, fail)=>{
      fs.writeFile(filename, contents, "utf8", (err) => {
        if(err) return fs.truncate(filename, 0, (err2)=>{
          if(err2) return silent? ok() : fail(err2);
          fs.writeFile(filename, contents, "utf8", (err3)=>{
            return !err3 || silent? ok() : fail(err3); })});
        ok(); })})},

}

