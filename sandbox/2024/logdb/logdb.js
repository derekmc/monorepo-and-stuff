
// LogDB is a "logging" database.
// each time
//let fs = require("fs");

const { mkdir, rmfile, mvfile, readfile, writefile } = require("./file");
const { parse, assert, isInt, inBounds } = require("./util");
const { MaxLogLen, DefaultDir, DefaultDB, BackupExtension } = require("./const");

module.exports = LogDB;

function checkRef(ref){
  assert(ref.hasOwnProperty("table"), "LogDB: reference missing table.");
  assert(ref.hasOwnProperty("key") || ref.hasOwnProperty("index"),
    "LogDB: reference does not have 'key' or 'index' property.");
}

async function LogDB(dirname = DefaultDir, dbname = DefaultDB){
  let _jsonfile = `${dirname}/${dbname}.json`;
  let _logfile = `${dirname}/${dbname}.log`;
  //let append
  let _tabledata = {};
  let _tablekeys = {};
  let _changes = [];
  let db = {};
  db.set = dbset;
  db.get = dbget;
  db.flush = dbflush;

  await mkdir(dirname, true);
  await load();

  return db;

  // TODO on every load, write database to newfile, then move it
  // to replace the old file.
  async function rewrite(){
    let ext = BackupExtension;
    await Promise.all([
      mvfile(_jsonfile, _jsonfile + ext, true),
      mvfile(_logfile, _logfile + ext, true),
    ]);
    let obj = { tabledata: _tabledata, tablekeys: _tablekeys};
    let s = JSON.stringify(obj, null, 1);
    await writefile(_jsonfile, s);
    _changes = [];
  }
  function applyChanges(changelog){
    for(let i=0; i<changelog.length; ++i){
      let [ref, value] = changelog[i];
      checkRef(ref);
      dbset(ref, value);
    }
  }
  async function load(){
    try{
      let [json, changetext] = await Promise.all([
        readfile(_jsonfile, true),
        readfile(_logfile, true)
      ]);
      let obj = parse(json);
      let changelist = [];
      if(changetext){
        changelist = changetext.split("\n").map(parse);
        // console.log("changelist: " + JSON.stringify(changelist, null, 1));
      }


      // TODO load _logfile as well.
      // let changelog = (await readfile(_logfile)).split("\n").map(s=>JSON.parse(s));
      applyChanges(changelist);

      _tabledata = obj.tabledata ?? {};
      _tablekeys = obj.tablekeys ?? {};
      _changes = [];

      await rewrite();
      // let outobj = { tabledata: _tabledata, tablekeys: _tablekeys};
      // let s = JSON.stringify(outobj, null, 1);
      // await writefile(_jsonfile, s);

    } catch(e){
      console.error("database format error");
      console.error("error: ", e);
    }
  }

  // get/set are synchronous, but flush is asynchronous.
  async function dbflush(){
    // Todo append to log file instead.
    if(_changes.length > MaxLogLen){
      let obj = { tabledata: _tabledata, tablekeys: _tablekeys};
      let json = JSON.stringify(obj, null, 1);
      await Promise.all([
        mvfile(_jsonfile, _jsonfile + BackupExtension, true),
        mvfile(_logfile, _logfile + BackupExtension, true),
      ]);
      await writefile(_jsonfile, json);
    }else {
      let changelist = _changes.map(x=>JSON.stringify(x)).join("\n");
      await mvfile(_logfile, _logfile + BackupExtension, true),
      await writefile(_logfile, changelist);
      _changes = [];
    }
  }

  // key is ignored if index is present.
  function dbget({table, index, key}){
    let tablelist;
    let keymap;
    if(_tabledata.hasOwnProperty(table)){
      assert(_tablekeys.hasOwnProperty(table),
        `no tablekeys for '${table}'`);
      tablelist = _tabledata[table];
      keymap = _tablekeys[table]; 
    } else {
      tablelist = _tabledata[table] = [];
      keymap = _tablekeys[table] = {};
    }


    if(!_tabledata.hasOwnProperty(table)){
      return null;
    }
    tablelist = _tabledata[table];
    let hasIndex = (index !== null && index !== undefined);
    let hasKey = (key !== null &&
      key !== undefined && true);
      // keymap.hasOwnProperty(key));

    // console.log('Keymap: ' + JSON.stringify(keymap));
    // console.log(`index: ${index}, key: ${key}`);
    assert(hasIndex || hasKey, "LogDB.get: no index or key");
    if(hasKey){
      if(hasIndex){
        assert(keymap[key] == index, "LogDB.get: index did not match provided key.");
      }
      index = keymap[key];
    }
    // console.log(_tabledata);
    // console.log("index: " + index);
    return tablelist[index - 1];
  }

  /*
  // TODO check index is valid and inbounds.
  db.keyLookup = ({table, key})=>{
    let keymap =
      _tablekeys[table] =
      _tablekeys[table] ?? {};

    let result = {table, key};
    result.index = keymap[key];

    return result;
  }

  db.keyAssign = ({table, index, key}) => {
    let keymap =
      _tablekeys[table] =
      _tablekeys[table] ?? {};

    if(Array.isArray(key)){
      for(let i=0; i < key.length; ++i){
        let k = key[i];
        keymap[k] = index;
      }
    } else {
      keymap[key] = index;
    }
    return;
  }
  */

  // key is optional, and key may only be set if not already.
  function dbset({table, index, key}, value){
    let tablelist;
    let keymap;
    if(_tabledata.hasOwnProperty(table)){
      assert(_tablekeys.hasOwnProperty(table),
        `no tablekeys for '${table}'`);
      tablelist = _tabledata[table];
      keymap = _tablekeys[table]; 
    } else {
      tablelist = _tabledata[table] = [];
      keymap = _tablekeys[table] = {};
    }

    let hasIndex = (index !== null && index !== undefined);
    let hasKey = (key !== null && key !== undefined);

    if(!hasIndex && hasKey){
      // use index if key exists.
      if(keymap[key] !== undefined){
        index = keymap[key];
        hasIndex = true;
      }
    }

    if(!hasIndex){
      tablelist.push(value);
      index = tablelist.length;
    } else {
      assert(isInt(index), "Index not an integer");
      assert(inBounds([1, tablelist.length + 2], index), "Index out of bounds");
      // TODO check key matches.
      if(!table in _tabledata){
        tablelist = _tabledata[table] = [];
      }
      tablelist[index - 1] = value;
    }
    if(hasKey){
      // console.log(`key: ${key}, index: ${index}`);
      // console.log("keymap: " + JSON.stringify(keymap));
      keymap[key] = index;
    }

    _changes.push([{table, index, key}, value]);
    let result = {
      table, key, index
    }
    return result;
  }

}
