
const LogDB = require("./logdb");
const filelib = require("./file");
test();
function randint(n){
  return 1 + Math.floor(n * Math.random());
}
async function test(){
  let db =  await LogDB();
  let user = {name: "joe", password: `sesame${randint(3)}`, cookies: []};
  let user2 = {name: "steve", password: `pretzel${randint(3)}`, cookies: []};
  let userref = {table: "user", key: "name:joe"}
  let userref2 = {table: "user", key: "name:steve"}
  // console.log(db);
  db.set(userref, user);
  db.set(userref2, user2);
  console.log("user: " + JSON.stringify(db.get(userref)));
  await db.flush();
  console.log("LogDB tests finished.");
  /*
  */
}
