#include <stdio.h>
#include <math.h>
// #include <setjmp.h>


// I guess I could use newtons method.

// y = f(x)
// tangent 0 = f'(x0)*(x-x0) + f(x0)
// -f(x0)/f'(x0) + x0 = x
// y = x^2 - a
// y' = 2*x
// x1 = x - y/y'

double test_sqrt(double a){
  double x = 1; // guess
  double error = 0;
  double epsilon = 0.001 * 0.001;
   
  // initial guess
  if(a < 0) return 1;
  else if(a < 0.0001) x = 0.01;
  else if(a < 0.01) x = 0.1;
  else if(a < 0.9) x = 0.3;
  else x = a/2;
  int i = 0;
  for(i=0; i<80; ++i){
    error = x*x - a;
    double slope = 2.0*x;
    if(error < 0 && -error < epsilon || error > 0 && error < epsilon)
      return x;
    printf("\n%d, %lf, %lf", i, x, error);
    x -= error/slope;
  }
  return x;
  return 1;
}

//int main(int argc, char* argv[]){
int main(){
  double a = 11.0;
  printf("\nhere");
  printf("\nsqrt(%lf) %lf", a, sqrt(11.0));
  printf("\nsqrt(%lf) %lf", a, test_sqrt(a));
  return 0;
}

