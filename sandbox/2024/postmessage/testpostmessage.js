
let outerurl = "http://localhost:8080/";

let innersrc = `
  <h3> Inner Page Here! </h3>
  <button id="sendbtn" onclick="sendMessage();"> Send Message </button>
  <script>
    window.addEventListener("message", onMessage);

    function onMessage(event){
      console.log("inner received message", event.data);
    }
    function sendMessage(){
      let data = {"pet": "Space Tiger Cat!"}; 
      let payload = JSON.stringify(data);
      window.parent.postMessage(payload, "${outerurl}");
      console.log("message sent", data);
    }
  </script>
`

let src = `
  <iframe id='embedded'></iframe>
  <style>
    #embedded{
      border: 2px solid black;
      width: 600px;
      height: 400px;
    }
  </style>
`

function init(){
  document.body.innerHTML = src;
  let iframe = document.querySelector("#embedded");
  iframe.srcdoc = innersrc;
  console.log("init");
  setTimeout(()=>{
    iframe.contentWindow.postMessage("coolbeans", "*"); }, 1000);
  window.addEventListener("message", (e)=>{
    alert("Message received: " + e.data);
  })
}

init();
