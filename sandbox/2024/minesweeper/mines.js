


const MinesMain = (()=>{


  function main(){
    console.log("mines main init.");
    let ui = { canvas: document.querySelector("#cc") }
    let state = {}

    draw(ui, state);
  }

  function draw(ui, state){
    let ctx = ui.canvas.getContext("2d");
    for(let i=0; i< 10; ++i){
      let x = 300 * Math.random();
      let y = 300 * Math.random();
      ctx.fillRect(x, y, 30, 30);
    }
  }

  return main;
})()
