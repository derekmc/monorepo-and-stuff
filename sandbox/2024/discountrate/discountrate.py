# an example of why you should think
# of financial returns as an upward correction
# for underpriced assets, and not an 
# intrinsic self-reproducing yield.
investments = [
  [-3.0, 200], # [rate of return, capacity at t0]
  [-2.0, 200], 
  [-1.0, 200], 
  [0, 200],
  [1.0, 200],
  [2.0, 200],
  [3.0, 200],
  [4.0, 200],
  [5.0, 200],
  [6.0, 200],
]

time_horizon = 10 # years

def calc_discount_rate():
    global investments, time_horizon
    final_global_wealth = 0 # the final wealth of global wealth portfolio
    initial_global_wealth = 0
    for x in investments:
        rate = x[0]
        principle = x[1] 
        final_value = principle * (1 + rate/100)**time_horizon
        print("final_value: ", final_value)
        initial_global_wealth += principle
        final_global_wealth += final_value
    growth_ratio = final_global_wealth/initial_global_wealth
    print("growth ratio: ", growth_ratio)
    discount_rate = growth_ratio ** (1/time_horizon) * 100
    return discount_rate

discount_rate = calc_discount_rate()
print("discount rate: ", discount_rate)
