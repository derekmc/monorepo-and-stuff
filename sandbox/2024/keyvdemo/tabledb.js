
const Keyv = require('@keyvhq/core')

module.exports = tabledb;
function assert(test, msg){
  if(!test) throw new Error(msg);
}

function tabledb(dbparams){
  let store = new Keyv(dbparams);
  let makekeykey = (table, key) => `:keyref:${table}:${key}`;
  let makeitemkey = (table, index) => `${table}:${index}`;
  let makecountkey = (table) => `:count:${table}`;
  let db = {};

  db.get = async ({table, index, key}) => {
    if(key){
      let keykey = makekeykey(table, key);
      let keyindex = await store.get(keykey);
      if(index != undefined){
        assert(index === keyindex, "tabledb.get: index/key mismatch");
      } else {
        index = keyindex;
      }
    }
    let itemkey = makeitemkey(table, index);
    console.log('get itemkey', itemkey);
    return await store.get(itemkey);
  }
  db.set = async ({table, index, key}, value) => {
    let countkey = makecountkey(table);
    let count = await db.get(countkey);
    if(!count) count = 0;
    if(key != undefined){
      let keykey = makekeykey(table, key);
      let keyindex = await store.get(keykey);
      if(index != null){
        assert(index === keyindex, "tabledb.get: index/key mismatch");
      } else {
        await store.set(keykey, keyindex);
        index = keyindex;
      }
    } else {
      index = count + 1;
    }
    let itemkey = makeitemkey(table, index);
    console.log('set itemkey', itemkey);
    await store.set(itemkey, value);
    return index;
  }

  return db;
}
