
const demo = require('./demo-common')
const KeyvSqlite = require('@keyvhq/sqlite')

main()
async function main(){
  let config = {store: new KeyvSqlite('sqlite://test.sqlite')}

  await demo(config);
  await demotable(config);
}
