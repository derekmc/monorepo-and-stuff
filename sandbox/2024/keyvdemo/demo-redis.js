
const demo = require('./demo-common')
const demotable = require('./demo-table')
const KeyvRedis = require('@keyvhq/redis')

let config = {store: new KeyvRedis('redis://127.0.0.1:6379')}
//demo(config);
demotable(config);

