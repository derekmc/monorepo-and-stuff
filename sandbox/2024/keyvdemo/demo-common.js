
const Keyv = require('@keyvhq/core')

module.exports = demo;

async function demo(dbparams){
  const db = new Keyv(dbparams);

  db.on("error", err => console.log("Connection error", err))

  await db.set("test", "Hello!!!"); 
  let count = await db.get("counter");
  if(!count) count = 0;
  ++count; 
  await db.set("counter", count);
  console.log(`counter: ${count}`);
  console.log("value of 'test': " + await db.get("test"));
  console.log("done");
  process.exit(0);
}
