# Responding to Per Bylund's "How to think about the economy"

For those of you familiar with Per's work, or austrian economics
in general, it should come as no surprise that the focus of analysis
is unapologetically on individuals.

Having been familiar with the austrian school from the time I first
began learning about economics, I understand the attractiveness
of this approach. It makes the answer to many issues clean and clear
cut, and by establishing a rigid moral framing, it cuts through much
of the moral tepidity and political vascilation that common.

But does this straightforward property centered morality
and individually focused analysis have inherent merit? 

I would make only one suggested correction for Per,
instead of how to think about the economy, it
may be better titled: "How to think like a
person", or "How to think about people",
because people is what the story is really all about.

# What is a person?

Like I said, that is really the only correction I have
to offer Per.  In the rest of this book I will spend
my time not so much directly about what he wrote,
 but me presenting my own thoughts
using his ideas as a starting point. Thus it is
a map for how to get from one philosophy to another.

# The good:
Per brings a modern approachable writing style to
a long legacy of commentators expounding
the austrian school's concepts of economics,
by consolidating many ideas 
into a concise yet accurate(to that viewpoint)
presentation. It avoids getting mired down
too much in the philosophical dilemmas
of the study of economics, while still
acknowledging many important ideas and issues.

# The questionable:
Per seems to have a strange notion of
the idea of laws, especially laws of economics,
that seems to reduce
all contention to the issue of indeterminate ontological status.
In other words, the only reason why we don't know what
is going to happen, is when we might not know what
a thing really is. Using this ontological determinism,
applied especially through the action axiom, every
principle of knowledge or claim of truth, becomes
pure application of tautology.  No matter what
is in the real world, the laws hold, we just
need to know which laws to apply. In many ways, this is what 
makes Per's task of writing and describing
his viewpoint so straightforward and also approachable.


# My viewpoint:
I do not have any kind of inclination to 
adopt a frame of ontological determinism,
because I think that taxonomies are messy and goofy
abstractions that we apply to generate a mental
model of the world, through which we attempt to
recreate a theater of reality in muddy clay-like
mental structures.

The classic example of this is plato's cave.
Shadows, as we know are elusive objects
that seem to change without rhyme or reason,
or limitation. They can grow big, shrink
small, or simply disappear, and are
not limited to sub light-speed travel,
making a shadow a very perplexing entity.

If there is such a universal and determined
ontology-- how I would describe Per's
viewpoint-- it certainly cannot be achieved
through the modern lense of physical
essentialism common in scientific discourse:
that objects are purely of a physical nature
interacting through physical laws, and
any higher level assessments are simply our
interpretation tiny "atoms", by that I mean
tiny physical units interacting, whether
those are "atoms", "quarks", "fields", or
whatever other primitive things described
by physics.

Per's view of the economy might still be
shown to hold with strict physical laws, but within
this approach, the physical laws would
be details colored in within the boundaries
of a higher and more determinate ontology,
 and not merely abstractions or interpretations
 we apply to color the world.

In this view, people are people, and molecules
are pieces of people, not that person
is a name we apply to a certain collection
of molecules.

Perhaps physical and ontological essentialism
could be shown to be compatible or even
equivalent. But this does not seem a very easy
task to me, it how one might approach this problem
of relating these two forms of essentialism.

# Essentialism vs Determinism

I am not really a student of philosophy, so 
what I talk about is just my best attempts
to say something coherent with the best
language I can. By comparison, this philosophical
foray might be compared to a newb programmer
writing a very advanced and complex game
using a simple programming laguage like
basic or cobol.  After, all our banking
system runs on messy archaic cobol, so
the comparison is ironically applicable
to discussions of economic interest.

How I would describe the difference
between essentialism and determinism,
is that essentialism describes something's
nature, while determinism describes
its path through time and space.

It is metaphorically the perplexing uncertainty principle
from heisenberg: you can know position or direction.

But maybe we can just describe the path.

So while essentialism is knowing something's
nature, determinism is to know its direction
or path.

Is it accurate to say that physical units
only serve to fill in the details of a
living being that has an essential nature
that we can become familiar with, without
knowledge of all externally identifiable
characteristics?

That is the philosophical issue I see
here.

And I am not one to resign myself
to one side of the fence on this
particular issue.  Maybe
there is a sense in which
people are people, planets are planets
and molecules and atoms are just
fuzzy inconsequential details.

That is the issue.


# Where is the Source of the Law: Is it Light?

Light is a self propagating electromagnetic
perturbation that carries information, a ripple
in the fabric of space, a hiccup in the
boundary we call time.  That is why
light travels at the speed of light:
it is allows physical space to remain
connected, and simultenously what
divides time into a mess of quantum
entangled potentials for outcomes.

The great mystery of quantum physics
is that it appears to embrace neither
determinism nor pure randomness, but
rather indeterminism. Just like
a child is both a rejection and reflection
of the character of parents and environment,
each knew moment of time for the world is
neither set instone, nor completely disconnected
from environmental events.

# The potentials in quantum states 

So Per's obsession with economic laws as a
pure and essential phenomenon, may seem
to require an embrace of "chartalism",
ie money as legal and institutional
mechanism for relating. After all,
if economic law is 100% certain,
why cannot legal laws approach this
same perfection in application? And wouldn't
they do this if we have all layers
of essentialism: individual, atomic, institutional,
and legal? Why should the human unit be more
essential than the unit of a country.
Why should aliens act different from
people. Why should ant colonies not
be an economy with price and markets?

I think this issue cuts to the heart of
what laws really are. And which Is why
I also insist on using the phrase "rules"
 in the place of "laws", when they are
 a more nuanced matter of application,
and not unambiguous projectiles of fate,
always dictating a specific target 
as the outcome.

But just like quantum indeterminism
in nature, don't laws retain some
mystery to their application? Need
the application of economic laws always
be certain, or can it be uncertain
and a whole connection of loosely
coupled mathematically related
potentials??

Isn't the mystery of quantum states
the same thing? Can we apply both
kinds of laws to create seemingly
both strict physical paths and
behaviors, but which result in 
ambiguous outcomes. Isn't
the legal system entirely how
we negotiate these uncertainties
and compromise on realities?

### That's my thoughts for now.

How should you think about the economy?
However you think people create the
world as a reflection of who they are.

In a sense, per's ideas can
be a positive method for self assurance
and reinforcement, allowing people to 
take on difficult or impossible
physical tasks by acting
with confidence and conviction. I don't really know.
But that is a different matter from if those ideas
are non-negotiable truths. and if adjustments ever need
to be made.

