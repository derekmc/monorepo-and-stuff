# Economics: The Science That Got Ahead of Itself

The relationship between scientific research and economics is not
a new topic, and many authors from Steve Keen to many others, have
discussed these ideas in depth: physics envy, realistic models,
and limiting scope of inquiry(economists often feel the need to weigh
in on all sorts of scientific and technological issues outside their
domain whether climate change or future technologies).

Today I just want to describe a little bit what the consensus in
modern economics looks like, and why it exists. Many heterodox people,
whether progressive or regressive austrian, work outside of this consensus
to develop and discuss foundational ideas to the discipline.

## How Scientific Domains Develop Consensus

Firstly, let's discuss how scientific domains develop consensus and what that
looks like. The comparison I would make is to that of a sport or game or skill.
Consider skateboarding. While skateboards can look very different, generally
you have a board with four wheels that the rider stands on without attachments.
While three points of contact with a surface are sufficient for stability, skateboards
often have "trucks", which allow the rider to steer by leaning to one side of the board
or the other.  Thus four wheels are needed so that you can lean to steer and still
maintain stability traveling in one direction or the other.

Skateboarding has developed its own language for tricks. I would like to include
a citation, but given it is not a typical academic discipline, it may take 
a while before I get to revising this. There is a clip I recall seeing of Rodney
Mullen discussing this idea, that skateboarders have an international language
for what they do, and that it can be used to describe tricks that haven't even 
been performed before.

So this is the fundamental way that consensus develops in these disciplines.
You have many people practicing a skill or art, and as they develop demonstrable
expertise, they begin to recognize some common principles.  To continue with
the skateboard example, there are basically 4 ways to stand on a skateboard and ride
it: regular, switch, nollie, and fakie.

These 4 terms may sound unusual to someone not familiar with skateboarding, but the
language actually tells us a lot about the history and culture of skateboarding.

First, let's discuss the term "fakie". This term is actually shared in different
"action sports", such as bmx. It basically means just to ride backwards.  In skateboarding
as the rider typically assumes a sideways stance, riding fakie doesn't necessarily look backwards,
but in the process of learning it, you have to learn how to balance on your board differently.

Becase these four board stances dramatically change the technique you use to balance and control
the skateboard, many tricks can be prefixed or suffixed with one of these, to indicate which
stance was used when the trick was performed. "Nollie" in particular, comes from the phrase "nose ollie",
indicating that this stance wasn't cemented as a formal idea until after the ollie was developed.

To someone who doesn't skateboard, it might be suprising that each of these four stances are dramatically
different, and you can be proficient in one stance, but look like a drunk kangaroo if you try to ride
in another stance. (kangaroos would be certainly impressive if they learned to skateboard, given
how powerful their legs are and if they used their tail as a counter balance, but it is hard to imagine
a kangaroo being naturally very coordinated with their gigantic feet and tiny arms, thus the analogy).

So only by doing a skill or art, does one learn the nuances of how it works.

## How to learn Economics In Practice

Many of the early economists were people involved in or interested in politics
or business.  And that is why it gets so sticky. Politics and business are two
of the most subjective and social activities we engage in, so being both
methodological and rigorous while being proficient is difficult: you are
dealing with other people acting according to their subjective experiences
and interests.

Thus, in order to form the modern consensus, economists had to limit what ideas
and principles they talked about, and which ones they wouldn't.

## The Political Consensus of Modern Economics

Economics was modernized from the 1970's onward. Banking and finance were modernized
earlier from the late 19th century to middle 20th century or 1950's.

Much of banking as it was being modernized, was focused on the agriculture practice of farming,
as farming is perhaps the quintessential economic activity involving capital investment:
land tools, seed, machinery, and labor.  If there was a textbook model of the
capital process, farming would be it.  So as farming become modernized, so did finance and
banking.

Why did economics lag behind?

I would attribute this to the fact that economics integrates both business and politics. And
the political system of the "modern" world, whatever that means, didn't get a real chance to
stabilize until after world war 2. After we figured out how to build planes and tanks, and boats
and atom bombs, then the issue of politics became dramatically different.

Before this point politics was the art of dealing with foreigners, after modern warfare,
politics became the art of dealing with your citizens in global world.

All the inventions leading to this point, would affect what politics and economics are
today.

## You Can't Do Economics In A Laboratory

One of the problems with the scientificization of economics, is
you can't really do it in a laboratory.  So it is like skateboarders
stuck learning about their art by playing video games such as
 __Tony Hawk's Pro Skater__. 

 In the rush to have consensus, the economics profession just sort
 of took a selection of existing ideas, and made them official cannon.

 The only problem is, many of these "traditional" ideas, were based on
 bad descriptions or outright fabrications.  We still tend to think
 of money as gold or silver, in media such as video games. We think
 banks store money in their vault, instead of the vault being just
 a buffer or relay between where the money gets printed, and where it
 gets used.

 It was probably a good idea that economics attempted to set
 certain topics as "out of bounds", so that focus and research
 could look elsewhere. But in this case, the boundary is basically
 a swimming pool deck covered in broken glass, and rough unfinished
 concrete that bakes in the sun to burn your feet.  It's time to
 clean up around the edges, and work on these questions that
 are "out of bounds", because they are political in nature.

 ## Is our "Capitalism Obsessed" Politics Just Anti-Marxism?

Many associate Karl Marx with Communism and Socialism. But
it is often pointed out that Marx's most significant work
was __Das Kapital__. Marx put most of his effort into 
describing what capitalism is.  He did this as 
an attempt to hyperbolize and villify a system which
concentrates all economic power in the hands of a few
wealthy and powerful individuals.

Many Americans today think of capitalism as a patriotic
idea and essential to what America is as a country. They
would perhaps be surprised to learn that the word comes from 
french, and it wasn't used in the modern
sense until the 1850s, almost 100 years after the formative period
for the United States.

The idea of Capitalism developed in historical parallel to the abolishment
of slavery and the United State Civil War, not its founding.


If you notice, earlier in this essay I was careful to specificy
"the capital process", rather than use the term capitalism. I would
define the capital process as the activities of investment, record keeping,
allocating and allocating profits, so specifically the activities itself,
and not the associated political ideas and system.

The capital process can be utilized under a broad range of societal norms
and political structures. And separating the politics of it, from the specifics
of the operation of it, is important to discuss both sides clearly.

When Marx wrote Das Kapital he was describing a phenomenon that was
not yet fully developed.  He described a political system that was so
obssessed with the capital process that it became the entire organizing
force of a society.  Rather than calling this hyperbolic and unrealistic,
modern "conservatism" has embraced marxism, or rather they have decided to aspire
to the society that Marx described as a distopian fairy tale.

Ask a modern conservative capitalist, and they would like to claim that they
are 100% free from and Marxian influence. But I think that is completely
wrong. They have embraced marx's distopia, the same way someone might
enjoy the aesthetic of the Hunger Games movies and then decide to have
that be a template for our actual society.

I would consider Marx's version of capitalism to be a fantasy/sci-fi novel.
I'm sure both rabid pro-capitalism types and the unoriginal Marx disciples
will hate me for this.

But the point is, we don't have embrace a society where the capital process
is the totality of our political and social organization just to "own Marx".

Anyway, this point has little to do with modern economics, except to highlight
why having a boundary is so important.  It is good thing that modern economics
tries to delineate between these issues, I simply think that we still need to
pay attention to thing on teh boundary and not simply dismiss them.

This boundary is money creation and social regulations.

## What Markets Actually Do

Because of the modern obsession with Marx's boogeyman, it is hard to talk
about, in a reasonable way, what markets actually do.  I think the
best way is to simply imagine a swap meet.

A swap meet is an organized event where people come together to buy and
sell a bunch of stuff. It's not intend to be a total organizing force
for all of society and every political and legal question. That would
be absurd.

Swap meets are not an every day event. And that is entirely the point.
They are a special unique day or event so that people can do this
activity all at once, and therefore as efficiently and quickly
as possible.  In this sense, it is much like any other convention
around a special interest.

So whereas some people think markets should be the organizing force
for all of economic life, I see it like a job fair or swap meet, or 
a draft in a sports season.  Yes, having freedom to choose teams and
trade wares is important, but most of our time is spent either on
a particular team or using our wares, not trading them.

In particular, capital markets are even more of a special event
that even fewer people participate in.  And in my opinion, that's
why they need political checks and balances. Because everyone's
life is effected by the decisions made in captial markets, yet
so few people directly are involved, they need to be regulated
and operate within public laws.

## The Modern Economic Consensus

The thing perhaps that annoys me the most about the modern
economics consensus, is not necessarily even their conclusions:
free international trade, public austerity, and regulation in
the interests of preserving capital markets. No, the thing that
annoys me the most is the language and scope they use for discussing
these ideas.

Modern economics talks about convexity of preferences with little emphasis on the
more general problem of mathematical search and optimization.  Convexity is
a useful property in mathematical search and optimization because it allows
you to draw boundaries where a possible solution might be, not that we
should always assume that it applies to particular variables.

Modern economics talks about price curves even when sellers and buyers do
not have different criteria for setting price, such as is the case with speculative
trading. Sellers prefer higher prices and buyers prefer lower prices, but when
you are trading assets based on speculation, everyone is just basing their
price on what they think the future price will be.  In this case, there is
no reason to use a different model for the demand side or the supply side, just
because buyers and sellers both prefer a price in their favor, does not mean
they are using different information or criteria for setting those prices.

Modern economics treats class issues as purely a specialization issue.  Some
people are computer programmers, and some people wash windows on cars. This outcome
represents some optimal allocation and not a division of power, according to
modern economic consensus. They describe any misallocation as "elite overproduction",
not working class undercompensation.  This is obviously a ridiculuous joke because
abstract jobs like programmer or accountant are only useful as long as someone is
doing physical work, and they primarily serve to help us better organize that physical
work, they cannot replace it.

So the issues of labor allocation and compensation are distorted by the generous returns
to capital.

I could go on and on.  But the bias in economics is not just from wrong conclusions,
in most cases, the conclusions are valid given the limited framing.  It is from
a limited scope of acceptable discourse, and everything outside that scope is treated as
tradition without acknowledgement.




