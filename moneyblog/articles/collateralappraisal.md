# Collateral Appraisal Controls Macro Credit and Determines the Value of Money
### An Old Idea deserves new Acknowledgement


## Cullen Once Again Antagonizes MMT

I was recently reading some tweets by Cullen Roche, pronouncing the death of 
MMT and the decisive success of our current 
monetary policy.  What bothers me so much about these
tweets, is not that Cullen disagrees with or wishes to criticize MMT, but that
the arguments which he puts forward are generally both lazy and invalid.  He relies
on conventional wisdom and appeal to authority, rather than making neutral
first principles arguments.

To be fair, most of the smartest and most respectable economists and financial
experts either openly disagree with, or rigorously oppose MMT. So this is not
a new thing.  This includes many economists that I greatly respect and admire,
including Perry Mehrling, Miles Kimball, Jeff Snider, Claudia Sahm, Ann Petifor,
George Gammon, and many more.  For me personally, I see each of these people
as experts in a specific domain, and so I can appreciate their expertise and
accomplishments, and even recognize specific ways in which they are far more
capable then me, and yet still disagree with them on specific technical points.

MMT is definitely the outsider, and so to make lazy and frankly inaccurate
arguments, is not only unhelpful, but simply looks bad.
In this case, the problem with Cullen's arguments, is that he presents certain
evidence as conclusive and definitive, when it is not sufficient.

I generally get uncomfortable when people tell grand stories with miraculuous
claims. Not that I want to doubt them, only that it seems disingenuous 
when I am not in a position to check of verify what they are saying. This 
is precisely the problem with Cullen's arguments.  To empirically verify the
effectiveness of macro managing monetary policy is not a simple task, and
proclaiming victory based only on current events is not only wrong and invalid,
but asks an audience for support or trust based on somethign they cannot
independently verify themselves.

The ability to empirically evaluate monetary macro policy is well beyond the
ability of casual observers, and even for experts has proven a daunting 
challenge with inconsistent results.
We should recognize that substantial progress has been
made in this statistical based approach to technocratic financial control,
but the inherent difficulty of controlling a complex economic engine 
using a strict empirical model should not be oversimplified.

## Prevailing Assumptions about Monetary Policy, and Why MMT disagrees.

Please read this article on [historical approaches to monetary policy]()
published on the Federal reserve website.

The historical approaches to monetary policy represent a continual retreat
from "conventional wisdom", to a more nuanced and complicated, technically
sound approach.  So far from being pesimisstic, I am exceedingly optimistic
that we are both moving in the right direction, and making significant continual
progress to more effective and better monetary policy.

However, I do not think this process is over quite yet, and it is something
that has been hundreds of years in development, happening in parallel to important
progress in science, technology, engineering, and education.

Much of our knowledge and ideas about monetary policy come from a "banker's view",
so it is very important to understand what bankers do.  Bankers are the interface
between a larger political authority and individuals and businesses trying to
survive and make money in a society.  In this role, banks make loans
and conduct accounting, satisfying both the political authority they 
are subject to and assisting the participants or constiuents in that
society to make a living.

So while we who agree with MMT on the most important and substantial questions
of monetary policy, may sound like we are coming out of nowhere with hairbrained
ideas, this is in fact very much a part of the historical conversations about
monetary policy and financial governance.

Becuase bankers interface both up and down: with a political authority and with
individuals and businesses, their role combines both a micro and macro view.
To always assume that we should see the macro level the same as the micro level,
would be a fallacy of composition. Sometimes, an action with one effect on 
micro level, can have the opposite effect when we try to apply this
across the board to everyone. A simple example I like to give is when you
are in an elevator, and it starts ascending, it feels like you are being pulled
down by a stronger gravity. The effect of increasing gravity would pull people
down, but when it is applied to the reference frame, it is because our entire frame
is accelerating upward.

This is the exact problem with contemporary monetary policy. If we try to increase
financial gravity, by increasing interest rates, then individuals may feel like 
they are being pulled downward with greater force, but in reality the entire
reference frame is simply being accelerated upwards.  When we increase
interest rates, money earns more money, diluting its value, unless something
else can anchor that value. The stuff that anchors the value of money is
called collateral.

# Collateral is an Old Idea, but Offers New Insights

The notion of collateral, that debt and promises can be guaranteed with
a specific promise to something, is a timeless notion in finance.

This is illustrated very well by the "bank for poor people", namely pawn
shops.  Pawn shops essentially offer two prices for a loan, one by offering you
a very conservative quote on your pledge, and then again by charging you
a high rate of interest if you want to recover that pledge.

The fact is, collateral appraisal, and interest rates, are two very different
ways to control credit and the cost to acquire money or purchasing power.

Collateral appraisal is the most direct and transparent way to price money,
which is why gold standards were popular for hundreds of years.  Seignorage
was a currency issuer explicitly changing the conversion rate of their token
into precious metal.

# Precious Metals are an Amazing Technology With Useful Properties

This is where we must acknowlege precious metals for what they are,
they have been a foundational technology for society for thousands of
years, both for purposes of craftsmanship, and for trade and accounting.

As recently as 2018, scientific bodies still used physical objects
to define our measurement of mass. From 1889 until 2019, the
"International Prototype of the Kilogram", was the standard accepted defintion
for a basis of measuring mass.

So what about our standard for financial mass or value? Why did we 
reform our standard for measuring physical mass, and is this at all
similar to the reforms we made in defining financial value relative
to physical objects.

I can't do a complete survey of metal based currency throughout
history, but some highlights are FDR, Bretton Woods, 
William Jennings Bryan's "Cross of Gold speech" promoting bimetalism,
the nixon shock, and much more.

The important roles that precious metal played were standardization, verification,
and universalization.  Trade could be conducted in different places, and 
the standards of value were stable over both time and space.
Precious metal as a standard of value were replaced for the same reason
we replaced them as a standard of mass, and this is because of their
limitations and we have discovered more universal 
tools for physical reference.

But this does not mean that physical metals are obsolete or no longer
useful!  The same properties that made physical metal objects the definitions
of units historically, make them still very useful tools for reference
and measurement today.  Just because they are no longer definitions of
units, does not mean we have abandoned them entirely.

## How to measure financial mass

The ability to measure units like mass accurately, consistenly, and repeatably,
across time and space is one of the few unambiguous accomplishments of modern
science.  In parallel our ability to measure financial value has also
improved significantly, and this is primarily through the direct
collaborative approach of regulated markets.

A well regulated market, much like a well regulated militia, is a collection
of people who have agreed to follow a common set of rules and respect
common expectations of integrity and honesty in their dealings.
The most deadly physical object
in our society today is probably the automobile, but we tolerate its widespread
use by common citizens, subject to proper licensing and oversight, because
of its great utility.

Even though guns are involved in more deaths in the U.S. each year,
that is only 

and accidental sense, but 




r test our systems of weights and measures, although there is a desire
to change that to k


Did we just increase gravity 

In the 1900's and before, conventional wisdon


presents
conclusive and definitive evidence, without directly 
conclu
