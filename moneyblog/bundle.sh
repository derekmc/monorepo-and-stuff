for infile in articles/*.md; do
  outfile=${infile#articles/}
  outfile=out/${outfile%.*}.html
  cat template/header.html > $outfile
  npx --yes markdown ${infile} >> $outfile
  cat template/footer.html >> $outfile
done
