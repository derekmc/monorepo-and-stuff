

let appname, appfile, author, iconfile, description;
appname = "Space Triangle"
author = "dm";
iconfile = "";
description = "A simple spacecraft control game.";
appfile = "";

typeof window == "object"? window.addEventListener("load", init) : main();

function main(){
  //console.log(process.argv);
  if(process.argv[2] == "list") list();
  else test();
}

function test(){
}
function list(){
  let path = __filename;
  appfile = path.substring(path.indexOf("appsrc") + 7).replace(/\.js$/, "/index.html");
  console.log(`AppCollection.listApp({` +
    `appname: "${appname}", ` + 
    `author: "${author}", ` + 
    `appfile: "${appfile}", ` + 
    `iconfile: "${iconfile}", ` + 
    `description: "${description}"}) `); 
}



let dt= 1/18;
let [x, y, dx, dy] = [30, 30, 80, 80];
let [firex, firey, firedx, firedy, firespeed] = [-4,-4,0,0,80];
let gas= 5;
let theta_decay= 0.25;
let gas_decay= 0.15;
let speed_decay= 0.85;
let gas_impulse=5;
let recoil_impulse=8; // recoil
let theta_impulse=Math.PI/4;
let len= 20, width= 5;
let theta= Math.PI/12;
let dtheta= Math.PI/10;
let angle= Math.PI/8;
let statusText="";
let statusFrames = 0;
let statusTime = 2.8;
//let focusControlsEnabled = true;
let active_keys = {};


function status(text){
  statusText = text;
  statusFrames = Math.floor(statusTime/dt);
}
function init(){
  document.body.innerHTML = doc;
  status("Welcome to Triangle Space Ship Game.");
  setTimeout(()=> status("Controls: WASD or Arrow Keys to move, Space to Fire"), 1000 * statusTime);
  c0.width = 400;
  c0.height = 400;
  draw();
  setInterval(loop, dt*1000);
  window.addEventListener('keydown', keydown);
  window.addEventListener('keyup', keyup);
  disableFocusControls();
  /*let buttons = document.getElementsByTagName("button");
  for(let i=0; i<buttons.length; ++i){
    buttons[i].addEventListener('mousedown', disableFocusControls);
  }*/
}
function disableFocusControls(){
  //if(!focusControlsEnabled) return;
  //focusControlsEnabled = false;
  //status("Focus controls disabled.");
  let buttons = document.getElementsByTagName("button");
  for(let i=0; i<buttons.length; ++i){
    let button = buttons[i];
    button.setAttribute('tabindex', -1);
    button.addEventListener("focus", preventFocus);
  }
}
function preventFocus(e){
  if(e.relatedTarget){
    e.relatedTarget.focus();
  } else {
    e.currentTarget.blur();
  }
}
function keyup(e){
  delete active_keys[e.keyCode];
}
function keydown(e){
  let code = e.keyCode; //console.log(code);
  if(code in active_keys) return;
  active_keys[code] = true;
  let codes = {
    65: 'left',
    87: 'gas',
    68: 'right',
    37: 'left',
    38: 'gas',
    39: 'right',
    32: 'fire'}
  if(code in codes) action(codes[code]);
}
function draw(){
  let ctx= c0.getContext('2d');
  ctx.lineWidth= 2;
  ctx.fillStyle="#000";
  ctx.strokeStyle= "#fff";
  ctx.lineCap= "round";
  ctx.lineJoin= "round";
  ctx.clearRect(0,0,400,400);
  ctx.fillStyle = "#fff";
  if(statusText.length && statusFrames > 0){
    --statusFrames;
    ctx.fillText(statusText, 10,20);
  }

  let [s1, c1, s2, c2] = [
    Math.sin(theta - angle/2), Math.cos(theta - angle/2),
    Math.sin(theta + angle/2), Math.cos(theta + angle/2),
  ];
  let [x1, y1, x2, y2] = [
    x + len*s1, y + len*c1,
    x + len*s2, y + len*c2,
  ];
  ctx.beginPath();
  ctx.moveTo(x, y);
  //ctx.fillRect(x-2, y-2, 4, 4);
  //ctx.lineTo(60,60);

  ctx.lineTo(x1,y1);
  ctx.lineTo(x2,y2);
  ctx.closePath();
  ctx.stroke();
  ctx.fillStyle = "#fff";
  ctx.fillRect(firex - 1.5, firey - 1.5, 3, 3);
  //ctx.fillRect(30,30,50,50);
}
function loop(){
  x+= dt*dx;
  y+= dt*dy;
  firex+= dt*firedx;
  firey+= dt*firedy;
  theta+= dt*dtheta;
  dtheta*= Math.pow(theta_decay, dt);
  gas*= Math.pow(gas_decay, dt);
  dx -= gas * dt * Math.sin(theta) * len;
  dy -= gas * dt * Math.cos(theta) * len;
  dx*= Math.pow(speed_decay, dt);
  dy*= Math.pow(speed_decay, dt);
  if(x> 400) x= 0;
  if(y> 400) y= 0;
  if(x< 0) x= 400;
  if(y< 0) y= 400;
  draw();
}
function action(act){
  if(act == 'left'){
    dtheta += theta_impulse;
    //theta += theta_impulse;
  }
  if(act == 'right'){
    dtheta -= theta_impulse;
    //theta -= theta_impulse;
  }
  if(act == 'gas'){
    gas += gas_impulse;
  }
  if(act == 'fire'){
    firex = x;
    firey = y;
    firedx = dx - firespeed * Math.sin(theta);
    firedy = dy - firespeed * Math.cos(theta);
    dx += recoil_impulse * Math.sin(theta);
    dy += recoil_impulse * Math.cos(theta);
  }
}

let doc = `
<meta name="viewport" content="width=device-width, initial-scale=1">
<canvas id=c0 style="border: 2px solid black";></canvas>
<div id="controls">
<button onclick="action('left');" title='Left Arrow'> &lt; </button>
<button onclick="action('right');" title='Right Arrow'> &gt; </button>
<button onclick="action('gas');" title='Up Arrow'> gas </button>
<button onclick="action('fire');" title='Space Bar'> fire </button>
</div>`

