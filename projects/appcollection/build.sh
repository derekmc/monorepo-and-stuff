#!/bin/bash

rm -rf build/
mkdir -p build/
echo "" > build/list.js
find appsrc | grep \.js$ | xargs -I '{}' node '{}' list >> build/list.js
find appsrc | grep \.js$ | cut -c8- | sed 's/\.js$//' | xargs -i sh -c 'echo {} && mkdir -p build/{} && cp ./files/apptemplate.html build/{}/index.html && cp appsrc/{}.js build/{}/appscript.js && cp appsrc/{}.css build/{}/appstyle.css 2>/dev/null'
#find appsrc | grep \.css$ | cut -c8- | sed 's/\.css$//' | xargs -i sh -c 'echo {} && mkdir -p build/{} && cp appsrc/{}.css build/{}/appscript.css'

