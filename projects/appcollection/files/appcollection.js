
let AppCollection = {};

(function(){
  let list_template = ({appname, appfile, author, iconfile, description})=>{
    let href = `./build/${appfile}`;
    // todo cards.
    return `
        <table style="margin-left: auto; margin-right: auto;">
          <tr>
            <td rowspan="2">
              <a href="${href}" style="text-decoration: none;">
                <div style="display: inline-block; width: 150px; height: 150px; background: darkblue; color: white; font-size: 20px; text-align: center; line-height: 150px;">
                  <img src="../${iconfile}" alt="${appname}"
                    width="150px" height="150px"
                    style="width: 150px; height: 150px; background: darkblue; color: white;"></img>
                </div>
              </a>
            </td>
            <th> ${appname} by ${author} </th>
          </tr>
          <tr>
            <td> <span> ${description} </span></td>
          </tr>
          <!--<tr>
            <th> Author </th>
          </tr>
          <tr>
            <td> <span> ${author} </span> </td>
          </tr>-->
        </table>
        <!-- <span> <b> ${appname}</b> &emsp; ${description} &emsp; by <b> ${author} </b></span>-->
    `;
  }
  let AC = AppCollection;
  let applist = [];
  AC.listApp = (args) => {
    applist.push(args);
  }

  AC.showList = (container)=>{
    if(!container) container = document.body;
    let htmlsrc = "";
    for(let i = 0; i<applist.length; ++i){
      htmlsrc += list_template(applist[i]);
    }
    document.body.innerHTML = htmlsrc;
  }

})();
