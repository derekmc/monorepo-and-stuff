setTimeout(demo, 1200)
function count(n){
  let a = Array.from(Array(n+1).keys())
  a.shift()
  return a
}
async function demo(){
  console.log("demo")
  console.log("settings", settings)
  settings.options({
    fizz: count(5000),
    buzz: 'abcdefghijklmnopqrstuvwx'.split(''),
    margin: [-50,-40,-30,-20,-10,0,10,20,30,40,50],
  })

  settings.values({
    fizz: '30', buzz:'k', margin: '-10'})
  //settings.labels({fizz: "Choose Fizz", buzz: "Choose Buzz"})
  //settings.updateCallback(console.log)

  settings.show() 
  let result = await settings.result()
  console.log("settings", JSON.stringify(result))
}

