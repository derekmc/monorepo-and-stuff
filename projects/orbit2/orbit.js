
const DATA_KEY = "__orbit2data__";
let data = {};
let ui = {};

window.addEventListener("load", init);

function buttonHandler(event){
  let name = event.target.name;
  actionHandler(name);
}

function actionHandler(name){
  if(name == "selectFormat  "){

  }
  else if(name == "openMenu  "){

  } else{
    console.log("Action: " + name)
  }
}

function initdata(){
  data.text = "Welcome to orbit 2. An embeddable document editor."
}

function initui(){
  ui.edit = document.getElementById("editarea");
  ui.edit.value = data.text;
}

function init(){
    console.log("init");

    if(!load()) initdata();
    initui();
    register();
    setInterval(update, 1000);
}

function register(){
  let buttons = document.getElementsByTagName("button");
  for(let i=0; i<buttons.length; ++i){
    buttons[i].addEventListener("click", buttonHandler);
  }
}

function addWordBreaks(str, maxLength = 4){
  const breakch = "-";
  const words = str.split(/ /);
  const newWords = [];
  words.forEach(function(word) {
    let newWord = ""
    let w = word;
    while(w.length > maxLength){
      const pre = w.substr(0, maxLength);
      w = w.substr(maxLength);
      if(w.substr(0, breakch.length) == breakch){
        w = w.substr(breakch.length);
      }
      newWord += pre + breakch; //"&shy;"
    }
    newWord += w;
    newWords.push(newWord);
  });
   return newWords.join(" ");
}

function save(){
    let str = JSON.stringify(data);
    localStorage.setItem(DATA_KEY, str);
}

function load(){
    let str = localStorage.getItem(DATA_KEY) ?? "";
    try{
      if(!str || !str.length) return false;
      data = JSON.parse(str);
      return true;
    } catch(e){
        return false;
    }
}

function update(){
  //ui.edit.value = addWordBreaks(ui.edit.value);
  data.text = ui.edit.value;
  save();
}