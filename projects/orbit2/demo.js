
window.addEventListener("load", demo);

function count(n){
  let a = Array.from(Array(n+1).keys());
  a.shift();
  return a;
}
function demo(){
  let cfg = ConfigJS();

  cfg.choices({
    fizz: count(5000),
    buzz: 'abcdefghijklmnopqrstuvwx'.split(''),
    margin: [-50,-40,-30,-20,-10,0,10,20,30,40,50],
  });

  cfg.updateCallback(console.log);

  setTimeout(cfg.init, 150);
  // setTimeout(()=>{cfg.destroy();}, 8000);
}
