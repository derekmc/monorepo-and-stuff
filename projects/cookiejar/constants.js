
let x = {};
x.CookieLen = 18;
x.CookieName = "Cookie";
x.keys = {};
x.keys.usersessions = "usersessions|";
x.keys.userlogin = "userlogin|";
x.keys.cookies = "cookies|";
x.keys.appdata = "appdata|";
x.keys.appuserdata = "appuserdata|";

module.exports = x;
