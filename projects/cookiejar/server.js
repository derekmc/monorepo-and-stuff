
let express = require('express');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let nunjucks = require('nunjucks');
let markdown = require('nunjucks-markdown');
let marked = require('marked');
let data = require('./data.js');
let constants = require('./constants.js');

let app = express();
let env = nunjucks.configure(['views/'], {
  autoescape: true,
  express: app,
})

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));


markdown.register(env, marked);

async function session(req, res){
  let cookiename = constants.CookieName;
  let cookie;
  if(req.cookies && cookiename in req.cookies){
    cookie = req.cookies[cookiename];
    return [cookie, await data.userId(cookie)];
  } else {
    let cookie = await data.newSession();
    console.log('cookie', cookie);
    res.cookie(cookiename, cookie);
    //res.setHeader('Set-Cookie', `${cookiename}=${cookie}`);
    return [cookie, cookie];
  }
}
function setCookie(res, cookie){
  let cookiename = constants.CookieName;
  res.cookie(cookiename, cookie);
  return true;
}

app.use(express.static("public"));

app.get('/', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  res.render('index.html', {username});
})


app.get('/cmd', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  res.render('cmd.html', {username});
})

app.get('/about', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  res.render('about.html', {username});
})
app.get('/login', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);
  if(username != "Guest"){
    return res.redirect('/logout');
  }

  res.render('login.html', {username});
})

app.get('/logout', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);
  if(username == "Guest"){
    return res.redirect('/login');
  }

  res.render('logout.html', {username});
})


app.get('/signup', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  res.render('signup.html', {username});
})


app.post('/loginsubmit', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let currentuser = (userid == cookie? "Guest" : userid);

  let {username, password} = req.body;

  //console.log("TODO parse post request", req.body);

  let [newcookie, message] =
    await data.userLogin(username, password);

  let destination = newcookie? "/" : "/login";
  let status = newcookie? "ok" : "error";

  if(newcookie){
    setCookie(res, newcookie);
  } else {
    username = currentuser;
  }

  // TODO set new cookie
  res.render('result.html', {username, status, message, destination});
})

app.post('/signupsubmit', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let currentuser = (userid == cookie? "Guest" : userid);

  let {username, pass1, pass2} = req.body;

  let newcookie = data.userLogout("");
  let [success, message] =
    await data.newUser(username, pass1, pass2);

  let destination = "/login";
  let status = success? "ok" : "error";

  res.render('result.html', {currentuser, message, destination, status});
})

app.post('/logoutsubmit', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  let newcookie = await data.userLogout(cookie);

  if(newcookie){
    setCookie(res, newcookie);
    username = "Guest";
  }
  let destination = "/";
  let message = "Logged out.";

  res.render('result.html', {username, message, destination});
})

app.post('/cmdsubmit', async (req, res)=>{
  let [cookie, userid] = await session(req, res);
  let username = (userid == cookie? "Guest" : userid);

  res.render('result.html', {username, message: "TODO", destination: "/"});
})


main();

function main(){
  let server = app.listen(8081, ()=>{
    let host = server.address().address;
    let port = server.address().port;

    console.log("Listening on http://%s:%s", host, port);
  })
}

