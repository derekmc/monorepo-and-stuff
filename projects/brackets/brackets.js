
//
// Brackets: A clean HTML sugar templating language.
//
//  Html is a noisy, ugly language. It requires a lot of ceremony to write,
//  which can also make it hard to read.  Bracket attempts to be a cleaner way
//  to write html, which additionally supports basic templating and template
//  inheritance.
//
// Features:
//
//   * Uses both significant whitespace and custom html grouping brackets "[]"
//   * Supports template inheritance with "page" and "block" tags.
//   * Client-side by default.
//   * Tag shorthand for ids, css classes and more.
//
// Colon Tags:
//
//   All html tags and bracket macros end with a colon.
//
// Whitespace Grouping:
//
//   Any part of the document not enclosed within brackets uses significant
//   whitespace for grouping.
//
// Bracket Groupings "[]":
//
//   Brackets can be used for further grouping or nesting, which allows you to
//   write the html compactly and fit a lot on a single line.
//
//   Additionally, commas within a bracket group create separate groups
//   themselves.
//
// Tag Shorthands:
//
//   Tags support the follow shorthand 
//  
// Props in parenthesis:
//
//   If you don't want to use the tag shorthand, or a particular prop is not
//   supported, you can put the tags in parenthesis.  You must have an even
//   number of tokens, to match property and values, or you can use quotes if
//   spaces are required inside the value.
//
// Inferred tags:
//
//   If a bracket grouping does not start with a colon tag, it infers a tag.
//   The tag inference is done based on the parent tag, so rows are inferred
//   inside tables, td cells are inferred inside table rows, list entries are
//   inferred inside lists, and so forth.

let Brackets = (function(){
  let brackets = toHtml;
  let impliedTags;
  let macros = {};

  brackets.parse = bracketsParse;
  brackets.toStr = bracketsStr
  brackets.addMacro = (name, macro, replace)=>{
    if(replace || !(name in macros)) macros[name] = macro
  }

  init();
  return brackets;

  // commas followed by a tag token, which end in a colon, are interepreted as
  // a tag separator.


  // a "passthrough" macro.
  function passThrough(x, props, data, f){
    x.shift()
      //console.log('passthrough props', props);
    let y = props["default"];
    if(y && y[y.length - 1] == ":"){
      x.unshift(y);
    }
    return f(x, data);
  }



  function bracketsStr(x, depth/*, newline*/){
    let max_line_tokens = 6;
    if(!depth) depth = 0;
    if(Array.isArray(x)){
      if(x.insideBrackets){
          return "[" + x.map(a => bracketsStr(a)).join(' ') + "]";
      } else {
        if(depth == 0){
          return x.map(a => bracketsStr(a, 1)).join("\n"); }
        else{
          let indent = "";
          for(let i=0; i<depth-1; ++i){
            indent += "  "; }
          //return `\n${indent + x[0]}: `+
          //  x.slice(1).map(a => bracketsStr(a, depth+1)).join(` `);
          let result = `${x[0]} `;
          let tail = tailBranching(x);
          let j = 1;
          let lastshort = true;
          for(let i=1; i<x.length; ++i){
            let shortline = !Array.isArray(x[i]) ||
               x[i].length<=2 && !Array.isArray(x[i][1]);
            let sameline = (tail || shortline && lastshort) &&
              (i-j < max_line_tokens);
            if(!sameline) j = i;
            result += (sameline? '' : `\n${indent}  `) +
              bracketsStr(x[i], depth + (sameline? 0 : 1));
            if(sameline || shortline) result += " ";
            lastshort = shortline;
          }
          return result;
        }
      }
    } else if(typeof x == "object"){
      let result = ""
      for(let k in x){
        result += k + " " + x[k] + " ";
      }
      return "(" + result.substring(0, result.length - 1) + ")";
    }

    return "" + x;
    // checks if the only branch is the tail.
    function tailBranching(node){
      for(let i=1; i<node.length - 1; ++i){
        if(Array.isArray(node[i])){
          //if(node[0][0] == 'c')
          //  console.log('is array %s?: %s/%s', node[0], i, node.length,
          //    JSON.stringify(node.slice(i)));
          return false;
        }
      }
      return true;
    }
  }
  let templatePages = {};
  function addMacro(){
  }

  function init(){
    impliedTags = {
      "": "div",
      "div" : "div",
      "ul": "li",
      "ol": "li",
      "li" : "span",
      "table": "tr",
      "tr" : "td",
      "td" : "span",
      "span" : "bold",
      "bold" : "i",
      "input" : "span",
      "button" : "span",
      "h1" : "span",
      "h2" : "span",
      "h3" : "span",
      "h4" : "span",
      "h5" : "span",
    }
    let currentPage;
    // for one page to inherit from another use a period separated list,
    // ie html
    macros.page = function(x, props, data, render){
      let name = props["default"];

      currentPage = {blocks:{}};
      
      //console.log('macro', x[0]);
      return passThrough(x, props, data, render);
    }

    macros.block = function(x, props, data, render){
      currentPage.blocks(x[1]);
      
      return passThrough(x, props, data, render);
    }
  }
  function toHtml(s, data){//, data){
    //if(!indent) indent = 0;
    let x = bracketsParse(s);
    return x.map(y => render(y, data, undefined, 0)).join("\n");
    function render(x, data, parentTag, depth){
      let nextdepth = depth === undefined? depth : depth + 1;
      if(Array.isArray(x) && x.length > 0){
        let tag = x[0];
        let rest = x;
        if(tag[tag.length-1] == ":"){
          rest = rest.slice(1);
        } else {
          if(parentTag && parentTag in impliedTags){
            tag = impliedTags[parentTag];
          } else {
            tag = impliedTags[""];
          }
        }
        let props = {};
        if(typeof rest[0] == "object" && !Array.isArray(rest[0])){
          props = rest[0];
          rest = rest.slice(1);
        }
        let indent = (depth !== undefined)? "\n" : "";
        for(let i=0; i<depth; ++i){
          indent += "  "; }

        if(tag in macros){
          return macros[tag](x, prop, data, (a, data)=> render(a, data, parentTag, depth));
        }
        let [start, end, tagname] = renderTags(tag, props, parentTag);
        start = indent + start;
        end = indent + end;
        //let start = `${indent}<${tag}${propStr(props)}>`
        //let end = `${indent}</${tag}>`
        //return start + end;
        return start + rest.map(a=>render(a, data, tagname, nextdepth)).join(' ') + end;
      } else {
        return "" + x;
      }
    }
    function propStr(props){
      let result = "";
      for(let k in props){
        result += ` ${k}="${trimAndEscapeQuotes(props[k])}"`
      }
      return result;
    }
  }
  function trimAndEscapeQuotes(s){
    if(s[0] == '"' || s[0] == "'") s = s.substring(1);
    if(s[s.length-1] == '"' || s[s.length-1] == "'") s = s.substring(0, s.length-1);
    return s.replace(/'/g, "\\x27"); //"
  }

  // .class#id~title$name :
  function renderTags(s, props, parentTag){
    //return s;
    // TODO parse tag shorthands

    let classes = [];
    let id = null;
    let title = null;
    let tag = null;
    let fg = null;
    let bg = null;
    let propstr = "";
    let separators = {
      '.': x=>classes.push(x),
      '#': x=>`id="${x}"`,
      '$': x=>`name="${x}"`,
      '~': x=>`title="${x}"`,
      '?': x=>`onclick="${x}"`,
      '+': x=>{fg = x},
      '-': x=>{bg = x},
      '': x=>{tag = x},
      ':': x=>"",
    };

    let j = 0;
    let lastc = "";
    for(let i=0; i<s.length; ++i){
      let c = s[i];
      if(c in separators){
        let word = s.substring(j, i);
        if(lastc.length){
          let text = separators[lastc](word);
          if(text && text.length){
            propstr += " " + text;
          }
        } else {
          separators[lastc](word);
        }
        j = i+1
        lastc = c;
      }
    }
    for(let k in props){
      propstr += ` ${k}="${trimAndEscapeQuotes(props[k])}"`
    }
    if(classes.length){
      propstr += ` class="${classes.join(" ")}"`;
    }
    let style = "";
    if(fg) style += `color: ${fg}; `;
    if(bg) style += `background: ${bg}; `;
    if(style.length){
      propstr += ` style="${style.trim()}"`;
    }
    if(tag == null){
      if(parentTag && parentTag in impliedTags){
        tag = impliedTags[parentTag];
      } else {
        tag = impliedTags[""]; // default
      }
    }
    let start = `<${tag}${propstr}>`;
    let end = `</${tag}>`;
    return [start, end, tag];
  }

  function error(message, s, i){
    throw new Error(
      "Brackets: " + message + "\nat \"" +  
      s.substring(Math.max(0, i-15), i) + "\" <");
  }

  function bracketsParse(s){
    let stack = [[]];
    let root = stack[0];
    let node = root;
    let i, j=0;
    let insideBrackets = false;
    let indent = 0;
    let newnode;
    //let firstToken = true;
    for(i=0; i<s.length; ++i){
      switch(s[i]){
        case '\\':
          ++i;
          break;
        case ':': //
          if(insideBrackets){
            break;
          }else{ // if(firstToken){
            newnode = [];
            newnode.insideBrackets = false;
            newnode.indent = indent;
            if(i > j) newnode.push(s.substring(j, i+1));
            node.push(newnode);
            stack.push(newnode);
            node = newnode;
            j = i+1;
          }
          break;
        case '(': // props
          if(i > j) node.push(s.substring(j, i));
          if(node.length > 1){
            error("Parenthesis not immediately after tag.", s, i+1); }
          let props = {}; // TODO proplist instead.
          node.push(props);
          let k = "key";
          let v = "value";
          j = i + 1;
          while(++i<s.length){
            let single = true;
            if(s[i] == " " || s[i] == "\t" || s[i] == "\n"){
              if(j == i){
                ++j; }
              else{
                if(k && v){
                  k = s.substring(j, i);
                  v = null; }
                else{
                  single = false;
                  v = s.substring(j, i);
                  //console.log(s.substring(j-2, i+2));
                  props[k] = v; }
                j = i+1;
              }
            }
            else if(s[i] == "\\"){
              ++i;
            }
            else if(s[i] == "("){
              error("Nested parenthesis not allowed.", s, i);
            }
            else if(s[i] == "\""){
              while(++i < s.length && s[i] != '"')
                if(s[i] == "\\") ++i;
            }
            else if(s[i] == ")"){
              if(k && !v){
                v = s.substring(j, i);
                props[k] = v;
              } else if(single){
                props['default'] = s.substring(j, i);
              } else {
                error("Unmatched property.", s, i);
              }
              break;
            }
          }
          j = i + 1;
          break;
        case '[':
          newnode = [];
          insideBrackets = newnode.insideBrackets = true;
          if(i > j) node.push(s.substring(j, i));
          node.push(newnode);
          stack.push(newnode);
          node = newnode;
          j = i + 1;
          break;
        case ']':
          if(i > j) node.push(s.substring(j, i));
          stack.pop();
          if(!stack.length) throw new Error("Unmatched ']'");
          node = stack[stack.length - 1];
          insideBrackets = node.insideBrackets;
          j = i + 1;
          break;
        case ',': // commas are shorthand for multiple bracket groups.
          if(!insideBrackets) break;
          newnode = [];
          newnode.insideBrackets = true;
          if(i > j) node.push(s.substring(j, i));
          stack.pop();
          if(!stack.length) throw new Error("Stack was unexpectedly empty on ','.");
          node = stack[stack.length - 1];
          node.push(newnode);
          stack.push(newnode);
          node = newnode;
          j = i + 1;
          break;
        case '\n':
        case '\t':
        case ' ':
          if(i > j){
            node.push(s.substring(j, i));
          }
          if(s[i] == "\n" && !insideBrackets){
            indent = 0;
            //let node.
            while(++i < s.length && (s[i] == ' ' || s[i] == "\t")){
              ++indent;
            }
            --i;
            while(!insideBrackets && indent <= node.indent){
              stack.pop();
              if(!stack.length) throw new Error("stack empty.");
              node = stack[stack.length - 1];
              insideBrackets = node.insideBrackets;
            }
          }
          j = i + 1;
          break;
        case '"':
          while(++i < s.length && s[i] != '"'){
            if(s[i] == "\\") ++i; }
          break;
      }
    }
    if(i > j) node.push(s.substring(i, j));
    if(insideBrackets) throw new Error("Unmatched '['");
    return root;
  }

  /*function bracketsOld(strs, ...args){
    //console.log('strs', strs);
    //console.log('args', args);
    let result = "";
    for(let i=0; i<strs.length; ++i){
      result += strs[i];
      if(i<args.length) result += "(" + args[i] + ")";
    }
    return result;
  }*/

  return brackets;

})();

if(typeof module === "object"){
  module.exports = Brackets;
}
