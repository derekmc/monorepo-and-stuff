
let brackets = require('./brackets');
test();

function test(){
  let s = `
    page: (html:)
      block: (head:)
        block: (includes)
        block: (style:)
        block: (script:)
      block:(body+white-black:)
        h1: Hello, World.
        table:
          [[th: a, th: b, th: c, th: d, th: e]]
          [[f, g, h, i, j]]
          [[k, l, m, n, o]]
        block: (main)
          p: Main content goes here.
        

    page: (html.one) <!-- inherits from page 'html' -->
      block: (body.main)
        This is page number one.

    form:(action ".")

    div.message:
      p: This is a test of the emergency broadcast system.
      p: Lorem Ipsum dolor sit amet, asdf

    form: (action "./submit.html" name "formUserInfo")
      input: (type "text")

    form: (action "./submit.html" name "formUserInfo")
      [ul:
        [li: 1, li: 4, li: 9] [li: 16]
        [li: [ol: [a, b, c, d, e, f, g]]]]
  `
  console.log("s:\n%s", s);
  console.log("toStr(parse(s)):\n%s",
    brackets.toStr(brackets.parse(s)));

  console.log();
  console.log();
  console.log('toHtml:', brackets(s));
  return;
}

