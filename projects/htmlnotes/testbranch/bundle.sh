# create a single file bundle, that is what we are going for after all.

FILE="htmlnotes.html"
BABELFILE="htmlnotes-babel.html"

rm -f $FILE

cat template/header.html >> $FILE
echo >> $FILE
cat css/menu.css >> $FILE
echo >> $FILE
cat template/mid.html >> $FILE
echo >> $FILE
cat lib/FileSaver.min.js >> $FILE
echo >> $FILE
cat lib/jszip.min.js >> $FILE
echo >> $FILE



# all the js files in js folder
for f in js/*.js; do (cat "${f}"; echo ) >> $FILE; done

cat template/footer.html >> $FILE

rm -f $BABELFILE

cat template/header-babel.html >> $BABELFILE
echo >> $BABELFILE
cat css/menu.css >> $BABELFILE
echo >> $BABELFILE
cat template/mid-babel.html >> $BABELFILE
echo >> $BABELFILE
cat lib/jszip.min.js >> $BABELFILE
echo >> $BABELFILE



# all the js files in js folder
for f in jsbabel/*.js; do (cat "${f}"; echo ) >> $BABELFILE; done

cat template/footer.html >> $BABELFILE


echo "bundle.sh finished."

