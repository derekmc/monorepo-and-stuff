
// not a module, only stores constants
let util = {};

util.alphanums = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
    "abcdefghijklmnopqrstuvwxyz" + "0123456789";
util.base64 = util.alphanums + "+/";

function cloneObj(obj){
  return JSON.parse(JSON.stringify(obj))
}

function setClass(elem, name, value){
  if(value === undefined) value = true;
  if(value)
    elem.classList.add(name);
  else
    elem.classList.remove(name);
}

function downloadFile(url){
  return new Promise((yes, no)=>{
    let xhr = new XMLHttpRequest()
    xhr.open("GET", url)
    xhr.send()
    xhr.onload = ()=>{
      if (xhr.status != 200) {
        no(`Error ${xhr.status}: ${xhr.statusText}`)
      } else {
        yes(xhr.responseText);
      }
    }
  })
}

function isInt(x){
  return !isNaN(x) && Math.round(x) == x;
}
function intInRange(x, a, b){
  return isInt(x) && x > a - 1 && x < b;
}

function randomInt(a, b){
  if(a == undefined){
    a = 0
    b = 100
  }
  if(b == undefined){
    b = a
    a = 0
  }
  return Math.floor(a + Math.random() * (b-a))
}

function randomString(len, chars){
  chars = chars ?? util.alphanums
  len = len ?? 10
  let result = ""
  for(let i=0; i<len; ++i){
    result += chars[randomInt(chars.length)]
  }
  return result
}


// return a tagged template function to unindent the specified number of spaces.
function unindent(spaces){
  return new function(parts, ...vars){
    if(parts.length != vars.length + 1){
      // console.log("parts, vars", parts, vars);
      // this error is just to make sure I am using tagged template literals correctly.
      throw new Error("tagged template literal should have vars and parts comparable lengths");
    }
    for(let i=0; i<vars.length; ++i){
      str += parts[i] + vars[i];
    }
    str += parts[parts.length - 1];

    let lines = str.split("\n");
    for(let i=0; i<lines.length; ++i){
      let line = lines[i];
      for(let j=0; j<spaces && j<line.length; ++j){
        if(!line[j].match(/\s/)) break; }
      let leadingspaces = j;
      lines[i] = line.substring(leadingspaces);
    }
    return lines.join("\n");
  }
}
function isBinaryFile(url){
  let suffixes = [".jpg", ".png", ".gif", ".jpeg", ".webp"]
  return suffixes.map(x=>url.endsWith(x)).reduce((a, b)=> a||b)
}
function getUrl(url, timeout = 8000){
  let req = new XMLHttpRequest()
  if(isBinaryFile(url)) req.responseType = "blob"
  req.open("GET", url)
  return new Promise((yes, no)=>{
    let done = false
    req.onload = (e)=>{
      if(req.readyState == 4){
        if(req.status == 200){
          done = true
          console.log('response type', req.responseType)
          yes(req.response)}
        else{
          console.error(req.statusText)
          done = true
          no()
        }
      }
    }
    req.onerror = (e) => {
      console.error(req.statusText)
      done = true
      no()
    }
    req.send(null)
    setTimeout(()=>{ if(!done) no()}, timeout)
  })
}

