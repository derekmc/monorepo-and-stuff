
=== TODO ===

prompt for js enabled or disabled.

=== IDEAS ==

Have a web version that factors sections into separate files:
  html, css, js, json
  index.html, style.css, app.js, data.json


Start apps paused.

orbit module

orbit.paused = true
orbit module has modals for prompt, alert, confirm, choose
  (replicate menu.js, but small)



Make a option in the main menu which clears the cache
to "update" the app.

Make a distributable package (electron? selfhosted?),
which automatically loads a zip file with a given
name, into an active running program.

This is intended to emulate the editor being "embeddable"
so your app can be modded by users in real time.

TODO: create a "pause" button.
factor "data" variable to be in module "orbit"
orbit.data = /* json */
orbit.pause = false;

orbit.pause lets the javascript know to "pause"
execution, to save cpu.

When the program is resumed it may be reloaded or
simply resumed from the active version.

You may pause and then save.






==================================


 - data tab/split
    if the first non-empty non-comment line of your js file starts with
    "let data = ", then the js table can toggle between data view or
    js view.  As the program is being run, data gets autosaved, and
    it is captured in the text for the data tab.
    the data tab only allows for one json object.
 - modal/popup window for alert, choose(prompt), confirm alternative.
 - download button with jszip
 - copy button to duplicate current document.

 - htmldump.  This is a fourth view which is the raw document text (not editable),
   including all the different formats.  It is shown as "html:"
 - data tab (which also may be disabled, which controls whether saves overwrite)
make this a service for editing and publishing various website templates
add a menu. (on right side, move page and split to right side as well).
add demos to the menu.
add a "download zip to the menu."
support naming/deleting pages in the menu.
generate document thumbnails for the menu.
when width is even smaller, < 280px, use a cycle format button.
support CORS external document loading.
share content with
nft-like licenses (with bulk discount and resell possibility)
(saved you a right click)
 - webservice


============= DONE =================
 - remove spellcheck from text fields.
 - progressive webapp.
 - disable js tab by default
