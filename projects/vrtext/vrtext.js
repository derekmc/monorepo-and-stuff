const Edit = "Edit"
const TextSize = "TextSize"
const WindowSizeDepth = "WindowSizeDepth"
const MoveWindow = "MoveWindow"
const Copy = "Copy"
const IncrementFactor = 1.05
const InBrowser = (typeof window !== "undefined")
const SaveKey = "VRTextData"
const RefreshInterval = 800  // maximum refresh delay in milliseconds
const SaveInterval = 3500  // autosave delay

let lastStyle = ""

let ui = {}
let data = {
  selectA: 10,
  selectB: 15,
  clipboardText: null,
  win: {
    crosseye: 8,
    widthratio: 25, //pct of full width
    aspectratio: 1.5,
    lookup: 0,
    lookleft: 0,
    fontpx: 8,
    padx: 1,
    pady: 6,
  },
  text: "\nBluetooth Keyboard Required!!\n" + 
    "=========\n" +
    "This is a VR based text editor.\n" + 
    "Please ensure you have your device\n" + 
    "in landscape orientation.\n" + 
    "Also, this app will not work on mobile\n" + 
    "devices without a bluetooth keyboard.\n\n" +
    "Use the <ESC> key, and arrow keys to edit settings.",
  version: "0",
}

let temp = {
  initPage: false,
  statusText: "<ESC> for Settings",
  colorScheme: "sepia",
  modes: [Edit, TextSize,
    WindowSizeDepth, MoveWindow],
  winbounds: {
    fontpx: [5, 16],
    crosseye: [-15, 30],
    lookup: [-250, 250],
    lookleft: [-180, 180],
    aspectratio: [0.8, 2.0],
    widthratio: [12, 38],
  },
  modeText: {
    Edit: "<ESC> for options",
    WindowSizeDepth: "Window Size and Depth <Arrows>",
    TextSize: "Text Size <Arrows>",
    MoveWindow: "Move Window <Arrows>",
    Copy: "Text Copied to Clipboard",
  }
}

let params = {
  maxshift: 20,
}

if(InBrowser)
  window.addEventListener("load", init)
else init()

function init(){
  load()
  if(InBrowser)
    window.addEventListener("keydown", keydown)
  setInterval(refresh, RefreshInterval)
  setInterval(save, SaveInterval)
  refresh()
}

function refresh(){
  let args = {...data.win, ...temp, ...data}
  let container = InBrowser? document.body : {}
  render(container, args)
}

function NextMode(){
  // cycle through modes
  temp.modes.push(temp.modes.shift())
  let mode = temp.modes[0]
  if(mode == Copy) copyFullText()
  temp.statusText = temp.modeText[mode]
  refresh()
}


function calcCharSize(fontpx){
  let span = document.createElement("span")
  let text = "test test test test "
  let lineCount = 10
  let s = ""
  for(let i=0; i<lineCount; ++i){
    s += (i>0? "<br>" : "") + text }
  span.innerHTML = s
  span.style.border = "1px solid black"
  span.style.fontSize = `${fontpx}px`
  span.style.fontFamily = "monospace"
  span.style.fontWeight = "bold"
  span.style.padding = 0
  span.style.position = "absolute"
  span.style.boxSizing = "border-box"
  document.body.appendChild(span)
  let width = span.offsetWidth
  let height = span.offsetHeight
  remove()
  function round(x){
    return Math.round(20*x)/20
  }
  function remove(){
    document.body.removeChild(span)
  }
  
  return [round(width / text.length), round(height/lineCount)]
}

function keydown(e){
  e.preventDefault()
  let key = e.key
  let actionkey = false
  if(key.length > 1) key = key.toLowerCase()
  if(key == "escape"){
    // next mode
    NextMode()
    return false
  }
  if(e.ctrlKey){
    if(key == "a"){
      data.selectA = 0
      data.selectB = data.text.length
      refresh()
      return false
    }
    if(key == "x"){
      cutSelection()
      refresh()
      return false
    }
    if(key == "c"){
      copySelection()
      refresh()
      return false
    }
    if(key == "v"){
      return pasteSelection().then(()=>{
        refresh()
        return false
      }).catch((err)=>{
        console.error(err)
      })
    }
    return false
  }
  let mode = temp.modes[0]
  if(mode == Edit){
    if(key == "enter") key = "\n"

    let selectmin = Math.min(data.selectA, data.selectB)
    let selectmax = Math.max(data.selectA, data.selectB)
    let prefix = data.text.substr(0, selectmin)
    let selected = data.text.substring(selectmin, selectmax)
    let suffix = data.text.substr(selectmax)
    if(key.length == 1){
      let ch = key
      data.text = prefix + ch + suffix
      data.selectB = data.selectA = selectmin + 1
    }
    if(key == "backspace"){
      if(selected.length > 0){
        data.text = prefix + suffix
        data.selectA = data.selectB = selectmin
      } else if(prefix.length){
        data.text = prefix.substr(0, prefix.length - 1) + suffix
        data.selectB = data.selectA = selectmin - 1
      }
    }
    if(key == "arrowup"){
      let i = data.selectA
      let j = i
      while(--j >= 0 && data.text[j] != "\n");
      let linepos = i - j
      i = j
      if(j < 0){
        data.selectA = 0
      } else {
        while(--j >= 0 && data.text[j] != "\n");
        linepos = Math.min(linepos, i - j)
        data.selectA = j + linepos < data.selectA? j + linepos : 0
      }

      if(!e.shiftKey){
        data.selectB = data.selectA
      }
      refresh()
      return false
    }

    if(key == "arrowleft"){
      let move = 1
      let newline = false
      while(--move >= 0 && !newline){
        newline = data.text[data.selectB] == "\n"
        --data.selectA
      }
      if(!e.shiftKey){
        data.selectB = data.selectA
      }
    }
    if(key == "arrowdown"){
      let i = data.selectA
      let j = i
      while(--j > 0 && data.text[j] != "\n");// --j;
      let linepos = i - j
      j = i
      while(j < data.text.length && data.text[j] != "\n") ++j
      i = j
      while(++j < data.text.length &&
        j < i + linepos && data.text[j] != "\n");
      data.selectA = Math.min(j, data.text.length)
      if(!e.shiftKey){
        data.selectB = data.selectA
      }
      refresh()
      return false
    }
    if(key == "arrowright"){
      //let move = (key == "arrowright"? 1 : temp.xchars)
      let move = 1
      let newline = false
      while(--move >= 0 && !newline){
        newline = data.text[data.selectB] == "\n"
        ++data.selectA
      }
      if(!e.shiftKey){
        data.selectB = data.selectA
      }
    }
    let len = data.text.length
    if(data.selectA < 0) data.selectA = 0
    if(data.selectB < 0) data.selectB = 0
    if(data.selectA > len) data.selectA = len
    if(data.selectB > len) data.selectB = len
    refresh()
    return false
  } else {
    key = key.toLowerCase()
    let movekeymap = {
      "arrowleft": "left", "arrowup": "up",
      "arrowright": "right", "arrowdown": "down",
      "w": "up", "a": "left", "s": "down", "d": "right",
    }
    if(key in movekeymap){
      key = movekeymap[key]
    } else {
      // return to edit mode when a letter key is pressed.
      //
      while(temp.modes[0] != Edit){
        NextMode()
      }
      return false
    }
    let offset = 0
    let xoffset = 0
    let yoffset = 0
    let wrap = false
    if(key == "up"){
      offset = yoffset = -1
    }
    if(key == "down"){
      offset = yoffset = 1
    }
    if(key == "left"){
      offset = xoffset = -1
    }
    if(key == "right"){
      offset = xoffset = 1
    }
    let applyOffset = ({value, offset, wrap, integer, bounds})=>{
      let y = value
      if(integer){
        y = Math.round(y)
        if(y == value) y += offset
      }
      if(y < bounds[0]){
        y = bounds[wrap? 1: 0] }
      else if(y > bounds[1]){
        y = bounds[wrap? 0: 1] }
      return y
    }

    if(mode == TextSize){
      let bounds = temp.winbounds.fontpx
      let value = data.win.fontpx
      data.win.fontpx = applyOffset({
        value, offset: -offset, bounds,
        wrap, integer: true})
    } else if(mode == WindowSizeDepth){
      if(xoffset){
        let bounds = temp.winbounds.widthratio
        let value = data.win.widthratio
        data.win.widthratio = applyOffset({
          value, offset: 2*offset, bounds,
          wrap, integer: true})
      }
      if(yoffset){
        let bounds = temp.winbounds.crosseye
        let value = data.win.crosseye
        data.win.crosseye = applyOffset({
          value, offset: -5 * offset, bounds,
          wrap, integer: true})
      }
    } else if(mode == MoveWindow){
      if(yoffset){
        let bounds = temp.winbounds.lookup
        let value = data.win.lookup
        data.win.lookup = applyOffset({
          value, offset: 15 * offset, bounds,
          wrap, integer: true})
      }
      if(xoffset){
        let bounds = temp.winbounds.lookleft
        let value = data.win.lookleft
        data.win.lookleft = applyOffset({
          value, offset: 15 * offset, bounds,
          wrap, integer: true})
      }
    }
  }
  return false
}
function cutSelection(){
  let selectmin = Math.min(data.selectA, data.selectB)
  let selectmax = Math.max(data.selectA, data.selectB)
  let prefix = data.text.substr(0, selectmin)
  let selected = data.text.substring(selectmin, selectmax)
  let suffix = data.text.substr(selectmax)
  let text = data.text.substring(selectmin, selectmax)

  data.text = prefix + suffix
  data.selectA = data.selectB =
    selectmax - selected.length
  data.clipboardText = text
  navigator.clipboard.writeText(text)
}
function copySelection(){
  let selectmin = Math.min(data.selectA, data.selectB)
  let selectmax = Math.max(data.selectA, data.selectB)
  let text = data.text.substring(selectmin, selectmax)
  data.clipboardText = text
  navigator.clipboard.writeText(text)
}
async function pasteSelection(){
  let clipboardText
  try{
    //throw 0
    clipboardText = await navigator.clipboard.readText()
  } catch(e){
    console.warn(e)
    console.log("Cannot read clipboard, falling back on virtual clipboard.")
    clipboardText = data.clipboardText
  }
  // console.log("clipboardText", clipboardText)

  if(clipboardText){
    let selectmin = Math.min(data.selectA, data.selectB)
    let selectmax = Math.max(data.selectA, data.selectB)
    let prefix = data.text.substr(0, selectmin)
    let selected = data.text.substring(selectmin, selectmax)
    let suffix = data.text.substr(selectmax)
    data.text = prefix + clipboardText + suffix
    data.selectA = data.selectB =
      selectmax + clipboardText.length - selected.length
  }
}

function copyFullText(){
  navigator.clipboard.writeText(data.text)
}

function save(){
  localStorage.setItem(SaveKey, JSON.stringify(data))
}

function load(){
  let s = localStorage.getItem(SaveKey)
  if(s){
    try{
      data = JSON.parse(s)
    } catch(e){
      console.warn("Error parsing saved data")
      console.warn(e)
    }
  }
}

function round10(x){
  return Math.round(10 * x)/10
}
function round20(x){
  return Math.round(20 * x)/20
}
function escapeHTML(s){
  if(!s) return
  return s.replace("<", "&lt;").replace(">","&gt;")
}

//let updateContent = ()=>{
//  let textboxleft = document.getElementById("textboxleft")
//  let textboxright = document.getElementById("textboxright")
//  let textleft = document.querySelector("#textboxleft")
//  let textright = document.querySelector("#textboxright")
//}
//let renderPage = (container, {text, crosseye, lookleft, lookup,
//  widthRatio, aspectRatio, colorScheme, statusText,
//  selectA, selectB, padx, pady, fontpx})=>{
//}

let render = (container, {text, crosseye, lookleft, lookup,
    widthratio, aspectratio, colorScheme, statusText,
    selectA, selectB, padx, pady, fontpx}) => {
  //console.log("in render function")

  let styleProps = JSON.stringify([
    window.innerWidth, window.innerHeight,
    lookleft, lookup, crosseye,
    widthratio, aspectratio,
    colorScheme, fontpx
  ])

  let styleChanged = (styleProps != lastStyle)
  lastStyle = styleProps

  let w = window.innerWidth
  let h = window.innerHeight
  let width = w * widthratio/100
  let height = width / aspectratio
  let left1 = round10(0.25 * w - (width - w/100*crosseye)/2 + lookleft)
  let left2 = round10(0.75 * w - (width + w/100*crosseye)/2 + lookleft)
  let top = round10(0.5 * h - height/2 + lookup)
  let [charw, charh] = calcCharSize(fontpx)
  let xchars = Math.floor((width - 2*padx)/charw) - 1
  let ychars = Math.floor((height - 2*pady)/charh) - 1

  let selectmin = Math.min(selectA, selectB)
  let selectmax = Math.max(selectA, selectB)

  let selectprefix =
    `<span class='${selectmin == selectmax? "cursor" : "invert" }'>`
  let selectsuffix = "</span>"

  let selectminline = -1
  let selectmaxline = -1

  temp.xchars = xchars
  temp.ychars = ychars
  let i = 0, j = 0
  let startline = 0
  let startcol = 0
  let line = []
  let lines = []
  function nextline(){
    lines.push(line)
    line = []
    j = i
  }
  for(; i<=text.length; ++i){
    let ch = text[i]  
    //if(i-j >= xchars-1){ //no wrap
    //  nextline() }
    if(i == selectA){  // focus on selectA
      startcol = Math.max(0, (i-j) - Math.floor(0.7*xchars))
      startline = lines.length - 1 - Math.floor(ychars/2)
    }
    if(i == selectmin){
      selectminline = lines.length
      line.push(selectprefix)
      if(selectmin == selectmax){
        line.push(selectsuffix)
        selectmaxline = lines.length
      }
    } else if(i == selectmax){
      line.push(selectsuffix)
      selectmaxline = lines.length
    }
    if(i == text.length) break
    if(ch == "<") line.push("&lt;")
    else if(ch == ">") line.push("&gt;")
    else if(ch == "&") line.push("&amp;")
    else if(ch == " " || ch == "\t") line.push("&nbsp;")
    else if(ch == "\n"){
      nextline() }
    else line.push(ch)
  }
  //startline = lines.length - 1 - Math.floor(ychars/2)
  nextline()
  if(startline >= lines.length){
    startline = lines.length - 1 - Math.floor(ychars/2) } 

  if(startline < 0) startline = 0

  let endline = Math.min(startline + ychars, lines.length - 1)
  let src = `<span class='bigger textcenter bg'>${escapeHTML(statusText)}       </span>`

  if(selectminline < startline){
    src += selectprefix }

  for(let i=startline; i<=endline; ++i){
    let line = lines[i]
    let linetext = ""
    try{
      linetext = line.slice(startcol, startcol + xchars).join("") }
    catch(e){
      console.error(e) }
    let selectstart = line.indexOf(selectprefix)
    let selectend = line.indexOf(selectsuffix)
    if(selectstart >= 0){
      if(selectstart < startcol){
        linetext = selectprefix + linetext }
      if(selectstart > startcol + xchars - 1){
        linetext += selectprefix }}
    if(selectend >= 0){
      if(selectend < startcol){
        linetext = selectsuffix + linetext }
      if(selectend > startcol + xchars - 1){
        linetext += selectsuffix }}
    src += (linetext.length? linetext : "&nbsp;") + "<br>"
  }

  if(selectmaxline > endline){
    src += selectsuffix }

  //update only, dont rerender.
  if(temp.initPage){
    ui.textleft.innerHTML = src
    ui.textright.innerHTML = src
    if(styleChanged){
      ui.textbox.forEach(x=>{
        x.style.padding = `${padx}px ${pady}px`
        x.style.fontSize = `${fontpx}px`
      })
      ui.vrall.forEach(x=>{
        x.style.top = `${top}px`
        x.style.width = `${width}px`
        x.style.height = `${height}px`
      })
      ui.vrleft.style.left = `${left1}px`
      ui.vrright.style.left = `${left2}px`
    }
    return
  }
  temp.initPage = true

  container.innerHTML = `
    <div class="main ${colorScheme}">
      <div class="bgleft"></div>
      <div class="bgright"></div>
      <p id="textboxleft" class="vr vrleft textbox bgshade">
        ${src}
      </p>
      <p id="textboxright" class="vr vrright textbox bgshade">
        ${src}
      </p>
      <!--
      -->
    </div>
    <style>
      body{ margin: 0; overflow: hidden;
        url("./img/desert.jpg");
        width: 100%; user-select: none; }
      *{ box-sizing: border-box; }
      .main{ position: absolute; top: 0; left: 0;
        width: 100vw; height: 100vh; }
      .textbox {
         white-space: nowrap; overflow: hidden;
         padding: ${padx}px ${pady}px;
         font-size: ${fontpx}px; font-family: monospace;
         font-weight: bold; }
      .vr{
         position: absolute; top: ${top}px;
         width: ${width}px; height: ${height}px;
         background: rgba(128,128,128,0.15); }
      .vrleft{ left: ${left1}px; }
      .vrright{ left: ${left2}px; }

      /* style changes */
      .bigger{ font-size: 110%; }
      .ul{ text-decoration: underline; }
      .textcenter{ text-align: center; }
      .bgshade{ background: rgba(128,128,128, 0.15); }
      .bg{ margin: -2px -10px;
           padding: 3px 10px; display: block; }

      /* color themes */
      .dark { background: #000; color: #fff; text-decoration-color: #000; }
      .dark .invert { color: #000; background: #fff; text-decoration-color: #fff; }
      .dark .cursor { border-left: 2px solid #fff; }
      .light .bg { background: #000; color: #aaa; }

      .light { background: #fff; color: #000; }
      .light .invert { color: #fff; background: #000; }
      .light .cursor { border-left: 2px solid #000; }
      .light .bg { background: #fff; color: #444; }

      .retro { background: #000; color: #48f058; }
      .retro .invert { color: #000; background: #48f058; }
      .retro .cursor { border-left: 2px solid #48f058; }
      .retro .bg { background: #000; color: #28b038; }

      .sepia { color: #310; }
      .sepia .vrleft,
      .sepia .vrright { background: rgba(242, 228, 200, 0.85); }
      .sepia .invert { color: #edb; background: #310; }
      .sepia .cursor { border-left: 2px solid #310; }
      .sepia .bg { background: #edb; color: #853; }

      .sepia .bgleft, .sepia .bgright{
        filter: blur(3px);
        url("./img/desert.jpg");
      }

      .bgleft, .bgright{
        background-image: url(img/desert.jpg);
        background-size: cover;
        background-position: center center;
        position: absolute;
        top: 0; bottom: 0;
      }
      .bgleft{
        left: 0;
        right: 50vw;
      }
      .bgright{
        left: 50vw;
        right: 0;
      }
    </style>
  `
  ui.textleft = document.querySelector("#textboxleft")
  ui.textright = document.querySelector("#textboxright")
  ui.main = document.querySelector(".main")
  ui.textbox = document.querySelectorAll(".textbox")
  ui.vrall = document.querySelectorAll(".vr")
  ui.vrleft = document.querySelector(".vrleft")
  ui.vrright = document.querySelector(".vrright")
}


