// working on standalone intepreter in 3 levels
// Level 0: stack based calculator only
// Level 1: can define commands and recurse
// Level 2: Global goto statements for procedural control
// Level 2+: local goto statements

module.exports = {}

// global intepreter state
let output = ''
let stack = []
let commands = {}
let variables = {}
let lines = []
let labels = {} // {name: {name, level, line, children}}
let lineLabels = []

module.exports.interpret = interpret

test()

// DEPRECATED for ref only
//function emptyLabel(){
//  return {name: '', line: 0, level: 0, children: {}}
//}

function test(){
  let sample1 = `
    ~~~START~~~
     1000 @maxnum
     "Guest" @username
     ~~Main~~
      1 @x
      0 @s
      ~loop
        $x > 100
        $x 1 + @x
        $s $x + @s
      loop
      ~endloop
      "Continue Playing(Y/N)?" ??? toLowerCase "y" == ?START
     ~~Menu
      "What is the maximum number?" ??? @maxnum
      "What is your name?" ??? @username
      ~help
      START
   ~~~END
    "Thank you for playing"
  `
  console.log('testing...')
  console.log('sample1:\n', sample1)
  let lines = sample1.split('\n')
  readLabels(lines)
  console.log("readLabels(sample1.split('\\n'))")
  removeUp(labels)
  console.log(JSON.stringify(labels, null, 2))

  console.log("Label Lines: ")
  for(let i=0; i<lineLabels.length; ++i){
    let label = lineLabels[i];
    let name = label? label.name : ''
    console.log(`${i}: ${name}  ${lines[i]}`)
  }
}

function reset(){
  output = ''
  stack = []
  commands = {}
  variables = {}
  lines = []
  labels = {}
}

// this is sort of a hacky "helper method"
// way of creating a javascript object,
// but it is also simple and easy to use.
// up is the parent label
// DEPRECATED FOR REFERENCE ONLY
//function addLabel({
//  up, name, lineno, level, children}){
//  if(!up && !labels.hasOwnProperty(name)){
//    labels[name] = [] }
//  let list = labels[name] // unordered
//  if(!up) labels 
//  list.pus
//}

// get the tighest in scope label for "local goto"
function inScopeLabel(currentLabel, name){
  let level = currentLabel.level
  while(!currentLabel?.children?.hasOwnProperty(name)){
    if(!currentLabel.up) return null;
    currentLabel = currentLabel.up
  }
  return currentLabel.children[name]
}

function readLabels(lines){
  let maxLevel = 1
  labels = {}
  currentLabel = null
  for(let i=0; i<lines.length; ++i){
    let line = lines[i].trim()
    let removeA = 0 // start
    let removeB = 0 // end
    while(removeA < line.length){
      if(line[removeA] != '~') break
      ++removeA
    }
    while(line.length > removeB){
      if(line[line.length - 1 - removeB] != '~') break
      ++removeB
    }
    line = line.substring(removeA, line.length - removeB)
    lineLabels[i] = currentLabel ?? null
    if(removeA > 0){
      let name = line
      let lineno = i
      let level = removeA
      while(level > maxLevel){ // tree wrappers
        //console.log('labels', JSON.stringify(labels, null, 2))
        let up = {}
        labels.up = up
        labels = up
        currentLabel = null
        ++maxLevel
      }
      let newLabel = {
        name, level, line: lineno,
        children: {}
      }
      if(currentLabel){
        while(currentLabel && level >= currentLabel.level)
          currentLabel = currentLabel.up
        if(currentLabel && currentLabel.children){
          currentLabel.children[name] = newLabel
          newLabel.up = currentLabel
        }
      }

      // currentLabel might be made null inside the conditional,
      // so we can't use an else here.
      if(!currentLabel || !currentLabel.children)
        labels[name] = newLabel
      currentLabel = newLabel
      //console.log('labels', labels)
      let depth = maxLevel - level
    }
  }
}

function removeUp(node){
  for(let k in node){
    if(node[k].hasOwnProperty('up'))
      delete node[k]['up']
    removeUp(node[k].children) }
}


// note: definitions command definitions
// should only be evaluated on the first pass

function interpret(src){
  lines = src.split("\n").map(txt => txt.trim())
}
