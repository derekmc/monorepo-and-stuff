
const epsilon = 0.000001
const suppress_output = false
const max_recurse = 1000

let output, stack, commands, variables, labels, hist, cmdname, recurse;
module.exports = {}
module.exports.resetCommand = resetCommand
module.exports.handleCommand = handleCommand
module.exports.commandOutput = commandOutput

// both a getter and a setter
function commandOutput(value){
  if(value) output = value
  return output
}

function resetCommand(){
  output = ''
  stack = []
  commands = {}
  variables = [{}] // uses a stack for scope, the top is global scope
  labels = [{}]
  hist = []
  cmdname = null  // in the middle of parsing a command
  recurse = 0
}
// returns true if anything (noncomment) was executed.
function handleCommand(text, suppress){
  let words = text.split(/\s+/)
  let executed = false
  //console.log('words: ' + words.join(','))
  let f = parseFloat
  let binary_operators = {
    '+': (x, y)=>f(x)+f(y),
    '-': (x, y)=>f(x)-f(y),
    '*': (x, y)=>f(x)*f(y),
    '^': (x, y)=>f(x)**f(y),
    '/': (x, y)=>f(x)/f(y),
  }
  
  let answer = ''
  for(let i=0; i<words.length; ++i){
    let word = words[i].trim()
    if(word.length && !suppress && i == 0 && !cmdname && ":#".indexOf(word[0]) == -1){
      output += "\n| "
    }
    if(word.length && !suppress && !cmdname && ":#".indexOf(word[0]) == -1){
      output += " " + word
    }
    if(word.length == 0) continue
    if(cmdname){ // entering a command
      if(cmdname == '#'){ // comment
        --i
        while((++i < words.length) && (word = words[i])){
          if(word == ";"){
            cmdname = null
            break
          }
        }
      } else {
        executed = true
        let cmd = []
        --i
        while((++i < words.length) && (word = words[i])){
          if(word == ";"){
            //console.log(": " + cmdname + " " + commands[cmdname] + " " + cmd.join(" "))
            commands[cmdname] += " " + cmd.join(" ")
            cmdname = null
            break
          }
          cmd.push(word)
        }
        if(cmdname) commands[cmdname] += " " + cmd.join(" ")
      }
    }else if(word == '#'){
      cmdname = '#'
      while((++i < words.length) && (word = words[i])){
        if(word == ";"){
          cmdname = null
          break }
      }
    }else if(word[0] == "?" && word[1] == "?"){ // run command on non-zero value with no default value
      executed = true
      let test = stack.pop()
      if(!(test < epsilon) && test !== null && test !== undefined){
        handleCommand(word.substr(2), true)
        answer = stack.length > 0? stack[stack.length - 1] : ''
      }
    }else if(word[0] == "?"){ // run command on non-zero value or return a default value
      executed = true
      let defvalue = stack.pop()
      let test = stack.pop()
      if(!(test < epsilon) && test !== null && test !== undefined){
        handleCommand(word.substr(1), true)
        answer = stack.length > 0? stack[stack.length - 1] : ''
      } else {
        stack.push(defvalue)
        answer = ' ' + defvalue
      }
    }else if(word in commands){ // run command
      executed = true
      variables.push({})
      if(++recurse > max_recurse)
        output += `\nError: recurse limit (${max_recurse}) exceeded.\n`
      else
        handleCommand(commands[word], true)
      --recurse
      answer = stack.length > 0? stack[stack.length - 1] : ''
      if(variables.length > 1)
        variables.pop()
    }else if(word[0] == "@"){ // variable definition
      executed = true
      let scope = variables[variables.length - 1]
      let name = word.substr(1)
      let value = stack.pop()
      scope[name] = value
    }else if(word[0] == '$'){ // variable reference
      executed = true
      let name = word.substr(1)
      let j = variables.length - 1
      let value = undefined
      do{
        let scope = variables[j]
        if(name in scope){
          value = scope[name]
        }
      }while(value === undefined && --j >= 0);
      stack.push(value)
      answer = ' ' + value
    }else if(word == ":"){
      executed = true
      let cmd = []
      cmdname = words[++i]
      while((++i < words.length) && (word = words[i])){
        if(word == ";"){
          //console.log(": " + cmdname + " " + cmd.join(" "))
          commands[cmdname] = cmd.join(" ")
          cmdname = null
          break
        }
        cmd.push(word)
      }
      if(cmdname) commands[cmdname] = cmd.join(" ")
    }
    else if(word == "xx"){
      executed = true
      stack.length = 0
    }
    else if(word == "="){ // peek
      executed = true
      if(stack.length)
        output += " " + stack[stack.length - 1]
    }
    else if(word == 'x'){ // drop
      executed = true
      if(stack.length) stack.pop()
    }
    else if(word == '.'){ // pop
      executed = true
      if(stack.length)
        answer = " " + stack.pop()
    }
    else if(word == "|"){
      executed = true
      answer = " " + stack.join(" ")
    }
    else if(word == '%'){ // swap
      executed = true
      let x = stack.pop(), y = stack.pop()
      stack.push(x)
      stack.push(y)
      answer = ' ' + x + ' ' + y
    }
    else if(word == '&'){ // dup
      executed = true
      if(stack.length){
        let x = stack.pop()
        stack.push(x)
        stack.push(x)
        answer = ' ' + x + ' ' + x
      }
    }
    else if(word == '<'){ // rot
      executed = true
      if(stack.length > 2){
         let a = stack.pop(), b = stack.pop(), c = stack.pop()
         stack.push(b, a, c)
         answer += ' ' + b + ' ' + a + ' ' + c
      }
    }
    else if(word == '>'){
      executed = true
      if(stack.length > 2){
         let a = stack.pop(), b = stack.pop(), c = stack.pop()
         stack.push(a, c, b)
         answer = ' ' + a + ' ' + c + ' ' + b
      }
    }
    else if(!isNaN(word)){ // number
      executed = true
      stack.push(word)
      answer = ' ' + word
    }
    else if(word in binary_operators){
      executed = true
      let f = binary_operators[word]
      if(stack.length < 1){
        output += "\nCannot perform op on shallow stack."
      } else{
        let b = stack.pop(), a = stack.pop()
        let value = f(a, b)
        stack.push(value)
        answer = ' ' + value
      }
    }
    else{
      output += "\n Unknown command '" + word + "'"
    }
  }
  if(!suppress && answer){
    // console.log('answer: ' + answer)
    output += "    =" + ('' + answer).trim()
  }
  return executed
}


