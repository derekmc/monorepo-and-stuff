dot = ([a,b,c],[d,e,f])=>
  a*d+b*e+c*f
cross = ([a,b,c],[d,e,f])=>
  [b*f-e*c, d*c-a*f, a*e-d*b]
m31 = ([a,b,c],d)=>
  [dot(a,d), dot(b,d), dot(c,d)]
t33=([a,b,c])=>
  [[a[0],b[0],c[0]],
   [a[1],b[1],c[1]],
   [a[2],b[2],c[2]]]
m33=(A,B)=>{
  let [c,d,e] = t33(B)
  return t33([m31(A,c), m31(A,d), m31(A,e)])}
