const WebSocket = require('ws')

let sockets = []
main()

function main(){
  let server = new WebSocket.Server({ port: 8020 })
  server.on('connection', sockConnect)
}

function sockConnect(socket){
  console.log('connect')
  sockets.push(socket)
  socket.on('message', sockMessage(socket))
  socket.on('close', sockClose(socket))
}

//curried 
function sockMessage(socket){
  return (msg)=>{
    sockets.forEach(s => s.send(msg))
  }
}
function sockClose(socket){
  return ()=>{
    sockets = sockets.filter(s => s !== socket)
  }
}
