# create a single file bundle
File="mathdemo.html"
rm -f $File
cat html/header.html >> $File
echo >> $File

cat css/simple.css >> $File
echo >> $File

cat html/mid.html >> $File
echo >> $File

for f in js/*.js; do (cat "${f}"; echo ) >> $File; done

cat html/mid2.html >> $File
echo >> $File

cat text/math.txt >> $File
echo >> $File

cat html/footer.html >> $File
echo >> $File

echo "bundle.sh finished."
