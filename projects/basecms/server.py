#!/usr/bin/env python

import os, re, html
from lib.bottle import route, run, request, redirect
from urllib.parse import parse_qs
import lib.passwd as passwd

# pages = ["demo", "edit", "index"]
pages_folder = "pages"
page_files = []
word_regex = re.compile('[A-Za-z0-9_]*')
extension_regex = re.compile('js|css|html')

#for page in page_files:
#  print("page: ", page)

#exit()
pages_src = {}

def load_pages():
  global page_files
  page_files = list(filter(lambda x: x[-5:] ==".html", os.listdir(pages_folder)))
  for filename in page_files:
    i = filename.find(".")
    extension = filename[i+1:]
    name = filename[:i] # clip .html
    if extension != "html":
      name += "_" + extension
    with open(f"pages/{filename}", "r") as file:
      pages_src[name] = file.read()

@route('/')
def index():
  page_list = "\n".join([
    f"<li><a href='/{x}'>{x}</a> (<a href='/cms/{x}/edit/page'> Edit </a>) </li>"
      for x in pages_src])
  #print "pagelist:\n" 
  src = pages_src["index"]
  src = src.replace("{{page_list}}", page_list)
  return src

@route('/newpage', method="POST")
def newpage():
  #postdata = request.body.read()
  newpage_name = request.forms.get("newpage_name")
  newpage_extension = request.forms.get("newpage_extension")
  if not (word_regex.match(newpage_name) and extension_regex.match(newpage_extension)):
    src = pages_src["error"]
    src = src.replace("{{msg}}", "Invalid Filename or Extension.")
    return src
  
  path = pages_folder + os.sep + newpage_name + "." + newpage_extension

  if os.path.exists(path):
    src = pages_src["error"]
    src = src.replace("{{msg}}", "File already exists")
    return src

  f = open(path, 'w')
  content = ""
  if newpage_extension == "html":
    content = pages_src['default']
  elif newpage_extension == "js":
    content = pages_src['default_js']
  elif newpage_extension == "css":
    content = pages_src['default_css']
  f.write(content)
  load_pages()
  return redirect('/')
  

# page format corresponds to the full page.
@route('/cms/<docname>/edit/<fmt>')
def main(docname, fmt="page"):
  if fmt == "":
    fmt = "page"
  content = ""
  if docname in pages_src:
    content = html.escape(pages_src[docname])
  src = pages_src["edit"]
  src = src.replace("{{format}}", fmt)
  src = src.replace("{{content}}", content)
  src = src.replace("{{docname}}", docname)
  return src

@route('/cms/<docname>/save', method="POST")
def write(docname):
  postdata = request.body.read()
  query = parse_qs(postdata)

  doc_content = query.get(b'editor_doc', [b''])[0].decode('utf-8')
  file_type = query.get(b'file_type', [b''])[0].decode('utf-8')

  with open(f"pages/{docname}.html", "w") as file:
    file.write(doc_content)
  if docname in pages_src:
    pages_src[docname] = doc_content
  else:
    print(f"{docname} not in pages.")

  print(f"doc: {docname}")
  print(doc_content)
  return "File written <br><a href='/'> home</a>"



@route('/<docname>')
def view(docname):
  if docname in ["", "/", "index"]:
    docname = "index"
  if not (docname in pages_src):
    docname = "404"
  return pages_src[docname]

def main():
  host = '0.0.0.0'
  port = 9416
  # print(f"server running on http://{host}:{port}/")
  # print("(copy link or click to open in browser)")
  load_pages()

  run(host=host, port=port)

main()

