
# Base CMS

## A cms system designed to be paired with orbitalnotes.


## Section Templates

Templates have "sections", which allow one template to be applied
to another document, and this also allows for the template information
to be embedded as comments in a static document.


The edited pages are designed to be checked into version control.
