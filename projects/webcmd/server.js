

const Koa = require('koa')
const router = require('koa-router')()
const bodyParser = require('koa-bodyparser')
const fs = require('fs/promises')
const data = require('./data.js')

const UserCookie = "SESSION_ID"

const app = new Koa()
const pages = {}
const sections = {}
const page_files = {
  "index": "./pages/index.html",
  "about": "./pages/about.html",
  "account": "./pages/account.html",
  "noaccount": "./pages/noaccount.html",
  "submit": "./pages/submit.html"
}
// TODO section_files: header, footer, nav, etc.  Automatically added to vars in render
const section_files = {
  "header" : "./sections/header.html",
  "footer" : "./sections/footer.html",
  "nav" : "./sections/nav.html"
}


loadPages = async ()=> {
  for(let name in section_files){
    let ref = section_files[name]
    sections[name] = await fs.readFile(ref, {encoding: 'utf8'})
  }
  for(let name in page_files){
    let ref = page_files[name]
    let src = await fs.readFile(ref, {encoding: 'utf8'})
    // fill sections
    // "fixed point" algorithm, apply sections until
    // no changes are made.
    let changed = true
    while(changed){
      changed = false
      for(let k in sections){
        let ref2 = "{{" + k + "}}"
        let value = sections[k]
        if(src.indexOf(ref2) > 0)
          changed = true
        src = src.replaceAll(ref2, value)
      }
    }
    pages[name] = src
  }
}


template = (src, vars)=>{
  for(let k in vars){
    let ref = "{{" + k + "}}"
    let value = vars[k]
    src = src.replaceAll(ref, value)
  }
  return src
}

//copy = x=>JSON.parse(JSON.stringify(x))

render = async (ctx, pagename, vars)=>{
  if(!vars) vars = {}

	cookie = ctx.cookies.get(UserCookie)
	let username = cookie? await data.getCookieUser(cookie) : "Guest"
	if(!username) username = "Guest"
  vars.username = username 

  vars.date = "" + new Date()
  if(!vars.err) vars.err = ""
  ctx.body = template(pages[pagename], vars)
}

router.get('/', async ctx => {
  await render(ctx, 'index')
})

router.get('/about', async ctx => {
  await render(ctx, 'about')
})

router.get('/account', async ctx => {
	cookie = ctx.cookies.get(UserCookie)
	let username = cookie? await data.getCookieUser(cookie) : null
  if(username){
    await render(ctx, 'account')
  } else {
    await render(ctx, 'noaccount')
  }
})

router.post('/submit', bodyParser(), async ctx => {
  let x = ctx.request.body
  console.log(x)
  let action = x.action
  if(action == "signup"){
    // TODO add status messages to login and signup pages.
    if(x.password != x.password2){
      return await render(ctx, 'signup', {err: "Password Mismatch"})
    }
    if(!await data.newUser(x.username, x.password)){
      return await render(ctx, 'signup', {err: "User Exists"})
    }
    // autologin
    let cookie = await data.newSession(x.username)
		ctx.cookies.set(UserCookie, cookie)
  }

  else if(action == "login"){
		if(!await data.userAuth(x.username, x.password)){
      return await render(ctx, 'login', {err: "Invalid Username or Password."})
		}
    let cookie = await data.newSession(x.username)
		ctx.cookies.set(UserCookie, cookie)
  }

  else if(action == "logout"){
	  cookie = ctx.cookies.get(UserCookie)
    if(cookie){
      rmCookie(cookie)
		  ctx.cookies.set(UserCookie, null)
    }
  }

  else{
    console.error("Invalid Form Action")
  }

  ctx.redirect('/')
  //ctx.body = pages
})

test = async ()=>{
  console.log("server tests")
  if(!await data.newUser("joe", "sesame1"))
    console.log(" cannot add user 'joe'")
  console.log(" user auth test: " + await data.userAuth("joe", "sesame1"))
  console.log(" bad user auth test: " + await data.userAuth("joe", "pirates"))
  console.log('tests finished.')
}


main = async ()=>{
  await Promise.all([
    loadPages(), data.init()])
  app.use(router.routes())
  app.listen(3000)
  await test()
  console.log('listening on port 3000')
}

main()
