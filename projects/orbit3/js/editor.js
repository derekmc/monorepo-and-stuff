
// Embedded editor
//

function renderEditor(frame){
  frame.srcdoc = `
    <textarea id='tt'></textarea>
    <script>
      let data = {};
      let UpdateTime = 3000;
      window.addEventListener("load", init);
      let temp = {};
      function init(){
        //alert("init");
        tt.value = "Replace this text";
        window.addEventListener("message", onMessage);
        setInterval(update, UpdateTime);
      }
      function onMessage(e){
        //alert(e);
        let msg = e.data;
        if(msg.action == "gettext"){
          window.parent.postMessage({
            action: "settext",
            value: tt.value,
          });
        }
        if(msg.action == "settext"){
          tt.value = msg.value;
        }
      }

      function update(){
        if(tt.value == temp.lastValue) return;
        temp.lastValue = tt.value;
        console.log("Updating. . .");
        let msg = {
          action: "settext",
          value: tt.value
        }
        window.parent.postMessage(msg);
      }

      function escapeHtml(s){
        let p = document.createElement("p");
        p.appendChild(document.createTextNode(s));
        return p.innerHTML;
      }
    </script>
    <style>
      textarea{
        font-family: monospace;
        font-size: 14px;
        padding: 4px 20px;
        border: none;
        background: #fff;
        font-weight: bold;
        width: 100vw;
        height: 100vh;
        overflow-wrap: break-word;
        outline: none;
        overflow-x: hidden;
      }
      body{
        overflow: hidden;
        margin: 0; padding: 0;
        background: #000;
      }
    </style>
  `
}
