let ui = {};
window.addEventListener("load", main);

let demo_src = `
<h1> Hello </h1>
<h3> This is a demo.</h3>
`

let page_src = `
  <div class="controls">
    <button> Portrait </button>
    <button> Landscape </button>
  </div>
  <iframe id='edit'></iframe>
  <iframe id='demo'></iframe>
`

function main(){
  document.body.innerHTML = page_src;
  let landscape = window.innerWidth > window.innerHeight;
  document.body.className = landscape? "landscape" : "portrait";
  window.addEventListener("message", onMessage);
  ui.edit = id('edit');
  ui.demo = id('demo');
  renderEditor(ui.edit);
  let msg = {
    action: "settext",
    value: demo_src
  };
  setTimeout(
    ()=>{ui.edit.contentWindow.postMessage(msg);},
    500);

  ui.demo.srcdoc = demo_src;
  registerButtons();
}

function registerButtons(){
  let buttons = document.getElementsByTagName("button");
  for(let i=0; i<buttons.length; ++i){
    let button = buttons[i];
    let name = button.innerHTML.trim();
    button.onclick = ((name)=>()=>buttonAction(name))(name);
  }
}

function buttonAction(name){
  if(name == "Portrait"){
    document.body.className = "portrait";
  }
  if(name == "Landscape"){
    document.body.className = "landscape";
  }
}

function onMessage(e){
  let msg = e.data;
  if(msg.action == "settext"){
    ui.demo.srcdoc = msg.value;
  }
}

function id(x){
  return document.getElementById(x);
}
