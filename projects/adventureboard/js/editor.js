
const Data_Key = "AdventureBoard_data_0000"
const SaveInterval = 2000
const labelCycle = 2000
const animatedt = 250
const simpleLabelSize = 14
const smallLabelSize = 12
const glyphWidthRatio = 9.9/16
const glyphHeightRatio = 19/16
const cursorOffset = 6
const pixsize = 1.0
const bwidth = 1.0
const buttonMargin = 3
const marginTop = 35 
const maxWordWrap = 8 // how many characters to break a newline early looking for a space.
//const hidelabels = true

temp.select = null
temp.textWindow = null
//temp.select = [30,999]

//const maxlines = 20
//const linewrap = 80
//const linetrim = 35; // only show part of the line.
const textchunk = 35
const textwindow = 300; // number of characters on screen
const repeatdt = Math.round(1000/6)
//const textwindow = 1800
//const windowlines = 14
// the display labels for the buttons
const simpleLabels = {
  a: 'a', t: 't', i: 'i', l: 'l',
  r: 'r', n: 'n', o: 'o', e: 'e',
  s: "shift", c: "space"
}
const letterLabels = {
  "a" : "j   v\nw   f",
  "t" : "b   y\ng   k",
  "i" : "j   b\nc   p",
  "l" : "v   y\nh   x",
  "r" : "c   h\ns   u",
  "n" : "p   x\nm   d",
  "o" : "w   g\ns   m",
  "e" : "f   k\nu   d",
  "s" : "shift",
  "c" : "space",
}
const symbolLabels = {
  "a" : "~   !\nq dn pd",
  "t" : "(   )\nz < >",
  "i" : "'   \"\n` ^ #",
  "l" : "0   @\n% & |",
  "r" : "rk  _\nup * ->",
  "n" : "+   -\npu = <-",
  "o" : "{   }\n: $ /",
  "e" : "esc sel\n[ ] \\",
  "s" : "shift",
  "c" : "space",
}

let buttonLabels = 
 ["1", "2", "3", "4", "5",
  "6", "7", "8", "9", "10"]
const delay = 1800
const symbolMap = getLabelMap(symbolLabels)
const letterMap = getLabelMap(letterLabels)

function getLabelMap(labels){
  let result = []
  for(let i=0; i < 10; ++i){
    let ch = buttonAlphabet[i]
    result[i] = labels[ch]
  }
}


// the canvas fits better if you use css for layout
// and don't require pixel perfect mapping

function escapeHTML(str){
  let p = document.createElement("p");
  p.appendChild(document.createTextNode(str));
  return p.innerHTML;
}

function saveData(items){
  //let _data = JSON.parse(JSON.stringify(data))
  let _data = data
  //console.info('saveItems', _data, items)
  saveItems(_data, items)
  localStorage[Data_Key] = JSON.stringify(_data)
}
function loadData(items){
  if(Data_Key in localStorage){
    data = JSON.parse(localStorage[Data_Key])
    loadItems(data, items)
  }
}

function loadItems(data, items){
  if(!('items' in data)) return
  let saved = data.items
  for(let name in items){
    let item = items[name]
    if(name in saved){
      item.setData(saved[name])
    }
  }
}


function saveItems(data, items){
  data.items = {}
  for(let name in items){
    let item = items[name]
    data.items[name] = item.getData()
  }
  //alert("saveItems: " + JSON.stringify(data.items))
}
function init(){
  let textwin = items['text_window'] = new TextWindow('TextWindow init')
  //textwin.text = 'asdf'
  //alert("items: " + JSON.stringify(items))
  loadData(items)
  data.settings.labelType ??= 'letters'
  if(data.buttonkeys && data.buttonkeys.length < 10){
    delete data['buttonkeys']
  }
  window.addEventListener("keydown", keydown)
  window.addEventListener("keyup", keyup)
  window.addEventListener("resize", show)
  let cc = document.querySelector('#cc')
  //if(!window.hasOwnProperty('cordova'){
    cc.addEventListener("mousedown", mousedown)
    cc.addEventListener("mouseup", mouseup)
    cc.addEventListener("mouseout", mouseout)
    cc.addEventListener("mousemove", mousemove)
  //}
  if(typeof touchstart != 'undefined'){
    cc.addEventListener("touchstart", touchstart)
    cc.addEventListener("touchmove", touchmove)
    cc.addEventListener("touchend", touchend)
  }
  status("init")
  setInterval(refreshOverlay, animatedt)
  setInterval(saveData, SaveInterval, items)
  //drawOverlay()
  //status("init finished.")
  //setTimeout(()=>status(""), 1000)
}

function drawOverlay(){
  if(!document.querySelector('#textout')) return
  let {
    labelType, buttonOpacity,
    buttonWidth, buttonHeight,
    buttonTop, buttonCenter
  } = data.settings
  if(labelType == 'none')
    buttonOpacity = 0
  const cc = document.querySelector("#cc")
  let W = window.innerWidth
  let H = window.innerHeight
  let MinSize = Math.min(W,H)
  let MaxSize = Math.max(W,H)
  let vert = H>W
  let w = buttonWidth
  let h = buttonHeight
  let m = buttonMargin
  textout.style.left = ((W - 96*MinSize)/2) + "px"
  textout.style.right = ((W - 96*MinSize)/2) + "px"
  let keyboard = data.settings.screenKeyboard
  if(keyboard == 'off') return
  cc.width = (W - 4)/pixsize
  cc.height = (H - 4)/pixsize
  let ctx = cc.getContext("2d")
  ctx.clearRect(0, 0, W, H)
  let blue = "rgba(0,0,255,0.2)"
  let blue2 = "rgba(0,0,255,0.4)"
  let blue3 = "rgba(0,0,255,0.6)"
  let white = "rgba(255,255,255, 1.0)"
  let white2 = "rgba(255,255,255, 0.35)"
  let black = "rgba(0,0,0, 0.35)"
  ctx.fillStyle = black
  ctx.textBaseline = "middle"
  ctx.textAlign = "center"
  // if(Math.random() < 0.5) return

  let cycle = labelType == 'cycle'
  let t1 = 0, t2 = 0, t3 = 0
  if(cycle){
    let i = modTime(labelCycle, 3)
    t1 = Math.max(0, 3*cosTime(3*labelCycle) - 2)
    t2 = Math.max(0, 3*cosTime(3*labelCycle, 0.333) - 2)
    t3 = Math.max(0, 3*cosTime(3*labelCycle, 0.667) - 2)
    //t1 = Math.random()
    //t2 = 1 - t1
    //labelType = ['simple', 'letters', 'symbols'][i]
  }
  //console.info('t1, t2, label, next', t1, t2, labelType)
  //console.info('labels', buttonLabels)
  let labelSize = (labelType == "simple" && temp.buffer?.length == 0) ? simpleLabelSize : smallLabelSize
  ctx.font = `bold ${labelSize}pt Arial`
  // button map
  let bmap = "243168750--9".split("")
  bmap[9] = bmap[10] = '-1'
  bmap[8] = '10'
  //let offset = (1-buttonTop)*H
  let maxOffset = (H - 3*h - m/2 - marginTop)/H
  let offset = 3*h + m/2 + marginTop + maxOffset*buttonTop*H
  let x0 = buttonCenter * (W/2 - 2*w - m/2)
  let direct = vert? -1 : -1
  for(let i = 0; i<3; ++i){
    for(let j=0; j<4; ++j){
      let k = j + 4*i
      let b = bmap[k]
      let x = j<2? x0 + bwidth*w*j : W - bwidth*(4*w - w*j) - x0 - m
      if(b == 9) x -= bwidth*w
      let ww = (b == 9 || b == 10) ? 2 * bwidth * w - 2*m : bwidth*w - 2*m
      let y = offset + direct*h*(i+1)-m

      let shift = b==9 && temp.shift && temp.buffer?.length < 1
      let highlight = temp.activeButtons.includes(parseInt(b))
      if(shift) highlight = true;      
       
      //ctx.strokeText("A", 10, 40)
      let label = buttonLabels[b - 1]
      
      let label1, label2 // rows
      let sp = label?.indexOf('\n')
      if(sp >= 0){
        label1 = label.substr(0,sp)
        label2 = label.substr(sp + 1)
      }
      if(!label) continue
      if(temp.ctrl) label = "*" + label
      if(temp.shift && label.length == 1){
        label = label.toUpperCase()
      }

      let bgOpacity = 0.33 + 0.67*(buttonOpacity)
      let bgFill = buttonOpacity == 0? `rgba(200, 200, 200, ${bgOpacity})` : `rgba(0, 0, 0, ${bgOpacity})`
      let labelColor = `rgba(255, 255, 255, ${Math.pow(buttonOpacity, 0.5)})`
      let borderColor = `rgba(255, 255, 255, ${Math.pow(buttonOpacity, 0.5)})`
      if(keyboard == 'displayOnly'){
        bgFill = buttonOpacity == 0? `rgba(255, 100, 100, ${0.8-bgOpacity})` : `rgba(255, 130, 130, ${bgOpacity})`
        borderColor = `rgba(255, 130, 130, ${buttonOpacity})`
      }
      ctx.fillStyle = highlight? blue :  bgFill
      ctx.strokeStyle = borderColor
      ctx.lineWidth = 2
      ctx.beginPath()
      ctx.strokeRect(x+m, y+m, ww, h-2*m)
      ctx.stroke()
      ctx.fillRect(x+m, y+m, ww, h-2*m)
      if(buttonOpacity > 0 && labelType != 'none'){
        let opacity = buttonOpacity
        if(false && cycle){
          opacity = Math.round(100*t1 * (1 - 0.8*(1 - buttonOpacity)))/100
        }
        //labelColor = `rgba(0, 0, 0, ${opacity})`
        ctx.fillStyle = labelColor
        if(highlight){
          ctx.fillStyle = `rgba(255, 255, 255, ${opacity})`
        }
        if(label1){
          ctx.fillText(label1, x + ww/2 + 3, y + 2*h/7)
          ctx.fillText(label2, x + ww/2 + 3, y + 5*h/7)
        } else {
          ctx.fillText(label, x + ww/2 + 2, y + h/2)
        }
      }
      ctx.globalCompositeOperation = "source-over"
    }
  }
}
function status(msg){
  let text = document.createTextNode(msg)
  let statusdiv = document.querySelector("#status-div")
  statusdiv.innerHTML = ""
  statusdiv.appendChild(text)
  setTimeout(()=>statusdiv.innerHTML = "", 5000)
}
// thumb keyboard
function renderText(){
  if(!document.querySelector('#textout')) return

  //let text = data.text.substr(0, curs) + "|" + data.text.substr(curs)
  let textFontSize = data.settings.textFontSize
  let glyphWidth = glyphWidthRatio * textFontSize
  let glyphHeight = glyphHeightRatio * textFontSize

  let ratio = data.settings.textAspectRatio
  let winw = window.innerWidth, winh = window.innerHeight
  if(winw < winh) ratio = 1/ratio
  let width = Math.min(winw, winh*ratio)
  let height = width/ratio
  let rows = Math.round(height / glyphHeight)
  let cols = Math.round(width / glyphWidth)
  // console.info('rows, cols', rows, cols)


  textout.style.fontSize = textFontSize
  let vw = Math.round(100*width/winw)
  let vh = Math.round(100*height/winh)
  textout.style.width = `calc(${vw}vw - 13px)`
  textout.style.height = `calc(${vh}vh - ${marginTop + 13}px)`
  if(data.settings.buttonTop < 0.5){
    textout.style.top = `calc(${100-vh}vh + ${marginTop}px)`
  } else {
    textout.style.top = `${marginTop}px`
  }

  let tw = items['text_window']
  //temp.textWindow = new TextWindow(data.text)

  tw.config({width: cols, height: rows,
    wrap: data.settings.lineWrap})
  //tw.endSelect()
  tw.renderHTML(textout)
}

function show(){
  document.body.classList[data.settings.mirrorImage? 'add' : 'remove']('mirrorImage')
  renderText()

  //status(temp.activeButtons.join(','))
  updateButtons()
  drawOverlay()
  window.scrollTo(0, document.body.scrollHeight)
}
function refreshOverlay(){
  updateButtons()
  drawOverlay()
}
function updateButtons(){
  let {labelType} = data.settings
  if(labelType == 'cycle'){
    let i = modTime(labelCycle, 3)
    labelType = ['simple', 'letters', 'symbols'][i]
  }
  let labels = buttonLabels
  let type = labelType
  type ??= 'simple'
  for(let i=0; i < 10; ++i){
    let ch = buttonAlphabet[i]
    let prefix = temp.buffer?.length? temp.buffer[0] : ""
    let endch = temp.buffer?.length? temp.buffer[temp.buffer?.length - 1] : ""
    let newbuffer = prefix + ch

    if(data.settings.shiftLock && temp.shiftHold){
      newbuffer = endch + ch
      if(ch == endch){
        newbuffer = temp.buffer.substring(temp.buffer?.length - 2)
      }
      //console.info('newbuffer: ' + newbuffer)
    }
    if(prefix.length || type == "simple"){
      labels[i] = (temp.ctrl? ctrlChars : table)[newbuffer] ?? "_"
    }else if(type == 'letters'){
      labels[i] = letterLabels[ch]
    }else if(type == 'symbols'){
      labels[i] = symbolLabels[ch]
    }else if(type == 'none'){
      labels[i] = ' '
    }else if(type != 'simple'){
      console.warn("unknown label type specified", type)
    }
  }
}

// gets a modulus of the current time, over period in milliseconds
function modTime(period, n){
  let t = Date.now()
  if(!period) period = 1000
  let x = Math.floor(t/period) % n
  return x
}
function cosTime(period, shift){
  shift ??= 0
  let t = Date.now()
  if(!period) period = 1000
  return (1 + Math.cos(Math.PI*(t/period + 2*shift)))/2
}

function zip(a, b){
  let x = {}
  let n = Math.min(a.length, b.length)
  for(let i=0; i<n; ++i){
    x[a[i]] = b[i] }
  return x
}

main()

// shift lock means keys aren't released until the shift key is released.
// it only applies to physical keyboard buttons, not the touch screen(this may be changed).
function defaultSettings(){
  data.settings ??= {}
  // this is because some numpads automatically
  // release keys and repress them when you type chords.
  data.settings.hiccupTimeout = 9
  data.settings.screenKeyboard ??= "touch"
  data.settings.labelType ??= "simple"
  data.settings.buttonOpacity ??= 0.6
  data.settings.buttonWidth ??= 75
  data.settings.buttonHeight ??= 68 
  data.settings.textFontSize ??= 16 
  data.settings.buttonTop ??= 0.1
  data.settings.buttonCenter ??=  0.0
  data.settings.lineWrap ??= true
  data.settings.shiftLock ??= true
  data.settings.hardwareInputMode ??= "chorded"
  data.settings.slideTimeout ??= 0.05
  data.settings.textAspectRatio ??= 1.0
}
async function settingsMenu(){
  document.body.className = ''
  document.body.classList[data.settings.mirrorImage? 'add' : 'remove']('mirrorImage')
  settings.options({
    screenKeyboard: ['touch', 'displayOnly', 'off'],
    hardwareInputMode: ['direct', 'chorded'],
    chordedLayout: ['Right', 'Left', 'Controller', 'Numpad', 'Split', 'Vertical', 'Custom'],
    buttonTop: [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0],
    buttonCenter: [1.0, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
    buttonWidth: [75, 82, 87, 94, 101, 108, 12, 19, 26, 33, 40, 47, 54, 61, 68],
    buttonHeight: [75, 82, 87, 94, 101, 108, 12, 19, 26, 33, 40, 47, 54, 61, 68],
    buttonOpacity: [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0, 0.1, 0.2, 0.3, 0.4],
    textFontSize: [8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 22, 25, 28, 32, 36, 40, 45, 50, 67, 75],
    textAspectRatio: [2, 1.67, 1.25, 1], 
    slideTimeout: [0, 0.01, 0.02, 0.03, 0.05, 0.08],
    labelType: ['letters', 'symbols', 'simple', 'cycle', 'none'],
    lineWrap: [false, true],
    shiftLock: [false, true],
    rightArrowIsSpace: ["End of Text", "Never", "Always"],
    mirrorImage: [false, true],
    hiccupTimeout: [0,3,6,9,12,15],
  })

  if(data.settings){
    settings.values(data.settings)
  }
  // default settings
  defaultSettings()

  let container = document.querySelector('#keyboardContainer')
  settings.show("Keyboard Settings", container)

  let result = await settings.result()
  //console.info('settings', result)
  if(result){
    data.settings = result
    saveData(items)
  }
  main()
}
async function main(){
  defaultSettings()
  document.body.className = 'init'
  document.body.classList[data.settings.mirrorImage? 'add' : 'remove']('mirrorImage')
  let container = document.querySelector('#keyboardContainer')
  container.innerHTML =
    `<div id="textout" class="" style="font-size: ${data.settings.textFontSize};"> Text goes here </div>`
  let div = document.createElement("div")
  div.className = 'keyboardCanvas'
  container.appendChild(div)
  div.innerHTML =
    "<canvas id='cc' class='overlay'></canvas>" +
    "<span id='baseline-div'>" + 
    "  <button onclick=\"buttonAction('menu')\"> Menu </button>" + 
    "  <button onclick=\"buttonAction('text')\"> Text </button>" + 
    "  <button onclick=\"buttonAction('labels')\"> Labels </button>" + 
    "  <br>" +
    "  <span id='status-div'>status</span>" + 
    "</span>" +
    "<style>" +
    "  #cc{ border: none; }" +
    "  .overlay{ position: fixed; top: 0; left: 0; right: 0; bottom: 0" +
    "    width: 100%; height: 100%;" + 
    "    box-sizing: border-box;" +
    "  }" + 
    "</style>"


  initKeyboard()
  init()

  // key layout
  let chordedLayout = data.settings.chordedLayout
  if(chordedLayout == 'Right') data.buttonkeys = "jkl;uiop '".split('')
  else if(chordedLayout == "Controller") data.buttonkeys = "/;p-zsw2x.".split('')
  else if(chordedLayout == 'Left') data.buttonkeys = "asdfqwer g".split('')
  else if(chordedLayout == 'Numpad') data.buttonkeys = ".369025814".split('')
  else if(chordedLayout == 'Split') data.buttonkeys = "zx./as;'q[".split('')
  else if(chordedLayout == 'Vertical') data.buttonkeys = "fghjrtyude".split('')
  else if(chordedLayout == 'Custom'){
    if(!data.buttonkeys) 
      data.buttonkeys = "asdfqwer g".split('')
    status("Custom Keys: " + data.buttonkeys)
  }
  else{
    data.buttonkeys = "asdfqwer g".split('')
    // data.buttonkeys = chordedLayout.split('')
  }

  //await settingsMenu()
  show()
  //registerButtons()

}

function nextLabelType(){
  let t = data.settings.labelType
  let o = data.settings.buttonOpacity
  if(o == 0){
    data.settings.buttonOpacity = 0.2
    setType('simple')
  }else if(t == 'none')
    setType('simple')
  else if(t == 'simple')
    setType('letters')
  else if(t == 'letters')
    setType('symbols')
  else if(t == 'symbols')
    setType('none')
  else if(t == 'cycle')
    setType('none')
  else{
    setType('simple')
  }
  function setType(t){
    data.settings.labelType = t
  }
}

async function buttonAction(name){
  if(name == 'menu'){
    let container = document.querySelector('#keyboardContainer')
    let items = ['Settings', 'Rekey', 'Chord Visualizer', 'Help']
    document.body.className = ''
    document.body.classList[data.settings.mirrorImage? 'add' : 'remove']('mirrorImage')
    let item = await settings.chooseMenu(container, items, "Menu")
    name = item?.toLowerCase()
    if(!name){
      main()
    }
    if(name == 'raw text') name = 'text'
  }
  if(name == 'chord visualizer'){
    window.location.replace('./chordvisualizer.html')
  }
  if(name == 'settings'){
    settingsMenu()
  }
  if(name == 'labels'){
    nextLabelType()
  }
  if(name == 'text'){
    showText()
  }
  if(name == 'rekey'){
    await main()
    rekey()
  }
  if(name == 'help'){
    showHelp()
  }
}
function closeText(){
  let text = document.querySelector('#textedit').value
  let textWindow = items['text_window']
  textWindow.text = text
  textWindow.cursor = text.length
  saveData(items)
  main()
}
function showText(){
  let container = document.querySelector('#keyboardContainer')
  //let container = document.body
  document.body.className = ''
  container.innerHTML = `
    <h3> Raw Text</h3>
    <p>Copy Paste, or Edit </p>
    <textarea id='textedit' rows='12' cols='40'></textarea>
    <br>
   <button onclick='closeText()'> Done </button>
  `

  document.querySelector('#textedit').value =
    items['text_window'].text
}

