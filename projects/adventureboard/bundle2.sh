
File=adventureboard2.html
rm -f $File

files=(
  "template/header.html"
  "css/style.css"
  "template/mid.html"
  "lib/settings.js"
  "js2/keymap.js"
  "js2/button.js"
  "js2/main.js"
  "template/footer.html"
)

for include_file in ${files[@]}; do
  cat $include_file >> $File
  echo >> $File
done

echo "bundle2.sh finished."
