
File=adventureboard.html
rm -f $File

files=(
  "template/header.html"
  "css/style.css"
  "template/mid.html"
  "lib/settings.js"
  "js/textwindow.js"
  "js/keyboard.js"
  "js/editor.js"
  "js/gamepad.js"
  "js/help.js"
  "js/touch.js"
  "template/footer.html"
)

for include_file in ${files[@]}; do
  cat $include_file >> $File
  echo >> $File
done

echo "bundle.sh finished."
