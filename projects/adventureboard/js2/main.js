  
let keyMap, temp
let data = Note.data ?? {}

function defaultSettings(){
  data.settings ??= {}
  data.settings.screenKeyboard ??= "touch"
  data.settings.labelType ??= "simple"
  data.settings.buttonOpacity ??= 0.6
  data.settings.buttonWidth ??= 75
  data.settings.buttonHeight ??= 68 
  data.settings.textFontSize ??= 16 
  data.settings.buttonTop ??= 0.1
  data.settings.buttonCenter ??=  0.0
  data.settings.lineWrap ??= true
  data.settings.hardwareInputMode ??= "chorded"
  data.settings.slideTimeout ??= 0.05
  data.settings.textAspectRatio ??= 1.0
}



function init(){
  data.text ??= ""
  data.cursor ??= 0
  temp = {}
  temp.buffer = []
  temp.pressedButtons = {}
  temp.emitted = false
  keyMap = parseCharTable(charTableText)
}


main()
async function main(){
  init()
  await testPressButtons()
  await testTouchButtons()
}

async function testPressButtons(){
  pressButton(1)
  await sleep(1)
  pressButton(2)
  await sleep(1)
  releaseButton(2)
  await sleep(1)
  pressButton(7)
  releaseButton(7)
  await sleep(1)
}

async function testTouchButtons(){
  buttonTouches([1, 3])
  sleep(1)
  buttonTouches([1])
  sleep(1)
  buttonTouches([2,6])
  sleep(1)
  buttonTouches([])
}
function json(x){
  return JSON.stringify(x)
}

function sleep(n){
  return new Promise((yes, no)=>{
    setTimeout(yes, 1000*n)
  })
}
