# Flo
### A simple page updating pattern

Flo is a minimal front end updating framework.  You provide several "updaters",
using render functions and a container.


## API

### Singleton Instance

flo uses a singleton pattern, you can only have one instance of flo instantiated
at once.


### Functions and Example
```js

    // elem: the id string, or the dom element, to rerender
    // render(data): returns the update
    //   updates can be an html string, or an object
    //   containing one or more of the following properties:
    //     - "html"
    //     - "text"
    //     - "addClass"
    //     - "removeClass"
    //     - "toggleClass"
    //     - "style"
    //   Any other object properties are treated as html
    //   attributes.
    // 
    //   If render returns a nullish value, the element is
    //   not updated. If renders l

    flo.updater({elem: "pageTitle",
      render: renderTitle, textOnly: false})


    // example render function
    function renderTitle(data, id, renderCount){
      if(renderCount == 0)
      	return `<h1> Hello ${data.name}</h1>

			let updates = {}
			updates[data.highlight? 'addClass' : 'removeClass'] = 'highlight'
			updates.toggleClass = 'red'
			return updates
    }
```
