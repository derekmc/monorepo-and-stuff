

window.addEventListener("load", test);

function test(){
  console.log("flo lib: ", flo);
  if(flo === undefined){
    throw new Error("flo library undefined.");
  }
  flo.updater({ elem: "main_div",
                render: mainRender});
    
  setInterval(flo.update, 3);
}
// flo updates an element according to what needs to be changed,
// so you can intelligently control updates
function mainRender(data, id){
  let now = Date.now();
  data.count ??= 0;
  data.start ??= now;
  ++data.count;
  if(Math.random() < 0.90){
    return;
  }
  if(Math.random() < 0.5){
    return `
      <h3> Count: ${data.count} </h3>
      <p> Elapsed: ${Math.floor((now - data.start)/100)/10} </p>
    `;
  }
  console.log("render: " + id);
  if(Math.random() < 0.5){
    return {style: {borderRadius: Math.floor(Math.log2(data.count, 2)) + "px"}};
  }
  return Math.random() < 0.5?
    {addclass: "ul"} : {removeclass: "ul"};
}
