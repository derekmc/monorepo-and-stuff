
// json-markup
// Convert JSON to html

let JsonMarkup = (function(){
  let json_markup = toHtml;
  let impliedTags;

  init();

  return json_markup;

  function init(){
    impliedTags = {
      "": "div",
      "div" : "div",
      "ul": "li",
      "ol": "li",
      "li" : "span",
      "table": "tr",
      "tr" : "td",
      "td" : "span",
      "span" : "bold",
      "bold" : "i",
      "input" : "span",
      "button" : "span",
      "h1" : "span",
      "h2" : "span",
      "h3" : "span",
      "h4" : "span",
      "h5" : "span",
    }
  }


  function toHtml(tree, data){
    //if(!indent) indent = 0;
    //return tree.map(y => render(y, data, undefined, 0)).join("\n");
    return render(tree, data, undefined, 0);

    function render(x, data, parentTag, depth){
      let nextdepth = depth === undefined? depth : depth + 1;
      if(Array.isArray(x) && x.length > 0){
        let tag = x[0];
        let rest = x;
        if(tag[tag.length-1] == ":"){
          rest = rest.slice(1);
        } else {
          if(parentTag && parentTag in impliedTags){
            tag = impliedTags[parentTag];
          } else {
            tag = impliedTags[""];
          }
        }
        let props = {};
        if(typeof rest[0] == "object" && !Array.isArray(rest[0])){
          props = rest[0];
          rest = rest.slice(1);
        }
        let indent = (depth !== undefined)? "\n" : "";
        for(let i=0; i<depth; ++i){
          indent += "  "; }

        let [start, end, tagname] = renderTags(tag, props, parentTag);
        //if(tagname in macros){
        //  return macros[tagname](x, data, (a, data)=> render(a, data, parentTag, depth));
        //}
        start = indent + start;
        end = indent + end;
        //let start = `${indent}<${tag}${propStr(props)}>`
        //let end = `${indent}</${tag}>`
        //return start + end;
        return start + rest.map(a=>render(a, data, tagname, nextdepth)).join(' ') + end;
      } else {
        return "" + x;
      }
    }
    function propStr(props){
      let result = "";
      for(let k in props){
        result += ` ${k}="${trimAndEscapeQuotes(props[k])}"`
      }
      return result;
    }
  }

  function renderTags(s, props, parentTag){
    //return s;
    // TODO parse tag shorthands

    let classes = [];
    let id = null;
    let title = null;
    let tag = null;
    let fg = null;
    let bg = null;
    let propstr = "";
    let separators = {
      '.': x=>classes.push(x),
      '#': x=>`id="${x}"`,
      '$': x=>`name="${x}"`,
      '~': x=>`title="${x}"`,
      '?': x=>`onclick="${x}"`,
      '+': x=>{fg = x},
      '-': x=>{bg = x},
      '': x=>{tag = x},
      ':': x=>"",
    };

    let j = 0;
    let lastc = "";
    for(let i=0; i<s.length; ++i){
      let c = s[i];
      if(c in separators){
        let word = s.substring(j, i);
        if(lastc.length){
          let text = separators[lastc](word);
          if(text && text.length){
            propstr += " " + text;
          }
        } else {
          separators[lastc](word);
        }
        j = i+1
        lastc = c;
      }
    }
    for(let k in props){
      propstr += ` ${k}="${trimAndEscapeQuotes(props[k])}"`
    }
    if(classes.length){
      propstr += ` class="${classes.join(" ")}"`;
    }
    let style = "";
    if(fg) style += `color: ${fg}; `;
    if(bg) style += `background: ${bg}; `;
    if(style.length){
      propstr += ` style="${style.trim()}"`;
    }
    if(tag == null){
      if(parentTag && parentTag in impliedTags){
        tag = impliedTags[parentTag];
      } else {
        tag = impliedTags[""]; // default
      }
    }
    let start = `<${tag}${propstr}>`;
    let end = `</${tag}>`;
    return [start, end, tag];
  }

  function error(message, s, i){
    throw new Error(
      "JsonMarkup: " + message + "\nat \"" +  
      s.substring(Math.max(0, i-15), i) + "\" <");
  }


})();

if(typeof module === "object"){
  module.exports = JsonMarkup;
}
