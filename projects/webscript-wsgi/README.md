# Webscript: A Minimal WSGI based framework
### "build your own framework"

## Getting Started

Copy this repository, and run "python scripts/setup.py" from the base directory.

You can use "git clean -xdf" to clear out all data and configuration
(WARNING, this will delete any files from the current directory not checked into
git, including files in ".gitignore")

## About

Webscript is a copy/paste framework that works right out of the box, in
pure python.  As a "copy/paste" dependency, so you simply
copy the codebase to start your project.  You may want to note
in your documentation if you make any modifications, and what
the original version was.  But webscript is designed to be
simple and minimal enough that most developers should understand
the source code.

Webscript uses a one file = one page, model of web development.
This is inspired by CGI.  I

## Non-features

The following features will never be implemented in webscript.
These are intentionally left up to the programmer to solve.

 * Templates (besides basic text substitution)
 * URL Routing
 * Database support
 * Non-admin user accounts
 * Email

## Features

 * Read or write cookies.
 * Handle post or get requests(post requests include a non-empty data field).
 * Minimal dependencies (python only)
 * Admin Accounts and Admin Pages
 * Setup script

## File Structure

 * __include__:
   These files are available to scripts, but
   not automatically served.
 * __config__:
   These files should be edited to adjust the server,
   but should not by modified by the server itself.
 * __data__:
   Here is where the server should save data.
   Unlike config, 
 * __static__:
   Statically served files.
 * __sitepages__:
   Specific files used by the site itself.
 * __webscript__:
   Put your webscripts here.
   webscripts render pages.


##

we want to get into the paradigm 1 page = 1 file
this calls for minimizing the amount of shared
functionality, which is typically not what you want,
but we can still have shared functionality through 
libraries.

CGI uses this 1 page = 1 file paradigm, and it
is very powerful.  If we want to be able
to imitate it, we should be able to route
a script's location, based only on the location
of that "webscript"

Most webscripts should be mere form processors.


## Dependency free web development

by using only wsgi, we demonstrate how to develop
without dependencies.  Many frameworks have
highly specific behavior and patterns, that are
more difficult to learn than it would be to write
the core functionallity of the framework itself.

The benefit of going "frameworkless" is that these
simple functions, like routing, parsing query strings,
etc, are all done "raw" by the language.

Perl was designed to be able to process strings
and text in a very direct and minimal way, especially
using regular expressions.  So even though we may
be in python now, we want to keep a lot of that
same minimalism and flexibility.
