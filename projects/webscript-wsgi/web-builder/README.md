# Web Builder

### How it works

 -  The "template" property allow pages to specify which template they use.
 -  properties are values that the template uses for substitution.
 -  Default sections: these are autofilled if the property is not defined by the page.
 -  static: the static folder is copied "as is" to the output folder.

### Example

   template: article.html
   body: Hello World

## File structure

the output file structure mirrors the same file structure used in
the content folder.

## temp


