
print("Test")

def is_tri(a, b, c):
  result = a*a + b*b == c*c
  if result:
    print("Right Triangle")
  else:
    print("Not a right triangle")
  return result

ans = is_tri(3,4,5)

print("Is triangle: ", ans)

ans = is_tri(*[3, 4], 7)

print("Is triangle: ", ans)
