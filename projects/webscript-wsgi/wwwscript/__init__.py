import os
import glob
import importlib
try:
    __script_dir = os.path.basename(os.path.normpath(os.path.dirname(os.path.realpath(__file__))))
except:
    __script_dir = "webscript"
__modules = glob.glob(os.path.join(os.path.dirname(__file__), "*.py"))
webscript = [ importlib.import_module(__script_dir + "." + os.path.basename(f)[:-3]) for f in __modules if os.path.isfile(f) and not f.endswith('__init__.py')]
