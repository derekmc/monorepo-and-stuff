import os
import glob
import importlib
__modules = glob.glob(os.path.join(os.path.dirname(__file__), "*.py"))

admin = [ importlib.import_module("webscript.admin." + os.path.basename(f)[:-3]) for f in __modules if os.path.isfile(f) and not f.endswith('__init__.py')]
