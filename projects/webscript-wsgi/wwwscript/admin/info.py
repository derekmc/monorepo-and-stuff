
# Info Page

template = """
<!doctype html>
<html>
  <head>
    <title> Server Config Information </title>
    <style>
      *{ font-family: sans-serif; }
      th{
        text-align: right;
        background: #d0d8ff;
      }
      body{
        background: lightblue;
        max-width: 1000px;
      }
      td, th{
        padding: 8px 12px;
      }
      table{
        background: #fff;
        border: 2px solid black;
        max-width: 1000px;
      }
      h3{
        background: #9af;
        padding: 8px;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <h1> Server Info </h1>

    <p>
      <a href="{{home}}">Home</a> &nbsp; | &nbsp;
      <a href="{{admin_home}}">Admin</a> &nbsp;
    </p>
    <h3> Environment Variables </h3>
    <table>
      {{table_body}}
    </table>
  </body>
</html>
"""

def render(environ={}, setcookies=None,
    path="", query="", data="", cookies=""):
  table_rows = [
    '<tr><th>%s</th><td>%s</td>' %
      (key, value) for key, value in sorted(environ.items())
  ]
  table_body = "".join(table_rows)
  response_body = template.replace("{{table_body}}", table_body)

  return response_body
