
import os
import json
user_folder = "data/users/"
request_file = "data/user_requests.txt"

with open('include/user.html') as file:
  userpage = file.read()

def initfolders():
  try:
    os.mkdirs(user_folder)
    os.mkdirs(request_folder)
  except:
    pass

def render(environ={}, setcookies=None,
    path="", query="", data="", cookies=""):

  message = ""
  initfolders()
  user_requested = cookies.get('user_requested', False)
  if data.get('userform_action',"") == 'request_newuser':
    if user_requested:
      message = "<h2> You already requested a user account.</h2>"
    else:
      with open(request_file, "a") as file:
        file.write(str(data) + "\n")
      message = "<h2> New User Requested </h2>"
      setcookies(f"user_requested=True")
  else:
    print("query", str(query))
    print("data", str(data))
    print('cookies', cookies)

  body = userpage
  body = body.replace("{{message}}", message)
  return body

