
window.addEventListener('load', main);

let ui = {};
function main(){
    ui.cc = document.getElementById("cc");
    ui.ctx = ui.cc.getContext("2d");
    resize();
    draw();
}

function resize(){
    ui.cc.width = 400;
    ui.cc.height = 400;
}

function draw(){
    ui.ctx.fillRect(30, 30, 10, 10);
}