exports.render = render

const fs = require('fs')

let page = ""

fs.readFile('./include/demo.html', 'utf8', (err, data) => {
  if(err) console.error(err)
  else page = data
})

let count = 0

function render({path, query, data, setCookie, cookies}){
  let src = page
  ++count
  /// console.log('query', query)
  src = src.replaceAll("{{count}}", count)
  if('cookiename' in query && 'cookievalue' in query){
    setCookie(query.cookiename, query.cookievalue)
  }
  console.log('cookies', cookies)
  return src
}
