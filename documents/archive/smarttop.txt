# The SmartTop

### A laptop with a smartphone.

## What you need

Required:
 - A bluetooth keyboard
 - A small shipping box, about 6"x9"x2"
 - Tape
 - A Pencil

Optional:
 - 1 pair of shoelaces
 - dark colored paper.
 - reading glasses.
 - attachable velcro strips.

## How it works

Connect the bluetooth keyboard to the smartphone.
This can typically be done by going to settings,
enabling bluetooth, searching for devices, pressing
the bluetooth button on your keyboard, and then
following the pairing instructions.

Once your bluetooth keyboard works, then you can
proceed with the rest of the steps.

Lay the box on a table or flat surface, with
the short end facing you.
The largest face of the box should be the top
and bottom.
Your box will need to be just
a little bit wider than the height of
your cellphone.

You will be essentially


## Keyboard Harness

For this harness, you will need to know 2 knots,
a bowline and a prussik knot.


## Tips and Tricks

 - Don't use this setup for extended periods of time,
   as it generally requires looking down at your phone.
 - 
