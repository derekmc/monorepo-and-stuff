Wearable Cellphone "Typewriter" Office Setup.

To create this setup, you need a few parts, first of all you will need a
smartphone, secondly a bluetooth keyboard.  The other parts can vary, but I find
that a clipboard and PVC, with shoelaces as a lanyard, is ideal.

## The clipboard

### Mounting the cellphone

Initially I mounted the cellphone to the clipboard using standard velcro strips
attached to each.  While this is a good option, there are other options that may
work better, depending on your needs.

When you are first trying this out, the easiest option is to simply place your
cellphone in a ziplock bag, and then tape that to the clipboard, or use the
"clip" to hand the ziplock bag.  I find that if the clip is on the backside of
the clipboard away from you, and the bag is flipped over the top toward you,
that that is the best setup.

Some people may not want to keep velcro strips attached to their phone.  You
can also use double sided adhesive strips, or even duct tape in loops.  Compared
to a ziplockbag, these options allow direct access to the phone and buttons,
without having the bag in the way.

Using duct tape loops to create small strips of "double sided" tape has pros and
cons, but it is the solution I have settled on.  For one thing, I find that I
do not use this setup that often.  In most cases it is preferred to work on an
actual laptop, and not try to pretend your cellphone is a laptop.  When
attaching such strips to the clipboard, it can be helpful to tape down some duct
tape on the clipboard normally, to create a better smoother surface for the loops to
stick to.  I find that 4 or 5 small loops, about half an inch wide when rolled,
and about 2 and a half inches tall creates a good attachment for the back of the
phone to stick to the board.  If the phone is just attached for a few hours, you
shouldn't have any problem with tape residue on the back of the phone.



There are many possible setups for using this office setup.  Perhaps the most
useful setup is basically as a typewriter to write and edit documents, but
mostly to compose documents with minimal user interaction.

It is nice to be able to focus on writing with the keyboard without having to
stare at the screen all the time, especially because the screen is small and it
may not be configured in an optimal viewing position.  Even small changes in
sitting or standing position can change how the screen is positioned for
viewing.

So roughly half the time or more, the user may want to use the typewriter
without staring directly at the screen.  Either staring blankly out into space,
or by closing and relacxing their eyes and focusing on the tactile feel fo the
keyboard. On the other hand, even if the screen is not positioned optimally, it
is helpful to be able to look at and review what one has written.  One may even
remove the clipboard lanyard and simple hold the screen to view it in an optimal
position.


The standard way to use this typewriter is probably a little bit off to tell you
the truth.  We just want a simple platform, slightly tilted toward the user so
they 
