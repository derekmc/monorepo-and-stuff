

# Crop both sides of a VR video to view as SBS prerender,
# So no special video player is required.


WRatio=$(( 100*3/5 ))
HRatio=$(( 100*5/5 ))
yOffset=$(( 100*0/10 ))

#Resolution=720 #output resolution
#ScaleFilter=",scale=640:$Resolution"
#ScaleFilter=""
#Presets="-preset slow -crf 18"
Presets=""
Start=${3:-8}

InFile=$1 
Time=${2:-8} #default 8 seconds
IFS=","
read SourceW SourceH <<< $( ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=p=0 $InFile )

fullw=$(( SourceW/2 ))
fullh=$(( SourceH/1 ))
w=$(( WRatio * SourceW/2/100 ))
h=$(( HRatio * SourceH/1/100 ))
x0=$(( (SourceW/2 - w)/2 ))
y0=$(( yOffset * SourceH/100 ))
x1=$(( x0 ))
y1=$(( y0 ))
LFile="leftcrop_$InFile"
RFile="rightcrop_$InFile"
JoinFile="vr_$InFile"


echo "Resolution: $Resolution"
echo "Source Dimensions: ${SourceW}x${SourceH}"
echo "Output Dimensions: $((2 * w))x${h}"
echo "$JoinFile (x0, y0: $x0 $y0) (x1, y1: $x1 $y1) (w h: $w $h)"

#exit
#ffmpeg -y -i $InFile -vf "crop=$w:$h:$x0:$y0,scale=-1:$Resolution" -t $Time $LFile
#ffmpeg -y -i $InFile -vf "crop=$w:$h:$x1:$y1,scale=-1:$Resolution" -t $Time $RFile
#ffmpeg -y -i $InFile -i $InFile -ss $Start -t $Time -filter_complex \
#  "[0:v]crop=$w:$h:$x0:$y0[c];[1:v]crop=$w:$h:$x1:$y1[d];[d][c]scale2ref[wm][base];[base][wm]hstack=2" $JoinFile

ffmpeg -y -i $InFile -ss $Start -t $Time -filter_complex \
  "[0:v]v360=input=hequirect:output=flat:h_fov=115:v_fov=115:\
in_stereo=sbs:out_stereo=sbs:w=$fullw:h=$fullh[flat];\
[flat]split[left][right];[left]stereo3d=sbsl:ml[left_mono];\
[right]stereo3d=sbsl:mr[right_mono];\
[left_mono]crop=$w:$h:$x0:$y0[left_crop];\
[right_mono]crop=$w:$h:$x0:$y0[right_crop];
[left_crop][right_crop]hstack=2"\
  vr_flat_$InFile

: '

ffmpeg -y -i $InFile -ss $Start -t $Time -filter_complex \
  "[0:v]v360=input=hequirect:output=flat:h_fov=115:v_fov=115:\
in_stereo=sbs:out_stereo=sbs:w=$fullw:h=$fullh[flat];\
[flat]split[left][right];[left]stereo3d=sbsl:ml[left_mono];\
[right]stereo3d=sbsl:mr[right_mono];\
[left_mono][right_mono]hstack=2"\
  vr_flat_$InFile

'



#[left_mono]crop=$w:$h:$x0:$y0[left_crop];\
# -map [left_mono] vr_left_$InFile -map [right_mono] vr_right_$InFile
#rm $LFile
#rm $RFile
