#!/usr/bin/sh
# This is a "typewriter shell"
# It is useful on systems without a screen, and "autologin enabled"
# For example, you can set this as the shell for the default user 
# on a raspberry pi, and use "sudo raspi-config" to enable autologin.
# after that you just need to plug in the pi, wait for it to boot,
# and then you can start typing, no screen required, but no editing
# ie, a "typewriter".
# you may need to update some config file such as /etc/shells
# and then run a command to set that user's default shell.
FILE=/home/pi/Documents/typewriterlog-`date +%Y_%m%d_%H%M`.txt
echo "Welcome to typewriter shell!"
echo "Type some text, and every time you press enter it will be appended to the log file."
echo "File: ${FILE}"
echo "Typewriter Log: `date`" >> $FILE
echo "" >> $FILE
cat >> $FILE
