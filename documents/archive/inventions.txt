I have been working a lot with wearable computing lately.  There are some things
that you can easily create, like a clipboard cellphone holder with a hanging
bluetooth keyboard.

But one design that may not be so obvious is based on the a variation of poker
where everybody else can see what cards you have.
This could not only function as a wearable computer, but also as an
accessibility device.  If you mount a cellphone to your chest, chin, or
forehead, then any mirror becomes a viewer.  Or you could use said device to
communicate textually with those around you.

Imagine that when you type, large words appear on a screen on your chest,
this would be beneficial to mute or deaf people, but also people who simply have
little desire to talk.
