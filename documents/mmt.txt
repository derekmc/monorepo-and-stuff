MMT:

Technical Principles
 - Endogenous Money
 - Consolidated View of Government Finance
 - Buffer Stocks and Price Anchoring

Conceptual or Philosophical Principles
 - Legal Theory of Money
 - Financial Unification
 - Public Value

1. Better relate this to conventional viewpoints.
2. Generalize principles and formulate independent axioms.

"The fiscal bidding theory of prices"
