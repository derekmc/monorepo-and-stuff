
const Data_Key = "LayoutMap_0003"
const Save_Interval = 5000
function id(x){
  return document.getElementById(x)
}
let data = {}
let keymap = {}
let capsmap = {}
let defaultCaps = `
  \`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
    qQ wW eE rR tT yY uU iI oO pP [{ ]} \\|
    aA sS dD fF gG hH jJ kK lL ;: '"
    zZ xX cC vV bB nN mM ,< .> /?`

let builtinLayouts = {
  'qwerty':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       qQ wW eE rR tT yY uU iI oO pP [{ ]} \\|
       aA sS dD fF gG hH jJ kK lL ;: '"
       zZ xX cC vV bB nN mM ,< .> /?`,
  'dvorak':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) [{ ]}
       '" ,< .> pP yY fF gG cC rR lL /? += \\|
       aA oO eE uU iI dD hH tT nN sS -_
       ;: qQ jJ kK xX bB mM wW vV zZ`,
  'colemak':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       qQ wW fF pP gG jJ lL uU yY ;: [{ ]} \\|
       aA rR sS tT dD hH nN eE iI oO '"
       zZ xX cC vV bB kK mM ,< .> /?`,
  'colemak-dh':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       qQ wW fF pP bB jJ lL uU yY ;: [{ ]} \\|
       aA rR sS tT gG mM nN eE iI oO '"
       xX cC dD vV zZ kK hH ,< .> /?`,
  'aptv3':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       wW gG dD fF bB qQ lL uU oO yY [{ ]} \\|
       rR sS tT hH kK jJ nN eE aA iI ;: 
       xX cC mM pP vV zZ ,< .> '" /?`,
  'canary':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       wW lL yY pP kK zZ xX oO uU ;: [{ ]} \\|
       cC rR sS tT bB fF nN eE iI aA ;: 
       jJ vV dD gG qQ mM hH /? ,< .>`,
  'gallium':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
       bB lL dD cC vV zZ yY oO uU ,< [{ ]} \\|
       nN rR tT sS gG pP hH aA eE iI /? 
       qQ xX mM wW jJ kK fF '" ;: .>`,       
  'graphite':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) [{ ]}
       bB lL dD wW zZ '_ fF oO uU jJ ;: =+ \\|
       nN rR tT sS gG yY hH aA eE iI ,? 
       qQ xX mM cC vV kK pP .> -" /<`,
  'zilted-graphite':
    `\`~ bB qQ lL dD 4$ 5% 6^ oO uU ;: jJ 7&
      nN rR tT wW zZ '_ fF aA eE iI ,? =+ \\|
      1! xX mM sS gG yY hH .> -" /< 8*
      2@ 3# cC vV 0) kK pP [{ ]} 9(`,
  'zilted-qwerty' :
    `\`~ qQ zZ wW eE 4$ 5% 6^ iI oO '" pP 7&
      aA sS dD rR tT yY uU kK lL ;: [{ ]} \\|
      1! xX cC fF gG hH jJ ,< .> /? 8*
      2@ 3# vV bB 0) nN mM -_ += 9(`,
  'recurva':
    `\`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
      fF rR dD pP vV qQ mM uU oO yY [{ ]} \\|
      sS nN tT cC bB .< hH eE aA iI '" 
      zZ xX kK gG wW jJ lL ;: '" ,<`,
}
defaultCaps.trim().split(/\s+/).
  forEach(x =>{ capsmap[x[0]] = x[1]})

// for now, layouts all involve single
// character buttons, and only the main section
// of the keyboard, not control buttons or
// anything else.
function init(){
  data.systemLayout = 'qwerty'
  data.inputLayout = 'qwerty'

  if(!data.layouts){
    data.layouts = JSON.parse(JSON.stringify(builtinLayouts))
  }
  loadBuiltinLayouts()
}
function loadBuiltinLayouts(){
  if(!data.layouts){
    data.layouts = {}
  }
  for(let name in builtinLayouts){
    data.layouts[name] = builtinLayouts[name]
  }
}

function selectTemplate(){
  let layout_options = layoutOptions()
  return `
  <h3> Select Layouts </h3>
  System:
  <select id='systemLayoutSelect'
    onchange='systemSelected()' >
    ${layout_options}
  </select>
  &emsp;
  Input:
  <select id='inputLayoutSelect'
    onchange='inputSelected()' >
    ${layout_options}
  </select>`
}
function layoutOptions(){
  let list = Object.keys(data.layouts ?? {})
  let layout_options = list.map(
    (x,i) => `<option value='${x}' ${i==0? 'selected="selected"': ''}>
      ${x in builtinLayouts? '*':''}${x}</option>`).join('\n')
  return layout_options
}
function deleteEditLayout(){
  let edit = id('editLayoutSelect').value
  if(edit in builtinLayouts){
    data.layouts[edit] = builtinLayouts[edit]
  } else {
    delete data.layouts[edit]
    id('editLayoutSelect').value = 'qwerty'
    document.getElementById('keyboardSelect').
      innerHTML = selectTemplate()
    document.getElementById('editLayoutSelect').
      innerHTML = layoutOptions() + "<option value='(new)'>(new)</option>"
  }
  updateLayouts()
}
function updateEditLayout(){
  let layout_name = id('editLayoutSelect').value
  data.layouts[layout_name] =
    id('editLayoutTextarea').value
  updateLayouts()
}
function editSelected(){
  id('inputLayoutSelect').value = id('editLayoutSelect').value
  updateLayouts()
}
function inputSelected(){
  id('editLayoutSelect').value = id('inputLayoutSelect').value
  updateLayouts()
}
function systemSelected(){
  updateLayouts()
}
function updateLayouts(){
  let sys = data.systemLayout = id('systemLayoutSelect').value
  let inp = data.inputLayout = id('inputLayoutSelect').value
  let edit = data.editLayout = id('editLayoutSelect').value

  if(sys == '') sys = data.systemLayout = id('systemLayoutSelect').value = 'qwerty'
  if(inp == '') inp = data.inputLayout = id('inputLayoutSelect').value = 'qwerty'
  if(edit == '') edit = data.editLayout = id('editLayoutSelect').value = 'qwerty'
  if(edit == '(new)'){
    let newlayout = prompt('New Layout Name:')
    if(!newlayout){
      edit = data.editLayout = id('editLayoutSelect').value = 'qwerty'
    } else {
      edit = newlayout
      let layout = id('editLayoutTextarea').value
      if(!layout.trim().length){
        layout = 
          data.layouts[inp] ??
          data.layouts[sys] ??
          data.layouts['qwerty']
      }
      data.layouts[newlayout] = layout
      document.getElementById('keyboardSelect').
        innerHTML = selectTemplate()
      document.getElementById('editLayoutSelect').
        innerHTML = layoutOptions() + "<option value='(new)'>(new)</option>"
      id('editLayoutSelect').value = newlayout
    }
  }
  if(data.layouts){
    keymap = makeKeyboardMap(data.layouts[sys], data.layouts[inp])
    let layout = data.layouts[edit] ??
      data.layouts[inp] ??
      data.layouts[sys] ??
      data.layouts['qwerty']
    id('editLayoutTextarea').value = layout.
      split('\n').map(x=>x.trim()).join('\n')
  }
  id('testoutput').focus()
}
function quizLink({hide}){
  let inp = data.inputLayout
  let edit = data.editLayout
  let sys = data.systemLayout

  let layout = data.layouts[edit] ??
    data.layouts[inp] ??
    data.layouts[sys] ??
    data.layouts['qwerty']
  // alert('layout: ' + layout)
  lines = //['1', '2', '3']
    layout.split('\n').
      map((row, i) => row.split(/\s+/g).
        slice(i==0? 1:0, 11).map(x => x[0]).join(''))
  layoutarg = encodeURIComponent(lines.join('\n'))
  console.info('layoutarg', layoutarg)
  // alert('layoutarg: ' + layoutarg)
  let href = './layoutquiz.html?' +
    `hide=${hide}&layout=${layoutarg}`
  // alert('href: ' + href)
  window.location.href = href
}

function rmws(x){
  return x.replace(/\s/g, "")
}
function fillRows(original, custom){
  let a = original.trim().split("\n").map(
   x=>x.split(/\s+/))
  let b = custom.trim().split("\n").map(
   x=>x.split(/\s+/))
  while(b.length < a.length){
    b.unshift(a[a.length - b.length - 1])}
  for(let i=0; i<a.length; ++i){
    let aa = a[i], bb = b[i]
    while(bb.length < aa.length)
      bb.push(aa[bb.length + 1])
    if(bb.length > aa.length)
      bb.length = aa.length
  }
  let s = b.map(x=> x.join(' ')).join('\n') 
  return s
}
function addCaps(s){
  let words = s.split(/\s+/)
  for(let i=0; i<words.length; ++i){
    let w = words[i]
    if(w.length == 1)
      words[i] += capsmap[w[0]]
  }
  return words.join(' ')
}
function makeKeyboardMap(systemLayout, inputLayout){
  if(!systemLayout || !inputLayout) return
  let keymap = {}
  sys = rmws(addCaps(systemLayout))
  inp = rmws(addCaps(fillRows(
    data.layouts.qwerty, inputLayout)))
  /*if(inp.length < sys.length){
    inp = sys.substring(0,
      sys.length - inp.length) + inp
  }*/
  let n = Math.min(sys.length, inp.length)
  for(let i=0; i<n; ++i){
    keymap[sys[i]] = inp[i]
  }
  return keymap
}

setTimeout(main, 500)

function main(){
  let items = {}
 
  init()
  let text = "This is a test."
  let testdiv = id('testoutput')
  let editor = items['text_editor'] = 
    textEditor(null, testdiv, text)

  loadData(items)
  
  let urlParams = new URLSearchParams(window.location.search);
  let layoutIndex = parseInt(urlParams.get('layout'));
  if(layoutIndex > 0) data.inputLayout = Object.keys(builtinLayouts)[layoutIndex - 1]
  loadBuiltinLayouts()
  setInterval(saveData, Save_Interval, items)
  if(data.layouts){
    document.getElementById('keyboardSelect').
      innerHTML = selectTemplate()
    document.getElementById('editLayoutSelect').
      innerHTML = layoutOptions() + "<option value='(new)'>(new)</option>"
    id('systemLayoutSelect').value = data.systemLayout
    id('inputLayoutSelect').value = data.inputLayout
    id('editLayoutSelect').value = data.inputLayout
  }
  updateLayouts()
  editor.renderHTML(testdiv)
}

function saveData(items){
  //let data = {}
  saveItems(data, items)
  localStorage[Data_Key] = JSON.stringify(data)
  //delete data['items']
}

function loadData(items){
  if(Data_Key in localStorage){
    data = JSON.parse(localStorage[Data_Key])
    loadItems(data, items)
    //delete data['items']
  }
}

// uses getters and setters on data field
function saveItems(data, items){
  data.items = {}
  for(let name in items){
    let item = items[name]
    data.items[name] = item.data
  }
}

function loadItems(data, items){
  if(!('items' in data)) return
  let saved = data.items
  for(let name in items){
    let item = items[name]
    if(name in saved){
      item.data = saved[name]
    }
  }
}

function textEditor(target, container, text, config){
  if(!text) text = 'replace text'
  text = text + text + text + '\n'
  text = text + text + text
  let tw = new TextWindow(text)
  let clipboard = ""

  //updateLayouts()
  //tw.text = text
  //tw.config(config)
  tw.config({wrap: true})
  
  let editElement = tw.renderHTML(container)
  if(!target) target = editElement
  target.addEventListener('keydown', keydown)
  console.log('editor ready')
  
  return tw
  async function keydown(e){
    e.preventDefault()
    // some platforms dont seem to get all keynames correct
    let codes = {
      37: 'left', 38: 'up', 39: 'right', 40: 'down',
      13: 'enter', 8: 'backspace', 46: 'delete'
    }
    let code = e.keyCode
    let key = e.key
    if(key in keymap) key = keymap[key]
    let k = key.toLowerCase()
    if(code in codes) k = codes[code]
    if(e.ctrlKey){
      if(k == 'x') await cut()
      if(k == 'c') await copy()
      if(k == 'v') await paste()
      if(k == 'a'){
        tw.endSelect()
        tw.cursor = tw.text.length
        tw.startSelect()
        tw.cursor = 0
      }
      tw.renderHTML(container)
      return
    }
    if(k.indexOf('arrow') == 0) k = k.substring(5)
    //if(k == "shift") tw.startSelect()
    if(k == "left") tw.moveLeft(e.shiftKey)
    if(k == "up") tw.moveUp(e.shiftKey)
    if(k == "right") tw.moveRight(e.shiftKey)
    if(k == "down") tw.moveDown(e.shiftKey)
    if(k == "backspace") tw.backspace()
    if(k == "delete") tw.deleteAction()
    if(k == "enter") tw.insert('\n')
    if(key.length == 1) tw.insert(key)
    tw.renderHTML(container)
    return false
  }
  async function readClip(){
    let s = clipboard
    try{
      s = await navigator.clipboard.readText()
    }catch(e){
      console.warn('cannot read user\'s clipboard', e)
    }
    return s
  }
  async function writeClip(s){
    clipboard = s
    try{
      await navigator.clipboard.writeText(s)
    }catch(e){
      console.warn('cannot write user\'s clipboard', e)
    }
  }
  async function cut(){
    await writeClip(tw.selection)
    tw.insert('')
  }
  async function copy(){
    await writeClip(tw.selection)
  }
  async function paste(){
    tw.insert(await readClip())
  }
}
