
# Lines that begin with hash tags
# are comments and ignored



# All examples use python 3
# 11 example python programs
#  1. Pythagorean Theorem
#  2. Triangular numbers: 1 + 2 + 3 + 4 ... + n
#  3. Fibonacci sequence
#  4. What is your name? (from monty python)
#  5. Josephus Problem
#  6. Guess a number 1-100 game
#  7. Zeno's paradox
#  8. Collatz Conjecture sequences
#  9. fizzbuzz problem
#  10. 20 questions game (computer asks q's from a limited dictionary of words and questions)
#  11. postfix/rpn calculator


# pythagorean theorem
print("pythagorean theorem")
a = 3
b = 4
c = (a*a + b*b) ** 0.5
# convert to strings
a = str(a)
b = str(b)
c = str(c)
print("a: " + a + ", b: " + b + ", c: " + c)

# triangular numbers
print("triangular numbers")
n = 100
s = 0
# for loop
for i in range(n):
    s += i
n = str(n)
s = str(s)
print("n: " + n + "s: " + s)


print("\nfibonacci sequence")
n = 8 
a = 0
b = 1
for i in range(n + 1):
    a += b
    temp = b
    b = a
    a = temp
print("fibonacci(%s) = %s" % (n, a))


#Monty Python
name = input("\nWhat is your name?")
quest = input("What... is your quest?")
dead = True
if quest.lower() == "to seek the holy grail":
    dead = False
    if name == "Sir Robin":
        trivia = input("What is the capital of Assyria?")
        if trivia != "Assur":
            dead = True
    elif name == "King Arthur":
        trivia = input("What is the airspeed velocity of an unladen swallow?")
        if trivia.lower() == "african or european?":
            print("bridgekeeper: \"I don't know that!\"")
            print("aaaah! The bridgekeeper died.")
        else:
            dead = True
    else:
        color = input("What is your favorite color?")
        if color.lower() != "yellow":
            dead = True

if dead:
  print("aaaaah! You died!")
else:
  print("You passed the bridge.")




# Josephus  Problem
from random import randint
import time
n = randint(6,20)
circle = [True] * n
print("\n\nJosephus must decide where to sit to avoid the zealot's suicide pact.")
print("Everyone is sitting a circle and every third person must kill themselves.")
print("They are then removed from the circle and the")
print(" countdown to the next 3rd person continues.")
print("There are {} people in the circle.".format(n))
seat = int(input("Where do you sit in the circle?"))
kill = 0
dead = False
print("You chose seat #{} in the circle.".format(seat))
for i in range(n - 1):
  # print("circle", circle)
  while not circle[kill]: # seat must be filled to count
      kill = (kill + 1)%n
  kill = (kill + 1)%n
  while not circle[kill]:
      kill = (kill + 1)%n
  kill = (kill + 1)%n
  while not circle[kill]:
      kill = (kill + 1)%n
  circle[kill] = False
  if kill + 1 == seat:
     print("You were forced to drink the poison and died.")
     dead = True
  else:
     print("The person in seat #{} drank the poison and died".format(kill + 1))
  kill = (kill + 1)%n 
  time.sleep(2)
    
if dead:
    print("Better luck next time. I mean... too bad.")
else:
    print("Congratulations! You survived")
   
