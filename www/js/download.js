
function isBinaryFile(filename){
  let suffixes = [".jpg", ".png", ".gif", ".jpeg", ".webp"]
  return suffixes.map(x=>filename.endsWith(x)).reduce((a, b)=> a||b)
}

function getCommonPath(urlList){
  if(!urlList.length) return "/"
  let firstUrl = urlList[0]
  if(urlList.length < 2){
    let i = firstUrl.lastIndexOf("/")
    if(i < 0) return "/"
    return firstUrl.substr(0, i + 1)
  }
  let n = urlList.map(x=>x.length).reduce((a,b)=>Math.min(a,b), 9999)
  for(let i=1; i<urlList.length; ++i){
    let match = (b) =>
      firstUrl.substr(0,n) == b.substr(0, n)
    let and = (a, b) => a && b
    while(n > 0 && !urlList.map(match).reduce(and)){
      --n }
  }
  while(n > 0 && firstUrl[n] != '/') --n
  return n==0? "/" : firstUrl.substr(0, n)
}
function getFilename(url){
  let i = url.lastIndexOf("/")
  if(i < 0) return url
  return url.substr(i + 1)
}

async function downloadZipAsync(name, urls, timeout=8000){
  let contents = ""

  document.body.classList.add("wait")
  //try{
    if(Array.isArray(urls)){
      contents = await Promise.all(urls.map(u=>getURL(u,timeout)))
    } else {
      contents = await getURL(urls);
    }
  //} catch(e){
    //console.error("Cannot zip: " + name)
    //console.error(e)
    //document.body.classList.remove("wait")
    //return
  //}

  let now = new Date();
  let timestamp_suffix = "_" + now.getFullYear() + "_" +
    ("0" + (now.getMonth() + 1)).slice(-2) + now.getDate()

  let zipname = name + timestamp_suffix

  let zip = new JSZip()
  let folder = zip.folder(name)
  if(typeof contents == "string"){
    folder.file(name + ".html", contents) }
  else if(Array.isArray(contents)){ // array
    let path = getCommonPath(urls)
    console.log("common path: " + path)
    for(let i=0; i < urls.length; ++i){
      let filename = urls[i].substr(path.length)
      folder.file(filename, contents[i])
    }
  } else {
    throw new Error("string or array expected for contents")
  }
  let file_data = await zip.generateAsync({type:"blob"})
  console.log(file_data)
  saveAs(file_data, zipname)


  document.body.classList.remove("wait")
}

function getURL(url, timeout = 8000){
  let req = new XMLHttpRequest()
  if(isBinaryFile(url)) req.responseType = "blob"
  req.open("GET", url)
  return new Promise((yes, no)=>{
    let done = false
    req.onload = (e)=>{
      if(req.readyState == 4){
        if(req.status == 200){
          done = true
          console.log('response type', req.responseType)
          yes(req.response)}
        else{
          console.error(req.statusText)
          done = true
          no()
        }
      }
    }
    req.onerror = (e) => {
      console.error(req.statusText)
      done = true
      no()
    }
    req.send(null)
    setTimeout(()=>{ if(!done) no()}, timeout)
  })
}

