
// 2 top buttons: precision, 
// 3 bottom buttons: zoom-in, zoom-out, solid/outline

// TODO "repack" the strokes into tuple tables
//  {__tables: {strokes: {headers, rows}}}
// with a header.
const
  PointSize = 3,
  DrawMargin = 2,
  DefaultLineWidth = 2,
  DefaultColor = "#000";
  Line = 0,
  Curve = 1,
  Polygon = 2,
  Polycurve = 3;


let
  zoomp = 1,
  origin = [0n, 0n],
  precision = 5/2,
  drawing = {
    // { styleIndex,
    //   windowIndex,
    //   array: [dx, dy...]}
    __tupletables: {
      strokes: { headers: [
        'styleIndex', 'windowIndex', 'array' ]}},
    strokes: [{
        styleIndex: 0,
        windowIndex: 0,
        array: [30, 30, 50, 50, 80, 50, 110, 30],
      }
    ],
    windows: [
      { zoomp: 0,
        origin: [0n, 0n] }, // optional: crop
    ],
    styles: [
      { type: Line,            // Line, Curve, Polygon, Polycurve
        width: 2,
        color: "rgba(0,0,0,0.8)",
        dash: null,
        fill: null,
      },
    ], // {lineWidth, color, dash, fill}
  }

function tupleTableSerialize(obj, tupletables){
  let tables = tupletables;
  if(!tables) tables = obj.__tupletables;
  else obj.__tupletables = tables;
  if(!tables) throw new Error("tuple table '__table' missing.");
  //console.log('tables', tables);
  //delete obj["__tables"];
  let arrays = {};
  for(let key in tables){
    let table = tables[key];
    let array = obj[key];
    if(!array) array = [];
    arrays[key] = array;
    let {headers} = table;
    let rows = obj[key] = [];
    for(let j=0; j<array.length; ++j){
      let item = array[j];
      let row = [];
      rows[j] = row;
      for(let k=0; k<headers.length; ++k){
        let header = headers[k];
        let value = item[header];
        row[k] = value;
      }
    }
  }

  let result = JSON.stringify(obj);
  for(let key in tables){
    obj[key] = arrays[key];
  }
  return result;
  //obj["__tables"] = tables;
}

function tupleTableParse(str, tupletables){
  let obj = JSON.parse(str);
  let tables = tupletables;
  if(!tables) tables = obj.__tupletables;
  if(!tables) throw new Error("tuple table '__table' missing.");
  for(let key in tables){
    let table = tables[key];
    let rows = obj[key];
    let array = obj[key] = [];
    let {headers} = table;
    delete table['rows'];
    for(let j=0; j<rows.length; ++j){
      let item = {};
      array[j] = item;
      let row = rows[j];
      for(let k=0; k<headers.length; ++k){
        let header = headers[k];
        let value = row[k];
        item[header] = value;
      }
    }
  }
  return obj;
}
function dumpDrawing(){
  // convert bigints to string.
  for(let i=0; i<drawing.windows.length; ++i){
    let win = drawing.windows[i];
    win.origin[0] = win.origin[0].toString();
    win.origin[1] = win.origin[1].toString();
  }

  let result = tupleTableSerialize(drawing); //JSON.stringify(drawing);

  // convert back to bigint
  for(let i=0; i<drawing.windows.length; ++i){
    let win = drawing.windows[i];
    win.origin[0] = BigInt(win.origin[0].toString());
    win.origin[1] = BigInt(win.origin[1].toString());
  }
  return result;
}
function loadDrawing(s){
  let data = tupleTableParse(s);
  for(let i=0; i<drawing.windows.length; ++i){
    let win = drawing.windows[i];
    win.origin[0] = BigInt(win.origin[0]);
    win.origin[1] = BigInt(win.origin[1]);
  }
  drawing = data;
}

function draw(){
  let ctx = c0.getContext("2d");
	// assume all strokes are lines for now.
  let strokes = drawing.strokes;

	for(let i=0; i<strokes.length; ++i){
		let stroke = strokes[i];
    let style = drawing.styles[stroke.styleIndex];
    let win = drawing.windows[stroke.windowIndex];
    let zoomdiff = zoomp - win.zoomp;
    let array = stroke.array;
    if(Math.abs(zoomdiff) > 5) continue;
    let zoom = 2**zoomdiff;
    console.log("zoom", zoom);
    console.log("origin", origin);
    let [x0, y0] = [
      zoom * parseInt(win.origin[0] - origin[0]),
      zoom * parseInt(win.origin[1] - origin[1]),
    ]
    let [minx, miny, maxx, maxy] = [
      -DrawMargin * c0.width,
      -DrawMargin * c0.height,
      c0.width + DrawMargin * c0.width,
      c0.height + DrawMargin * c0.height
    ];
    if(x0 < minx || y0 < miny || x0 > maxx || y0 > maxy){
      // out of bounds, not within one window of current window.
      continue;
    }
    let [x, y] = [ x0, y0 ];
    ctx.beginPath();
    //try{
      console.log(`win.origin ${win.origin}`);
      console.log(`x,y ${x}, ${y} ${JSON.stringify(array)}`);
    //}catch(e){
      //console.error(e);
    //}
    // TODO style
    for(let j=0; j<array.length; j+=2){
      let dx = zoom * array[j],
          dy = zoom * array[j+1];
      x += dx;
      y += dy;
      if(j==0) ctx.moveTo(x, y);
      else     ctx.lineTo(x, y);
      ctx.fillRect(x - PointSize/2, y - PointSize/2, PointSize, PointSize);
    }
    ctx.stroke();
  }
  ctx.fillRect(30, 30, 10, 5);
}
function resize(){
  c0.width = window.innerWidth - 5;
  c0.height = window.innerHeight - 4;
  draw();
}

function refresh(){
  draw();
}

function mousedown(e){
}
function mouseup(e){
}
function mousemove(e){
}

let html_src = `
  <canvas id="c0"></canvas>
  <style>
    *{
      margin: 0;
      overflow: hidden;
    }
    #c0{
      border: 2px solid green;
    }
  </style>
`
function testTupTables(){
  let testobj = {
    strokes: [
      {a: 0, b: 1, c: 2},
      {a: 0, b: 1, c: 2},
      {a: 0, b: 1, c: 2},
    ],
  }
  let tupletables = {
    strokes: {headers:['a', 'b', 'c']},
  }
  // console.log('testobj', JSON.stringify(testobj));
  let str = tupleTableSerialize(testobj, tupletables);
  console.log('\ntestobj serialized: ', str);
  console.log('\ntestobj parsed: ', tupleTableParse(str, tupletables));
}

function test(){
  // testTupTables();
  // return;
  console.log("Test");
  let s = dumpDrawing();
  console.log("drawing: " + s);
  //loadDrawing(s);
  console.log("Drawing loaded.");
}
function init(){
  document.body.innerHTML = html_src;
  window.addEventListener("resize", resize);
  window.addEventListener("mousedown", mousedown);
  window.addEventListener("mouseup", mouseup);
  window.addEventListener("mousemove", mousemove);
  resize();
  draw();
}

if(typeof window == "undefined")
  test();
else
  init();
