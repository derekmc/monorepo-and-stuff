
window.addEventListener("load", main);

let linetext = [];
let logtext = "";

function main(){
  window.addEventListener("keydown", keydown);
}

function allspace(line){
  for(let i=0; i<line.length; ++i){
    let ch = line[i];
    if(ch == "&nbsp;" || ch == " ") continue;
    return false;
  }
  return true;
}

function keydown(e){
  let k = e.key;
  if(k.length == 1){
    if(k == "<") k = "&lt;";
    if(k == ">") k = "&gt;";
    if(k == "&") k = "&amp;";
    if(k == " " && allspace(linetext)){
      k = "&nbsp;";
    }
    linetext.push(k);
  } else{
    if(k == "Backspace"){
      let n = linetext.length;
      if(n > 0){
        linetext = linetext.slice(0, n-1);
      } else {
        let lines = logtext.split("<br>");
        logtext = lines.slice(0, lines.length-1).join("<br>");
      }
    }
    if(k == "Enter"){
      if(logtext.length) logtext += "<br>";
      logtext += linetext.join("");;
      linetext = [];
    }
  }
  let typeline = document.querySelector(".typeline");
  let typemirror = document.querySelector(".typemirror");
  let typelog = document.querySelector(".typelog");
  let linestr = linetext.join("");
  typeline.innerHTML = linestr;
  typemirror.innerHTML = linestr;
  typelog.innerHTML = logtext;
}

function reverse(s){
  return s.split('').reverse().join('');
}
