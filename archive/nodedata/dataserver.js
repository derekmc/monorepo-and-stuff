
const http = require('http');
const url = require('url');
const Type = require('./type.js');
const fs = require('fs');

module.exports = DataServer;

function fileExists(filename){
  return new Promise((resolve, reject) => {
    fs.access(filename, fs.constants.F_OK, (err) => {
      if(err) resolve(false);
      else resolve(true);
    })
  })
}

function readFile(filename){
  return new Promise((resolve, reject) => {
    fs.readFile(filename, null, (err, data) => {
      if(err){
        console.log(err);
        reject(err);
      } else {
        try{
          resolve(JSON.parse(data));
        } catch (e){
          console.error(e);
          reject(e);
        }
      }
    })
  })
}

function writeFile(filename, contents){
  return new Promise((resolve, reject) => {
    fs.writeFile(filename, contents, null, (err)=>{
      if(err){
        console.log(err);
        reject(err);
      } else {
        resolve(true);
      }
    })
  })
}

function refStr(col, id){
  return col + "|" + id;
}

function DataServer(initfile, dbfile, port){
  let data_server = {};
  let schemas = {};
  let data = {};
  let ids = {};
  let indexes = {};
  let indexrefs = {};  // maps indexed columns to a rowid [col]::[val] -> rowid
  let loaded = false;

  initfile = initfile? initfile : "initdb.json";
  dbfile = dbfile? dbfile : "db.json";

  port = port? port : 8080;
  
  data_server.load = async ()=> {
    let file_data;
    // TODO check schema matches between dbfile and initfile? nah.
    if(await fileExists(dbfile)){
      file_data = await readFile(dbfile);
    } else {
      file_data = await readFile(initfile);
    }
    //console.log(file_data);

    data = file_data.data;
    schemas = file_data.schemas;
    indexes = file_data.indexes;
    ids = file_data.ids;
    //console.log(JSON.stringify({data, schemas, indexes, ids}, null, 1));

    data = data ? data : {};
    schemas = schemas ? schemas : {};
    indexes = indexes ? indexes : {};
    ids = ids ? ids : {};

    for(let tbl in schemas){
      if(!ids[tbl]) ids[k] = data[tbl]? data[tbl].length + 1 : 1; // ids start at 1.
      if(!indexes[tbl]) indexes[tbl] = [];
      if(!data[tbl]) data[tbl] = {};
      if(!indexrefs[tbl]) indexrefs[tbl] = {};
    }

    // populate all indexrefs
    for(let tbl in indexes){
      let indexlist = indexes[tbl];
      let tabledata = data[tbl];
      if(indexlist.length){
        for(let rowid in tabledata){
          let row = tabledata[rowid];
          for(let i = 0; i < indexlist.length; ++i){
            let index = indexlist[i];
            let ref = refStr(index, row[index]);
            indexrefs[tbl][ref] = rowid;
          }
        }
      }
    }

    // console.log("Index refs:\n" + JSON.stringify(indexrefs, null, 1));
    loaded = true;
  }

  data_server.save = async ()=> {
    let contents = JSON.stringify({ids, schemas, indexes, data}, null, 1);
    await writeFile(dbfile, contents);
  }
  
  data_server.runServer = () => {
    if(!loaded)
      throw new Error(
        "DataServer.load() must be called before you call runServer()");
    let server = http.createServer(
      makeHandler({schemas, data, ids, indexes, indexrefs}));
    server.listen(port);
    return server;
  }

  return data_server;
}

function makeHandler({schemas, data, ids, indexes, indexrefs}){
  return function(req, res){
    /* This is the new way to get url parts */
    /*const baseURL = req.protocol + "://" + req.headers.host + "/"
    const reqUrl = new URL(req.url, baseURL);
    let path = reqUrl.pathname.split("/");
    let query = reqUrl.searchParams; /**/


    /* This is the old way to get url parts */
    let url_object = url.parse(req.url, true);
    let path = url_object.pathname.split("/");
    let query = url_object.query; /**/


    if(path.length && path[0] == ""){
      path.shift();
    }
    
    // /tablename/[add|get|set]/[rowid];
    if(path.length < 2)
      return error("Incomplete path, '/[table]/[action]' required.");
    if(path.length > 3)
      return error(
        "Path only permits three maximum entries '/[table]/[action]/[rowid]'");
      
    let tbl = path[0];
    let action = path[1];
    let rowid = path.length > 2? path[2] : null;

    if(!(tbl in schemas)){
      return error(`Unknown table '${tbl}'`);
    }

    let rows = [];

    let id = null;
    if(action == "add"){
      let cols = urisplit(query.cols);
      let values = urisplit(query.values);
      let schema = schemas[tbl];
      let indexlist = indexes[tbl];
      let tabledata = data[tbl];

      // set id
      id = ids[tbl];

      let dataobj = {};
      for(let i = 0; i < cols.length; ++i){
        let col = cols[i];
        let value = values[i];
        if(!schema.hasOwnProperty(col)){
          //console.log(col, "\n", schema);
          return error(`Unknown column '${col}'`);
        }
        for(let j=0; j<indexlist.length; ++j){
          let index = indexlist[j];
          if(index == col){
            let ref = refStr(index, value);
            let refs = indexrefs[tbl];
            if(ref in refs){
              return error(`Cannot add to '${tbl}', '${ref}' already added.`);
            }
            refs[ref] = id;
          }
        }

        // TODO more type handling
        // try{ value = JSON.parse(value); }
        // catch(e){ /* do nothing */ }

        try{
          Type(schema[col], value);
        } catch (e) {
          console.error(e);
          return error(`Illegal value for '${col}': '${value}'`);
        }
        dataobj[col] = value;
      }

      // increment table id
      ++ids[tbl];

      // store data object
      data[tbl][id] = dataobj;
    }
    if(action == "get"){
    }
    if(action == "set"){
    }

    // console.log("request", path, query);
    res.writeHead(200, {'Content-Type': 'text/json'});
    let payload = {
      "success": true,
      "action": action,
      "rowid": id,
    }
    if(action == "add"){
    }
    let result_str = JSON.stringify(payload, null, 2);
    res.end(result_str);
    function error(msg){
      res.end(JSON.stringify({success: false, message: msg}, null, 2));
    }
    function urisplit(s){
      return decodeURIComponent(s).split(" ");
    }
  }
}

