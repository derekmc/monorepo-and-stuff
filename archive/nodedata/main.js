
let DataServer = require("./dataserver.js");
let port = 8888;
let initfile = "initdb.json";
let dbfile = "db.json";


async function main(){
  let ds = DataServer(initfile, dbfile, port);
  await ds.load();

  ds.runServer();
  console.log(`Server listening on ${port}`);
}

main();
