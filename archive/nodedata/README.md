
# Node Data

This is a demonstration of a simple database in nodejs,
which uses a table structure.

### Why NodeJS / Design

NodeJS excels and asynchronous operations and concurrency handling, which is
exactly what databases need.  Therefore, I believe nodejs to be one of the

### Use Case

Typically, this will not be directly web facing, but used internally on a
network, much like how a mysql database might be used.  It should be easily
used without any library or middleware, by making get requests and parsing
responses.

In order to make it easy to use, this demo currently restrict data types,
for both keys and values, to mostly alphanumerics.  This is covered in the
"Features" section below.

### Storage Implementations


The initial version will be implemented as a live service with no storage,
ie if the database goes, down, the data is lost.

Subsequent versions may include polling whole file storage,
(ie dump the entire database to a file every 10 minutes if changed.)

Using sqlite or an sql database like mysql or mariadb may also be implemented,
or perhaps some nosql database like redis or memcache.  This is only expected if
a full featured implementation of this demonstration is built.


### Features and Limitations

The database is accessed through basic http operations, and operates on 
table structured data, with tables, rows, keys, and columns(not key/value).

####  Restricted Keys and Values

Values should be alphanumeric strings only, with dashes, underscores,
and periods.
Keys or Indexes should be only alphanumerics, underscores, and dashes,
no periods.
There are no join operations or foreign key constraints, you must enforce
and perform those manually.


All operations may use get or post requests, and all will return json.

** UPDATED **

dashes separate fields, and are not allowed in tablenames, or column names

#### References

```
Column Reference: [tableprefix]colname
RowId Reference: [tableprefix]rowid (rowid is a positive integer)
Index Reference: [tableprefix]indexname-indexvalue


All tables get mapped to a prefix, depending on the order they are listed.
Prefixes start with A, B, C... 
Only 26 tables in a single call are allowed.
Otherwise, split up calls.
```


```
; Example Table and Row Operations

/add?tables=user&refs=1name+1password+1email&values=joe+sesame+feiw%2104ow%23oen
/get?tables=user+cookies&refs=1name-joe+2user-joe
/set?tables=cookies&refs=1token&values=asdf

; Tables and schemas are defined in initdb.json

/version                           ; Freezes or gets a recent version for dumping the database
/v/tablename?page=0&pagesize=100   ; 100 is the default pagesize.
```




### Optional / Future Features

 - Data types, allow more data values (json based, json-value-types on npm)
 - Softlocking (does not prevent database modifications, but the transaction
    with a "lock" may be 
