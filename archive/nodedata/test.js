
let DataServer = require("./dataserver.js");
const http = require("http");

const port = 8082;
const dbfile = "db.json";
const initfile = "initdb.json";

async function test(){
  console.log("DataServer tests started.");
  let ds = DataServer(initfile, dbfile, port);
  await ds.load();
  ds.runServer(port);

  let data;
  await wait(1);
  data = await get('localhost', port,
    '/user/add?cols=name+password+email&' +
    'values=tom+sesame+' + encodeURIComponent("tom@websitenamee.com"));

  console.log("" + data);

  await wait(1);
  data = await get('localhost', port,
    '/user/add?cols=name+password+email&' +
    'values=tomstwin+sesame+' + encodeURIComponent("tom@websitenamee.com"));

  console.log("" + data);
  ds.save();
  console.log("DataServer tests finished.");
  await wait(3);
  console.log("Test server terminating...");
  process.exit();
}

function wait(scd){
  return new Promise((resolve, reject) => {
    setTimeout(resolve, scd*1000);
  });
}


function get(host, port, path){
  return new Promise((resolve, reject) => {
    let options = {
      host: host,
      port: port,
      path: path,
      method: "GET",
    }

    const req = http.request(options, res => {
      res.on('data', d => {
        resolve(d);
      })
    });
    req.on('error', err => {
      console.error(err);
      reject(err);
    })

    req.end();
  })
}

test();
