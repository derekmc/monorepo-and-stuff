
if(typeof module !== undefined){
  module.exports = Type; }
else if(typeof window !== undefined){
  window.Type = Type; }

Type.test = test;

function test(){
  Type([0,0, /-{0,1}[1-9][0-9]*/], [1,2,-9]);
  // Type(User, {username:"joe", passsalt:"asldkfj", test: 7});
  Type(User, {username:"joe", passsalt:"asldkfj"});
}

function Type(def, obj){
  function err(msg){
    console.error("Type definition: " + JSON.stringify(def));
    console.error("Failed value: " + JSON.stringify(obj));
    throw new Error(msg);
  }
  const opt = "$";

  let t = typeof def;
  let s = typeof obj;

  if(def === null || def === undefined){
    return obj;
  }
  if(t == "object"){
    if(def.constructor == RegExp){
      let str = obj.toString();
      let match = str.match(def);
      if(!match){
        err("object.toString() value did not match regex");
      }
      if(match[0] !== str){
        err("object.toString() value partinal regex match");
      }
      return obj;
    }

    if(Array.isArray(def)){
      if(!Array.isArray(obj)) err("Not an array");
      if(def.length == 0) return obj;
      if(def.length == 1){
        for(let i=0; i<obj.length; ++i){
          Type(def[0], obj[i]); }
      } else if(def.length != obj.length){
        err(`Tuple size was ${obj.length} not ${def.length}`);
      } else for(let i=0; i<obj.length; ++i){
        Type(def[i], obj[i]); 
      }
    } else {
      // normal duck typing object.
      //
      // TODO if definition has a value under the empty string,
      //  all extra properties of obj, must match that value.
      //  if the value is undefined, then no extra properties are allowed.

      if(s != "object") err("Not an object");
      for(let k in def){
        if(k == "") continue;
        let key = k;
        let optional = (k[0] == opt)
        if(optional) key = k.substring(1);
        if(key in obj)
          Type(def[k], obj[key]);
        else if(!optional)
          err(`Missing property "${key}"`);
        // Note: obj may have extra properties
      }
      if(def.hasOwnProperty("")){
        let extratype = def[""];
        for(let key in obj){
          let k = '$' + key;
          if(!(key in def || k in def)){
            if(extratype === undefined){
              err(`Extra property "${key}" not allowed.`); 
            }
            Type(extratype, obj[key]);
          }
        }
      }
    }
  }
  if(s != t && (t == "string" || t == "number" || t == "boolean")){
    err(`Not a ${t}`);
  }
  return obj;
}

