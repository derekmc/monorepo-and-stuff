
const
  Port = 8080;

const
  http = require("http"),
  path = require("path"),
  wslib = require("ws"),
  static = require("node-static"),
  { createServer } = http,
  { WebSocketServer } = wslib;
 



function initServer(){
  const fileServer = new static.Server("./www/");
  const wss = new WebSocketServer({ noServer: true });
  const httpServer = createServer(httpServerHandle);

  console.log(`WebSocket demo started on localhost:${Port}`);

  wss.on("connection", (ws)=>{
    console.log("connected.");

    ws.on("message", (data)=>{
      console.log("data: %s", data);
      ws.send(`\nYou said: "${data}".`);
    })

    ws.send("connected!");
  })
  httpServer.on("upgrade", function upgrade(req, socket, head){
    /*const { pathname } = parse(req.url);*/
    console.log(`ws connection ${req.url}`);
    wss.handleUpgrade(req, socket, head, (ws)=>{
      wss.emit('connection', ws, req);
    })
  })

  httpServer.listen(Port);

  function httpServerHandle(req, res){
    req.addListener("end", function(){
      fileServer.serve(req, res);
    }).resume();
  }
}


initServer();
