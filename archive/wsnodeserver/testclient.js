
let wslib = require("ws");
let { WebSocket } = wslib;

const ws = new WebSocket("ws://localhost:8080");

ws.on("open", ()=>{
  let i = 0;
  setInterval(()=>ws.send(`Hello, World: ${++i}`), 5000);
})

ws.on("message", (data)=>{
  console.log("data: %s", data);
})
