
function main(){
  console.log("In websocket demo.js");
  let socket = new WebSocket("ws://localhost:8080");
  socket.addEventListener("open", (ev)=>{
    socket.send("Hello, World.");
  })
  socket.addEventListener("message", (ev)=>{
    console.log('message: ', ev.data);
  })
}

main();
