stylesheets close to the content they affect seems like a good idea
to make document easily composable.

If you want to factor styles more fully, I think semantic style
rules are a good idea, ".pad10px .colorfff", etc.  That way
you can understand rules clearly.
