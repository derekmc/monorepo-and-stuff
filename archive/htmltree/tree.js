
// Template inheritance demo
// TODO use a template filter.
const Verbose = false;
let log = x=>{ if(Verbose) console.log(x); }
let print = x=> console.log(x);

let src = `
  %page
    <head>
      %head
        %title <title> Example Page </title>
        %meta
          <meta name="viewport" content="width: device-width, initialscale=1">
        %includes
          <link rel="stylesheet" href="style.css">
    </head>
    <body>
      %body
        <h1>
          %title
        </h1>
        <div class="nav">
          %nav
            <a href="/"> Home </a> &emsp;
            <a href="/about"> About </a>
        </div>
          
        <div>
          %main
            <p> The main page content goes here. </p>
        </div>
        <footer>
          <hr>
          %footer
            Thanks for visiting. This is the footer.
        </footer>
    </body>
`;

let asdf = `
  # TODO only render the last block, if the last block does not inherit from a
  # previous block, then issue a warning.  If a previous block is unused, also
  # issue a warning.

  %page.alert # inherits from page
    %head.meta
      <meta name="viewport" content="width: device-width, initialscale=0.5">
    %body
      %title Nevermind
      %main
        <p> Hello, world </p>
      %user
        %fname Joe
        %lname Cool

`
//log(src);
let tree = parse(src);
print("parsed");
print(JSON.stringify(tree, null, 2));

let templates = {};

/*
function render(template, data){
  let 
}

function renderSubtemplate
*/

function copy(obj){
  return JSON.parse(JSON.stringify(obj));
}
// TODO to render, first substitute
function render(root){
  if(root.name.length){
    let path = root.name.split(".");
    let parentPath = root.slice(0);
    parentPath.pop();
    let parentTemplate = node
  }
  
  // substitute the value in at path within node.
  // this will perform multiple substitutions if there
  // are duplicate named block paths within a parse tree.
  function subPath(node, path, value){
    if(typeof node == "string") return;
    if(node.name.length){
      let namepath = node.name.split(".");
      if(namepath.length > path) return;
      for(let i=0; i<namepath.length; ++i){
        if(namepath[i] != path[i]) return; // paths don't match
      }
      // remove this current node's name path from the given search path.
      path = path.slice(namepath.length);
    }
    if(!path.length){
      return value;
    }

    for(let i=0; i<node.nodes.length; ++i){
      let next = node.nodes[i];
      let replacement = subPath(node, path, value);
      if(replacement) node.nodes[i] = replacement;
    }
  }
  // If a node in a child template has direct text children
  // all that node's siblings must be text as well.
  function render(node){
    
  }

  function renderSubs(node, ptemp, context){
    if(typeof node == "string")
      throw new Error("Node unexpectedly a string");
    let hasText = false;
    let nodes = node.nodes;
    for(let i=0; i<nodes.length; ++i){
      let child = nodes[i];
      if(typeof child == "string") hasText = true;
    }
    if(hasText && typeof nodes[0] != "string"){
      console.warn(
        "Warning: if a child's template block has a direct text child,\n" +
        " that forces the entire block to be rendered directly and not\n" +
        " inherited from the parent template, so it is reccomended that\n" +
        " the first child in the block be text, for to avoid confusion.");
    }
    if(hasText){
      return true;
    } else {
      for(let i=0; i<nodes.length; ++i){
        let path = nodes[i];
        //subPath(ptemp, 
      }
    }

    if(node.name.length){
      let path = node.name.split(".");
      // the parent's blocks, into which to render
      // this node.
      //let blockstack = findPath(ptemp, path); 
      //let last = 
      //blockstack[blo
    }
  }
  function renderChild(node, ptemp, context){
    if(typeof node == "string")
      return node;
    if(node.name.length){
      let path = node.name.split(".");
      for(let i=0; i<child.nodes.length; ++i){
        let cnode = child.nodes[i];
        //if(
      // TODO handle template inheritance.
      }
    }
    return node.nodes.map(x => renderParent(x, ptemp, context)).join("");
  }
}

function parse(s){
  s = s.replace(/\t/g, "  ");

  let i = 0, j = 0;
  let root = null, block = null, blockstack = [];
  let lineno = 0, sameline = false,
      indent = 0; // minindent = 9999;

  start();
  if(blockstack.length) root = blockstack[0];

  return root;
  //block = [];
  // block.indent;
  // {indent: 0, entries: []}

  function start(){
    do{
      indent = 0;
      while(++i < s.length && s[i] == " ") ++indent;
    }while(s[i] == "\n");

    linestart = i;

    // text only block
    if((!block || indent > block.indent) && s[i] != "%"){
      let newblock = {
        name: "", // empty name string indicates not a template section.
        indent: indent,
        nodes: [],
      }
      blockstack.push(newblock);
      if(block) block.nodes.push(newblock);
      block = newblock;
    } else{
      while(block && (indent < block.indent || block.name.length && indent ==
      block.indent)){
        // close block
        log("close block:" + block.name);
        if(blockstack.length == 1) root = blockstack[blockstack.length - 1];
        blockstack.pop();
        block = blockstack[blockstack.length - 1];
      }
    }
    sameline = false;
    if(i < s.length && s[i] == "%") return blocktag();
    return text();
    //log("tagname:" + s.substring(i, i+5));
  }
  function blocktag(){
    j = i+1;
    log("blocktag");
    while(++i < s.length && !s[i].match(/[\n \t]/));
    if(i > j+1){
      sameline = (s[i] != "\n");
      let newblock = {
        name: s.substring(j, i),
        indent: indent,
        nodes: []
      };
      log("newblock:" + newblock.name);
      blockstack.push(newblock);
      if(block) block.nodes.push(newblock);
      block = newblock;
    }
    return text();
  }
  function addline(){
    while(i < s.length && s[i] != "\n") ++i;

    if(!block){
      block = {
        name: "",
        indent: indent,
        nodes: [],
      };
      blockstack.push(block);
    }

    let extra = "";
    if(indent > block.indent){
      for(let k=0; k<indent - block.indent; ++k)
        extra += " ";
    }
    let chunk = (!sameline? "\n" + extra : "") + s.substring(j, i);
    if(chunk.length > 1){
      log("line:" + chunk.substring(1));

      let nodes = block.nodes;
      if(nodes.length && typeof nodes[nodes.length - 1] == "string"){
        nodes[nodes.length - 1] += chunk;
      } else {
        nodes.push(chunk);
      }
    }
    return start();
  }
  function text(){
    j = i;
    while(i < s.length){
      if(s[i] == "\n")
        return addline();
      if(s[i] == "#")
        return comment();
      ++i;
    }
  }
  function comment(){
    j = i;
    while(++i < s.length){
      if(s[i] == "\n"){
        log("comment:" + s.substring(j, i));
        return start();
      }
    }
  }
}
