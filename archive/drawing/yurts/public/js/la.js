
window.addEventListener("load", init);

const StartZoom = 3;
const Margin = 10;
const TileSize = 10;
const DataKey = "pixadraw-data";
const AnimateDt = 180;
const SaveInterval = 3500;

let active_keys = {};
let [x, y] = [30,30];
let [x0, y0] = [0, 0];
let points = {};
let framecount = 0;
let zoom = 3;
let minzoom = 3/TileSize;

async function init(){
  document.body.innerHTML = pagesrc;
  window.addEventListener('keydown', keydown);
  window.addEventListener('keyup', keyup);
  window.addEventListener('resize', resize);
  setInterval(loop, AnimateDt);
  setInterval(save, SaveInterval);
  resize();
  //await load();
  draw();
}
function load(){
  return new Promise((yes, no) => {
    if(DataKey in localStorage){
      let data = localStorage.getItem(DataKey);
      items = JSON.parse(data);
      points = items.points ?? {};
      x0 = items.x0 ?? 0;
      y0 = items.y0 ?? 0;
      x = items.x ?? 0;
      y = items.y ?? 0;
    }
    yes();
  })
}
function save(){
  return new Promise((yes, no)=> {
    let data = JSON.stringify({points, x0, y0, x, y});
    localStorage.setItem(DataKey, data);
    yes();
  })
}

let key_actions = {
  65: 'left',
  87: 'up',
  68: 'right',
  83: 'down',
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
  13: 'enter',
  32: 'space'
}
let repeat_actions = {
  65: 'left',
  87: 'up',
  68: 'right',
  83: 'down',
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
}

function keyup(e){
  delete active_keys[e.keyCode];
}
function keydown(e){
  let code = e.keyCode; //console.log(code);

  if(code in active_keys) return;
  active_keys[code] = framecount;
  if(code in key_actions) action(key_actions[code]);
}
function action(act){
  switch(act){
    case "left": x -= TileSize; break; 
    case "right": x += TileSize; break; 
    case "up": y -= TileSize; break; 
    case "down": y += TileSize; break; 
    case "space":
      let a = `${x}:${y}`;
      points[a] = !points[a];
      break; 
  }
  let [w, h] = [aaa.width, aaa.height];
  let s = zoom;
  let z = TileSize;

  updateZoom();

  if(s*(x - x0) + w/2 < Margin){
    x0 = x - (Margin - w/2)/s;
  }
  if(s*(y - y0) + h/2 < Margin){
    y0 = y - (Margin - h/2)/s;
  }
  if(s*(x - x0 + z) > w/2 - Margin - z){
    x0 = x + z - (w/2 - Margin - z)/s;
  }
  if(s*(y - y0 + z) > h/2 - Margin - z){
    y0 = y + z - (h/2 - Margin - z)/s;
  }

  draw();
}
function draw(){
  let ctx = aaa.getContext("2d");
  ctx.fillStyle = "#fff";
  ctx.fillRect(0,0,aaa.width, aaa.height);
  ctx.fillStyle = "#000";
  let [w, h] = [aaa.width, aaa.height];
  let size = zoom * TileSize;
  for(let key in points){
    if(!(points[key])) continue;
    let [a, b] = key.split(":").map(x=>parseInt(x))
    a -= x0;
    b -= y0;
    a *= zoom;
    b *= zoom;
    a += w/2;
    b += h/2;
    if(-10 < a && a < w && -10 < b && b < h){
      ctx.fillRect(a, b, size, size); }
  }
  let line = Math.max(1/2, size/5);
  ctx.beginPath();
  ctx.lineWidth = Math.min(line, 1);
  ctx.strokeStyle = "#fff";
  ctx.strokeRect(
    zoom*(x-x0) + w/2,
    zoom*(y-y0) + h/2,
    size, size);
  ctx.stroke()
  ctx.beginPath();
  ctx.lineWidth = line + 1;
  ctx.strokeStyle = "#000";
  ctx.strokeRect(
    zoom*(x-x0) + w/2 - line,
    zoom*(y-y0) + h/2 - line,
    size + 2*line, size + 2*line);
  ctx.stroke()
}
function updateZoom(){
  let w = aaa.width;
  let h = aaa.height;
  let offscreen = Math.max(Math.abs(x0), Math.abs(y0)) / Math.min(w,h);
  zoom = Math.max(minzoom,
    Math.min(zoom, StartZoom * Math.pow(2, -4*offscreen)));
}
function resize(){
  aaa.width = window.innerWidth-4;
  aaa.height = window.innerHeight-4;
  updateZoom();
}
function loop(){
  for(let k in active_keys){
    let keyframe = active_keys[k];
    if(k in repeat_actions && keyframe < framecount){
      action(repeat_actions[k]); }
  }
  ++framecount;
  draw();
}

let pagesrc = `
  <style>
    *{
      margin: 0px;
      overflow: hidden;
      box-sizing: border-box;
    }
  </style>
  <canvas id='aaa' style="border: 2px solid black"></canvas>
`
