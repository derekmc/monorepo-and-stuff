const express = require('express')
const open = require('open')
const app = express()
const getPort = require('get-port')
const defaultPort = 9000;

// TODO client host, ie hostless controller. A client browser runs the data transaction controller.
//require((()=>"./userscript/test.js"
require(process.cwd() + "/userscript/test.js")();

(async function(){
  app.get('/hello', (req, res) => {
    res.send('Hello World!')
  })

  app.use(express.static("public"))

  //app.get('/
  const port = await getPort({port: defaultPort})
  const host = `http://localhost:${port}`

  app.listen(port, async () => {
    console.log(`Example app listening at http://localhost:${port}`)
    open(host);
    console.log(`Opening ${host}.`)
  })
})()

