
# Section Templates

## Intro

A section template is a template which uses subsections to
allow authors to rapidly update the static documents


## Why Static Documents

Instead of keeping the content and templates separated, and
in different formats, section templates allow you to keep
templates and content in the same place.  This means you can
edit a document as it is, without the need to run a static
site generator to view the changes.  Once you are happy with
the changes to one document, you can apply those formatting
changes to all your other documents.


## A Pattern, and a target

The pattern is the template being read, the target is the
document being modified.  Applying a pattern to a target
replaces all the "boilerplate", which is not inside a
section.


## Inner Content and Outer Content

Inner content is everything inside of a section.  Outer
content is everything not contained in a section.  When you
apply a pattern to a target, the inner content is preserved
and the outer content is replaced. 

Sections are marked with comment that contains "Section: ",
either HTML style or c style comments (which also work in
css and javascript), sections are closed with the simple
"End" keyword, again inside a comment.

C style comments can be single line or multiline.

    // Section: js
    console.log("hello mars?");
    // End

    /* Section: js */
    console.log("this is mars :)");
    /* End */

HTML style comments only have one format
    <!-- Section: main -->
    <!-- End -->

You could update this system to work with other comment
styles.


## The Master Pattern

If you make changes to the outer content specific document
in a project, it is good practice to have a primary pattern,
which you then use to update the rest of your documents.

You may want to change a specific document first, then you
can update your master pattern, finally, you can update the
rest of your documents.
