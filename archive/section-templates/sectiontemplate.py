#!/usr/bin/env/

import sys

def apply(patternfile, targetfile):
   
    pattern 
    print(f"TODO 'sectiontemplate.py'\n  apply pattern '{patternfile}' to target '{targetfile}'.")

def main(args):

    c = len(args)
    if c < 2:
        print(f"'sectiontemplate.py' called with {c} arguments.")
        print("Usage: \"python sectiontemplate.py pattern target1 [target2]...\"")
    else:
        pattern = args[0]
        targets = args[1:]
        for target in targets:
            apply(pattern, target)

    

if __name__ == "__main__":
    # remove name of python script from argument list.
    args = sys.argv[1:] 
    main(args)
