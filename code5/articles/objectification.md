# The Objectification Criterion
# When to use objects

The objectification criterion is the method
you use of assessing when to make a specific
part of your program an object.
The answer is simple: Use an object when
you want to develop a part of your program
as a standalone program itself.

# What Do Objects Do?

While it may be instictive to think
of objects as enhanced datastructures,
or even a specialized datatype, I think
this is not an effective way to use
objects, and can easily result
in a program with too many separate
objects, which makes the program's
development more difficult and
complicated at every step, including
design, modification, documentation,
and testing.

Objects provide 4 primary benefits in programming:

 1. Delegate Responsibility.
 2. Encapsulate Private Data and Methods
 3. Simplify naming.
 4. Abstract Interface (polymorphism)

Perhaps the most important part of objects is the
delegation of responsibility to the object itself.

This allows us to create code divisions with
minimal connections between modules, and furthermore
a hierarchy of code delegation. Higher level
components call lower ones, but lower level code should
avoid directly interacting with the the higher level
implementation. It should not directly 
rely on specific details of the higher level code,
so that you can modify the two parts of the program
independently.
This will allow the development of the object
to be reasoned about and tested completely independent
from the rest of the program, which dramatically reduces
complexity. You can use a small wrapper program to 
create a user interface to interact with the program
for thorough testing, or script automated tests
that can test features automatically.

While the larger program may affect some of the
design decisions for object code, the object code
should be able to operate completely independently.
(In some cases objects use callbacks, but these should have a fixed interface,
defined by the object itself and not its owner)

This leads us back to the objectification criterion
we just stated: use an object whenever you
want to allow a certain part of the code to be
developed and tested in a completely standalone
fashion, that is the object itself can be
used as its own program with its own interface,
either programmatic or user facing.

We can think of objects as
independent programs inside of a larger
program.

This brings us to the title of this blog:
"The five levels of coding". Objectification
is what I call level for, and I would contend
it is the most determining skill for successful
professional software development. While more
basic skills are still critical, they are
generally well understood and followed, and 
while more advanced skills can be extremely
useful, they are not necessarily make
or break for the success of a developer.

If you can therefore master level four, 
that is a good indication that you have
the mastery necessary to be an effective
professional developer, even if there are
many other skills that are critical to
professional success, especially learning
to use tools and frameworks
and communication and productivity.


# The Five Levels of Coding
today, "The five levels of coding". Technically
there are 7 levels, but level 0 is precoding
and level 6 is post coding(automation).


### Level Zero: Document Editing, text or image based.

1. Scripting and calculations: a sequence of commands to modify a system
2. Goto and Globals: Flow of control and explicit state space
3. Functions or Subroutines: Cleanup
4. Objects: API controlled Subprograms
5. Domain Specific Language:
     Create the optimal language to express instructions or
     an idea, then use it to express that idea.
     This includes tools like level editors for games,
     spreadsheets, etc.

### Level Six: Automation, Formal Verification, Static Typing, Artificial Intelligence


# Programming Full Circle

Programming full circle is the phenomenon where the higher
levels of coding, can be used by more highly
trained developers, to implement tools that allow
common users or developers with basic skills to build
the majority of the system.
This makes both individuals and teams more productive,
as it reduces barriers to entry and task complexity
at the lower level. Users only need a very basic
understanding of the system to make substantial contributions
and build their own unique variations.

In general, the higher you move up the programming circle,
into objectification and then automation, the more accessible
and efficient the development process will be on the other
end of those tools.

A simple example might be a level editor or character designer
for a game or system.  This could be thought of as either
a domain specific language for describing the details of
your game, or automation tools that modify your
game according with minimal user input.

With tools like domain specific
languages and automation, basic users
can build very substantial systems.

Spreadsheet formulas are another extremely prevelant
example of this. Even if you have more advanced
programming skills, it is often beneficial 
to simply rely on spreadsheet formulas when 
that is a sufficient tool for the specific
task you need to do.
Common users are able to leverage
such tools with minimal training. Thus, development work becomes
accessible and not exclusive exclusive exclusive
